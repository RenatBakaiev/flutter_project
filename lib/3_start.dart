import 'package:flutter/material.dart';

class MyApp3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.orange[900],
          title: Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                width: 30,
                height: 30,
                child: IconButton(
                  icon: Image.asset(
                    'assets/arrow2.png',
                    width: 11,
                    height: 11,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/2_start');
                  },
                ),
              ),

              // GestureDetector(
              //   onTap: () {
              //     Navigator.pushNamed(context, '/2_start');
              //   },
              //   child: SizedBox(
              //     width: 11,
              //     height: 11,
              //     child: Image.asset(
              //       'assets/arrow2.png',
              //       width: 11,
              //       height: 11,
              //     ),
              //   ),
              // ),
              Image.asset('assets/freegen-logo-white.png',
                  width: 39, height: 34, color: Colors.white),
              Text(
                'FreeGen Reality',
                style: TextStyle(
                    fontSize: 28,
                    color: Colors.white,
                    fontFamily: 'ArialRoundedMTBold'),
              ),
              SizedBox(
                width: 30,
                height: 30,
                child: IconButton(
                  icon: Image.asset(
                    'assets/close.png',
                    width: 11,
                    height: 11,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/');
                  },
                ),
              ),
              // GestureDetector(
              //   onTap: () {
              //     Navigator.pushNamed(context, '/');
              //   },
              //   child: SizedBox(
              //     width: 11,
              //     height: 11,
              //     child: Image.asset(
              //       'assets/close.png',
              //       width: 11,
              //       height: 11,
              //     ),
              //   ),
              // ),
            ],
          )),
        ),
        body: Container(
          // child: SingleChildScrollView(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/3.start_pic.jpg', width: 420),
                Container(
                  margin: const EdgeInsets.only(bottom: 5.0, top: 10),
                  child: Text(
                    'Найкращий спосіб\nознайомитися із закладом,\nлокацією, або містом',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 22,
                        color: Colors.black,
                        fontFamily: 'Comfortaa',
                        fontWeight: FontWeight.w900),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 10.0, top: 5),
                  child: Text(
                    'Вбудовані карти, контрольні мітки,\nсчитувач: QR-кодів, зображень та\nоб’єктів, - не дозволять тобі\nнудьгувати. А цікаві коментарі, історії\nта загадки, допоможуть дізнатися\nбільше корисної інформації про\nмісцевість.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        fontFamily: 'Comfortaa',
                        fontWeight: FontWeight.w500),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 35.0),
                  child: SizedBox(
                    width: 237,
                    height: 50,
                    child: RaisedButton(
                      color: Colors.orange[900],
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(25.0)),
                      onPressed: () {
                        Navigator.pushNamed(context, '/4_login');
                      },
                      child: Text(
                        'ДАЛI',
                        style: TextStyle(
                            fontSize: 13,
                            color: Colors.white,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w900),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          // ),
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.orange[900], Colors.white])),
        ),
      ),
    );
  }
}
