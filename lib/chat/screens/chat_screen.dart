import 'package:flutter/material.dart';
import 'package:flutterfreegenapp/chat/models/message_model.dart';
import 'package:flutterfreegenapp/chat/models/user_model.dart';

class ChatScreen extends StatefulWidget {
  final User user;

  ChatScreen({this.user});
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  _buildMessage(Message message, bool isMe) {
    final Container msg = Container(
      margin: isMe
          ? EdgeInsets.only(top: 8.0, bottom: 8.0, left: 80.0, right: 16.0)
          : EdgeInsets.only(top: 8.0, bottom: 8.0, left: 16.0),
      padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0),
      width: MediaQuery.of(context).size.width * 0.75,
      decoration: BoxDecoration(
          color: isMe ? Colors.blue[500] : Colors.white,
          borderRadius: isMe
              ? BorderRadius.only(
                  topLeft: Radius.circular(12.0),
                  bottomLeft: Radius.circular(12.0),
                  topRight: Radius.circular(12.0),
                )
              : BorderRadius.only(
                  topRight: Radius.circular(12.0),
                  bottomRight: Radius.circular(12.0),
                  topLeft: Radius.circular(12.0),
                )),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(message.time,
              style: TextStyle(
                  color: isMe ? Colors.white : Colors.black,
                  fontSize: 15.0,
                  fontWeight: FontWeight.w600)),
          SizedBox(height: 8.0),
          Text(message.text,
              style: TextStyle(
                  color: isMe ? Colors.white : Colors.black,
                  fontSize: 15.0,
                  fontWeight: FontWeight.w600)),
        ],
      ),
    );
    if (isMe) {
      return msg;
    }
    return Row(
      children: <Widget>[
        msg,
        IconButton(
          icon: message.isLiked
              ? Icon(Icons.favorite)
              : Icon(Icons.favorite_border),
          iconSize: 30.0,
          color: message.isLiked ? Colors.red : Colors.white,
          onPressed: () {},
        ),
      ],
    );
  }

  _buildMessageComposer() {
    return Container(
      margin: EdgeInsets.only(bottom: 5.0),
      padding: EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      height: 44.0,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          GestureDetector(
            child: Image.asset('assets/chat_camera.png', height: 32, width: 24),
            onTap: () {
              Navigator.of(context).pushNamed('/6_main_layout_camera');
            },
          ),
          GestureDetector(
            child: Image.asset('assets/chat_icon.png', height: 34, width: 26),
            onTap: () {},
          ),
          // IconButton(
          //   icon: Icon(Icons.photo),
          //   iconSize: 25.0,
          //   color: Colors.red,
          //   onPressed: () {},
          // ),
          // Expanded(
          // child:
          Container(
              height: 34,
              width: 250.0,
              child: Stack(alignment: Alignment.centerRight, children: <Widget>[
                TextFormField(
                  textCapitalization: TextCapitalization.sentences,
                  style: TextStyle(
                      fontSize: 17, color: Colors.black, fontFamily: 'Roboto'),
                  textAlign: TextAlign.left,
                  textAlignVertical: TextAlignVertical.center,
                  decoration: new InputDecoration(
                    hintText: 'Напиши повідомлення...',
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 8.5, horizontal: 8.5),
                    fillColor: Colors.white,
                    filled: true,
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(17.0),
                    ),
                  ),
                ),
                Positioned(
                  right: 4.0,
                  child: GestureDetector(
                    child: Image.asset('assets/chat_microphone.png',
                        height: 26, width: 26),
                    onTap: () {},
                  ),
                ),
              ])),

          //     TextFormField(

          //   textCapitalization: TextCapitalization.sentences,
          //   onChanged: (value) {},
          //   decoration:
          //       InputDecoration.collapsed(hintText: 'Напиши повідомлення...', ),
          // )
          // ),
          // IconButton(
          //   icon: Icon(Icons.send),
          //   iconSize: 25.0,
          //   color: Colors.red,
          //   onPressed: () {},
          // )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange[900],
      // appBar: AppBar(
      //   backgroundColor: Colors.orange[900],
      //   title: Center(
      //     child: Text(
      //       widget.user.name,
      //       style: TextStyle(
      //           fontSize: 28.0,
      //           fontWeight: FontWeight.bold,
      //           fontFamily: 'ArialRoundedMTBold'),
      //     ),
      //   ),
      //   elevation: 0.0,
      //   actions: <Widget>[
      //     IconButton(
      //       icon: Icon(Icons.more_horiz),
      //       iconSize: 30.0,
      //       color: Colors.white,
      //       onPressed: () {},
      //     ),
      //   ],
      // ),
      appBar: AppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.orange[900],
        title: Container(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              width: 30,
              height: 30,
              child: IconButton(
                icon: Image.asset(
                  'assets/arrow2.png',
                  width: 11,
                  height: 11,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '../screens/home_screen.dart');
                },
              ),
            ),
            // GestureDetector(
            //   child: SizedBox(
            //        height: 11,
            //       width: 11,
            //     child: Image.asset('assets/arrow2.png',width: 11, height: 11)),
            //   onTap: () {
            //     Navigator.pushNamed(context, '../screens/home_screen.dart');
            //   },
            // ),
            Image.asset('assets/freegen-logo-white.png',
                width: 39, height: 34, color: Colors.white),
            Text(
              'FreeGen Reality',
              style: TextStyle(
                  fontSize: 28,
                  color: Colors.white,
                  fontFamily: 'ArialRoundedMTBold'),
            ),
            SizedBox(
              width: 30,
              height: 30,
            )
          ],
        )),
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Colors.orange[900], Colors.white],
          ),
        ),
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () => FocusScope.of(context).unfocus(),
          child: Column(
            children: <Widget>[
              Column(
                children: <Widget>[
                  CircleAvatar(
                    radius: 40,
                    backgroundColor: Colors.white,
                    child: CircleAvatar(
                      radius: 35,
                      backgroundImage: AssetImage(widget.user.imageUrl),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Text(
                    widget.user.name,
                    style: TextStyle(
                        fontSize: 13,
                        color: Colors.white,
                        fontFamily: 'Comfortaa',
                        fontWeight: FontWeight.w700),
                  ),
                  SizedBox(height: 5.0),
                  Text(
                    widget.user.status,
                    style: TextStyle(
                        fontSize: 10,
                        color: Colors.white,
                        fontFamily: 'Comfortaa',
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(height: 5.0),
                ],
              ),
              Expanded(
                child: Container(
                  // margin: EdgeInsets.only(right: 16.0, left: 16.0),
                  // decoration: BoxDecoration(
                  //     gradient: LinearGradient(
                  //       begin: Alignment.topCenter,
                  //       end: Alignment.bottomCenter,
                  //       colors: [Colors.orange[900], Colors.white],
                  //     ),
                  //     borderRadius: BorderRadius.only(
                  //       topLeft: Radius.circular(30.0),
                  //       topRight: Radius.circular(30.0),
                  //     )),
                  child: ClipRRect(
                    // borderRadius: BorderRadius.only(
                    //   topLeft: Radius.circular(90.0),
                    //   topRight: Radius.circular(30.0),
                    // ),
                    child: ListView.builder(
                      reverse: true,
                      padding: EdgeInsetsDirectional.only(top: 15.0),
                      physics: BouncingScrollPhysics(),
                      itemCount: messages.length,
                      itemBuilder: (BuildContext context, int index) {
                        final Message message = messages[index];
                        final bool isMe = message.sender.id == currentUser.id;
                        return _buildMessage(message, isMe);
                      },
                    ),
                  ),
                ),
              ),
              _buildMessageComposer(),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        alignment: Alignment.topCenter,
        height: 83,
        decoration: BoxDecoration(color: Colors.white),
        child: Container(
          margin: const EdgeInsets.only(top: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_1.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/6_main_layout');
                  },
                ),
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_2.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/7_search');
                  },
                ),
              ),
              GestureDetector(
                child: Image.asset(
                  'assets/bottom_pic_3.png',
                  height: 42.0,
                  width: 63.0,
                ),
                onTap: () {
                  Navigator.of(context).pushNamed('/6_main_layout_camera');
                },
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_4.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '../screens/home_screen.dart');
                  },
                ),
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_5.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/9_profile');
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
