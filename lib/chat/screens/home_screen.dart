import 'package:flutter/material.dart';
import 'package:flutterfreegenapp/chat/widgets/category_selector.dart';
import 'package:flutterfreegenapp/chat/widgets/favourite_contacts.dart';
import 'package:flutterfreegenapp/chat/widgets/recent_chats.dart';

class MyApp9 extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<MyApp9> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.orange[900],
      // appBar: AppBar(
      //   backgroundColor: Colors.orange[900],
      //   leading: IconButton(
      //     icon: Icon(Icons.menu),
      //     iconSize: 30.0,
      //     color: Colors.white,
      //     onPressed: () {},
      //   ),
      //   title: Center(
      //     child: Text('Chats',
      //         style: TextStyle(
      //             fontSize: 28.0,
      //             fontWeight: FontWeight.bold,
      //             fontFamily: 'ArialRoundedMTBold')),
      //   ),
      //   elevation: 0.0,
      //   actions: <Widget>[
      //     IconButton(
      //       icon: Icon(Icons.search),
      //       iconSize: 30.0,
      //       color: Colors.white,
      //       onPressed: () {},
      //     ),
      //   ],
      // ),
      appBar: AppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.orange[900],
        title: Container(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              width: 30,
              height: 30,
              child: IconButton(
                icon: Image.asset(
                  'assets/arrow2.png',
                  width: 11,
                  height: 11,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/6_main_layout');
                },
              ),
            ),
            // GestureDetector(
            //   child: SizedBox(
            //        height: 11,
            //     width: 11,
            //     child: Image.asset('assets/arrow2.png',width: 11, height: 11)),
            //   onTap: () {
            //     Navigator.pushNamed(context, '/6_main_layout');
            //   },
            // ),
            Image.asset('assets/freegen-logo-white.png',
                width: 39, height: 34, color: Colors.white),
            Text(
              'FreeGen Reality Chat',
              style: TextStyle(
                  fontSize: 24,
                  color: Colors.white,
                  fontFamily: 'ArialRoundedMTBold'),
            ),
            SizedBox(
              width: 30,
              height: 30,
            )
          ],
        )),
      ),
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.orange[900], Colors.white])),
        child: Column(
          children: <Widget>[
            CategorySelector(),
            Expanded(
              child: Container(
                // margin: EdgeInsets.only(right: 10, left: 10),
                decoration: BoxDecoration(
                  // color: Colors.orange[700],
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30.0),
                      topRight: Radius.circular(30.0)),
                ),
                child: Column(
                  children: <Widget>[
                    FavouriteContacts(),
                    RecentChats(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        alignment: Alignment.topCenter,
        height: 83,
        decoration: BoxDecoration(color: Colors.white),
        child: Container(
          margin: const EdgeInsets.only(top: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_1.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/6_main_layout');
                  },
                ),
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_2.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/7_search');
                  },
                ),
              ),
              GestureDetector(
                child: Image.asset(
                  'assets/bottom_pic_3.png',
                  height: 42.0,
                  width: 63.0,
                ),
                onTap: () {
                  Navigator.of(context).pushNamed('/6_main_layout_camera');
                },
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_4.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '../screens/home_screen.dart');
                  },
                ),
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_5.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/9_profile');
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
