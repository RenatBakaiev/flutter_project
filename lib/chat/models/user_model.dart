class User {
  final int id;
  final String name;
  final String imageUrl;
  final String status;

  User({
    this.id,
    this.name,
    this.imageUrl, 
    this.status,
  });
}