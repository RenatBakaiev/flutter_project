import '../models/user_model.dart';

class Message {
  final User sender;
  final String
      time; // Would usually be type DateTime or Firebase Timestamp in production apps
  final String text;
  final bool isLiked;
  final bool unread;

  Message({
    this.sender,
    this.time,
    this.text,
    this.isLiked,
    this.unread,
  });
}

// YOU
final User currentUser = User(
  id: 0,
  name: 'Current User',
  imageUrl: 'assets/images/renat.jpg',
  status: 'Explorer',
);

// USERS
final User renat =
    User(id: 1, name: 'Renat Bakaiev', imageUrl: 'assets/images/renat.jpg', status: 'Explorer',);

final User catwoman =
    User(id: 2, name: 'Catwoman', imageUrl: 'assets/images/catwoman.jpg', status: 'Explorer',);

final User batman =
    User(id: 3, name: 'Batman', imageUrl: 'assets/images/batman.jpg', status: 'Explorer',);

final User hulk =
    User(id: 4, name: 'Hulk', imageUrl: 'assets/images/hulk.jpg', status: 'Explorer',);

final User spiderman =
    User(id: 5, name: 'Spiderman', imageUrl: 'assets/images/spiderman.jpg', status: 'Explorer',);

final User joker =
    User(id: 6, name: 'Joker', imageUrl: 'assets/images/joker.jpg', status: 'Explorer',);

final User superman =
    User(id: 7, name: 'Superman', imageUrl: 'assets/images/superman.jpg', status: 'Explorer',);

final User xena =
    User(id: 8, name: 'Xena', imageUrl: 'assets/images/xena.jpg', status: 'Explorer',);

final User ironman =
    User(id: 9, name: 'Ironman', imageUrl: 'assets/images/ironman.jpg', status: 'Explorer',);

final User storm =
    User(id: 10, name: 'Storm', imageUrl: 'assets/images/storm.jpg', status: 'Explorer',);            
    

// FAVORITE CONTACTS
List<User> favourites = [catwoman, joker, storm, spiderman, xena, batman];

// EXAMPLE CHATS ON HOME SCREEN
List<Message> chats = [
  Message(
    sender: batman,
    time: '5:30 PM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: catwoman,
    time: '4:30 PM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: hulk,
    time: '3:30 PM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: false,
    unread: false,
  ),
  Message(
    sender: ironman,
    time: '2:30 PM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: joker,
    time: '1:30 PM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: false,
    unread: false,
  ),
  Message(
    sender: xena,
    time: '12:30 PM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: false,
    unread: false,
  ),
  Message(
    sender: spiderman,
    time: '11:30 AM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: false,
    unread: false,
  ),
    Message(
    sender: storm,
    time: '11:50 AM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: false,
    unread: false,
  ),
    Message(
    sender: superman,
    time: '15:10 AM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: false,
    unread: false,
  ),    
    Message(
    sender: renat,
    time: '12:35 AM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: false,
    unread: false,
  ),
];

// EXAMPLE MESSAGES IN CHAT SCREEN
List<Message> messages = [
  Message(
    sender: catwoman,
    time: '5:30 PM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: true,
    unread: true,
  ),
  Message(
    sender: currentUser,
    time: '4:30 PM',
    text: 'Just walked my doge. She was super duper cute. The best pupper!!',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: xena,
    time: '3:45 PM',
    text: 'How\'s the doggo?',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: renat,
    time: '3:15 PM',
    text: 'All the food',
    isLiked: true,
    unread: true,
  ),
  Message(
    sender: currentUser,
    time: '2:30 PM',
    text: 'Nice! What kind of food did you eat?',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: spiderman,
    time: '2:00 PM',
    text: 'I ate so much food today.',
    isLiked: false,
    unread: true,
  ),
];
