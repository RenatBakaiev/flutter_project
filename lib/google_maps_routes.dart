// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';

// class MyAppRoutes extends StatefulWidget {
//   @override
//   _MapScreenState createState() => _MapScreenState();
// }

// class _MapScreenState extends State<MyAppRoutes> {
//   GoogleMapController mapController;
//   double _originLatitude = 50.450148, _originLongitude = 30.524024;
//   double _destLatitude = 50.445309, _destLongitude = 30.517827;
//   // double _originLatitude2 = 50.443716, _originLongitude2 = 30.512435;
//   // double _destLatitude2 = 50.455216, _destLongitude2 = 30.507202;
//   // double _destLatitude0 = 50.450404, _destLongitude0 = 30.510954;
//   // double _dest2Latitude = 50.445522, _dest2Longitude = 30.516997;
//   // double _dest3Latitude = 50.452588, _dest3Longitude = 30.514120;
//   Map<MarkerId, Marker> markers = {};
//   Map<PolylineId, Polyline> polylines = {};
//   List<LatLng> polylineCoordinates = [];
//   PolylinePoints polylinePoints = PolylinePoints();
//   String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

//   @override
//   void initState() {
//     super.initState();

//     /// origin marker
//     // _addMarker(LatLng(_originLatitude, _originLongitude), "origin",
//     //     BitmapDescriptor.defaultMarker);
//         // _addMarker(
//         // LatLng(_originLatitude, _originLongitude),
//         // "destination",
//         // BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
//         // InfoWindow(title: 'Point2'));

//     /// destination marker
//     // _addMarker(
//     //     LatLng(_destLatitude0, _destLongitude0),
//     //     "destination0",
//     //     BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
//     //     InfoWindow(title: 'Start'));

//     // _addMarker(
//     //     LatLng(_destLatitude, _destLongitude),
//     //     "destination",
//     //     BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
//     //     InfoWindow(title: 'Point2'));

//     _addMarker(
//         LatLng(_originLatitude, _originLongitude),
//         "origin",
//         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
//         InfoWindow(title: 'Start'));
//     _addMarker(
//         LatLng(_destLatitude, _destLongitude),
//         "destination",
//         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
//         InfoWindow(title: 'Point2'));

//     // _addMarker(
//     //     LatLng(_dest3Latitude, _dest3Longitude),
//     //     "destination3",
//     //     BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
//     //     InfoWindow(title: 'Point3'));

//     _getPolyline();
//     // _getPolyline2();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//       child: Scaffold(
// appBar: AppBar(
//   automaticallyImplyLeading: false,
//   backgroundColor: Colors.orange[900],
//   title: Container(
//       child: Row(
//     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//     children: <Widget>[
//       GestureDetector(
//         child: SizedBox(
//             height: 11,
//             width: 11,
//             child: Image.asset('assets/arrow2.png',
//                 width: 11, height: 11)),
//         onTap: () {
//           Navigator.pushNamed(context, '/1_start');
//         },
//       ),
//       Image.asset('assets/freegen-logo-white.png',
//           width: 39, height: 34, color: Colors.white),
//       Text(
//         'FreeGen Reality',
//         style: TextStyle(
//             fontSize: 28,
//             color: Colors.white,
//             fontFamily: 'ArialRoundedMTBold'),
//       ),
//       SizedBox(
//         width: 11,
//         height: 11,
//       )
//     ],
//   )),
// ),
//         body: GoogleMap(
//           initialCameraPosition: CameraPosition(
//               target: LatLng(_originLatitude, _originLongitude), zoom: 15),
//           myLocationEnabled: true,
//           tiltGesturesEnabled: true,
//           compassEnabled: true,
//           scrollGesturesEnabled: true,
//           zoomGesturesEnabled: true,
//           onMapCreated: _onMapCreated,
//           markers:
//               // {kievMarker},

//               Set<Marker>.of(markers.values),
//           polylines: Set<Polyline>.of(polylines.values),
//         ),

//         bottomNavigationBar: Container(
//           alignment: Alignment.topCenter,
//           height: 83,
//           decoration: BoxDecoration(color: Colors.white),
//           child: Container(
//             margin: const EdgeInsets.only(top: 10.0),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceAround,
//               children: <Widget>[
//                 GestureDetector(
//                   child: Image.asset(
//                     'assets/bottom_pic_1.png',
//                     height: 15.0,
//                     width: 14.0,
//                   ),
//                   onTap: () {
//                     Navigator.pushNamed(context, '/6_main_layout');
//                   },
//                 ),
//                 GestureDetector(
//                   child: Image.asset(
//                     'assets/bottom_pic_2.png',
//                     height: 16.0,
//                     width: 16.0,
//                   ),
//                   onTap: () {
//                     Navigator.pushNamed(context, '/7_search');
//                   },
//                 ),
//                 GestureDetector(
//                   child: Image.asset(
//                     'assets/bottom_pic_3.png',
//                     height: 42.0,
//                     width: 63.0,
//                   ),
//                   onTap: () {
//                     // _openCamera(context);
//                     // Navigator.pushNamed(context, '/ar');
//                     Navigator.of(context).pushNamed('/6_main_layout_camera');
//                   },
//                 ),
//                 GestureDetector(
//                   child: Image.asset(
//                     'assets/bottom_pic_4.png',
//                     height: 16.0,
//                     width: 16.0,
//                   ),
//                   onTap: () {
//                     Navigator.pushNamed(context, '../screens/home_screen.dart');
//                   },
//                 ),
//                 GestureDetector(
//                   child: Image.asset(
//                     'assets/bottom_pic_5.png',
//                     height: 15.0,
//                     width: 11.0,
//                   ),
//                   onTap: () {
//                     Navigator.pushNamed(context, '/9_profile');
//                   },
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }

// //   Marker kievMarker = Marker(
// //   markerId: MarkerId('Quest1'),
// //   position: LatLng(50.447497, 30.522705),
// //   infoWindow: InfoWindow(title: 'Finish Point'),
// //   icon: BitmapDescriptor.defaultMarkerWithHue(
// //     BitmapDescriptor.hueGreen,
// //   ),
// // );

//   void _onMapCreated(GoogleMapController controller) async {
//     mapController = controller;
//   }

//   _addMarker(LatLng position, String id, BitmapDescriptor descriptor,
//       InfoWindow infowindow) {
//     MarkerId markerId = MarkerId(id);
//     Marker marker = Marker(
//         markerId: markerId,
//         icon: descriptor,
//         position: position,
//         infoWindow: infowindow);
//     markers[markerId] = marker;
//   }

//   _addPolyLine() {
//     PolylineId id = PolylineId("poly");
//     Polyline polyline = Polyline(
//       polylineId: id,
//       visible: true,
//       color: Colors.green,
//       points:
//           polylineCoordinates,
//           // [
//           //   LatLng(50.450388, 30.510998),
//           //   LatLng(50.449283, 30.512280),
//           //   LatLng(50.448545, 30.516775),
//           //   LatLng(50.447623, 30.521828),
//           //   LatLng(50.447064, 30.522810)
//           // ]

//       //     [
//       //   LatLng(50.450404, 30.510954),
//       //   LatLng(50.450906, 30.510188),
//       //   LatLng(50.446815, 30.508659),
//       //   LatLng(50.445504, 30.516952),
//       //   LatLng(50.444725, 30.521641),
//       //   LatLng(50.447622, 30.522531),
//       //   LatLng(50.448913, 30.514560),
//       //   LatLng(50.451898, 30.515697),
//       //   LatLng(50.451898, 30.515697),
//       //   LatLng(50.452588, 30.514120),
//       // ],
//       width: 4,
//       startCap: Cap.roundCap,
//       endCap: Cap.buttCap,
//     );
//     polylines[id] = polyline;
//     setState(() {});
//   }

//   //   _addPolyLine2() {
//   //   PolylineId id = PolylineId("poly");
//   //   Polyline polyline = Polyline(
//   //     polylineId: id,
//   //     visible: true,
//   //     color: Colors.red,
//   //     points: polylineCoordinates,
//   //     width: 4,
//   //     startCap: Cap.roundCap,
//   //     endCap: Cap.buttCap,
//   //   );
//   //   polylines[id] = polyline;
//   //   setState(() {});
//   // }

//   _getPolyline() async {
//     PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
//         "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//         PointLatLng(_originLatitude, _originLongitude),
//         PointLatLng(_destLatitude, _destLongitude),

//         travelMode: TravelMode.walking,
//         wayPoints: [PolylineWayPoint(location: "Kiev")]);
//     if (result.points.isNotEmpty) {
//       result.points.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });
//     }
//     _addPolyLine();
//   }

//   // _getPolyline2() async {
//   //   PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
//   //       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//   //       PointLatLng(_originLatitude2, _originLongitude2),
//   //       PointLatLng(_destLatitude2, _destLongitude2),

//   //       travelMode: TravelMode.walking,
//   //       wayPoints: [PolylineWayPoint(location: "Kiev")]);
//   //   if (result.points.isNotEmpty) {
//   //     result.points.forEach((PointLatLng point) {
//   //       polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//   //     });
//   //   }
//   //   _addPolyLine2();
//   // }

// }

// --------------------------------------flutter_polyline_points 0.2.2----------------------------------------------------

// // import 'dart:collection';

// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';
// import 'package:geolocator/geolocator.dart';

// // import '9_profile_user_rewards.dart';

// class MyAppRoutes extends StatefulWidget {
//   @override
//   _MapScreenState createState() => _MapScreenState();
// }

// class _MapScreenState extends State<MyAppRoutes> {
//   var currentLocation;
//   GoogleMapController mapController;
//   bool mapToggle = false;

//   double _originLatitude = 50.450712, _originLongitude = 30.510529; // kiev //
//   double _destLatitude = 50.448938, _destLongitude = 30.514434;

//   double _originLatitude2 = 50.448857, _originLongitude2 = 30.514577;
//   double _destLatitude2 = 50.446069, _destLongitude2 = 30.513515;

//   double _originLatitude3 = 50.446021, _originLongitude3 = 30.513841;
//   double _destLatitude3 = 50.445422, _destLongitude3 = 30.517384;

//   // double _originLatitude = 37.423351, _originLongitude = -122.078315; // google //
//   // double _destLatitude = 37.423697, _destLongitude = -122.090152;

//   // double _originLatitude2 = 37.421532, _originLongitude2 = -122.088667;
//   // double _destLatitude2 = 37.420519, _destLongitude2 = -122.078464;

//   // double _originLatitude3 = 37.414503, _originLongitude3 = -122.092519;
//   // double _destLatitude3 = 37.414852, _destLongitude3 = -122.084268;
//   // Set<Marker> _markers = HashSet<Marker>(); // Custom marker

//   Map<MarkerId, Marker> markers = {};
//   Map<PolylineId, Polyline> polylines = {};
//   List<LatLng> polylineCoordinates = [];
//   List<LatLng> polylineCoordinates2 = [];
//   List<LatLng> polylineCoordinates3 = [];
//   PolylinePoints polylinePoints = PolylinePoints();
//   PolylinePoints polylinePoints2 = PolylinePoints();
//   PolylinePoints polylinePoints3 = PolylinePoints();
//   String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

//   // var locationsAreCool = false; //

//   // BitmapDescriptor _markerIcon;

//   // void _setMarkersIcon() async {
//   //   _markerIcon = await BitmapDescriptor.fromAssetImage(
//   //       ImageConfiguration(), 'assets/profile_item4.png');
//   // }

//   @override
//   void initState() {
//     super.initState();

//     Geolocator().getCurrentPosition().then((currloc) {
//       setState(() {
//         currentLocation = currloc;
//       });
//     });

//     // addCustomMarker(){}

//     /// first route
//     _addMarker(LatLng(_originLatitude, _originLongitude), "origin",
//         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
//         InfoWindow(title: 'Start point'));
//     _addMarker(
//       LatLng(_destLatitude, _destLongitude),
//       "destination",
//       BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
//       InfoWindow(title: 'End point'),
//     );
//     _getPolyline();

//     /// second route
//     _addMarker(
//         LatLng(_originLatitude2, _originLongitude2),
//         "origin2",
//         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
//         InfoWindow(title: 'Start point 2'));
//     _addMarker(
//         LatLng(_destLatitude2, _destLongitude2),
//         "destination2",
//         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
//         InfoWindow(title: 'End point 2'));
//     _getPolyline2();

//     /// third route
//     _addMarker(
//         LatLng(_originLatitude3, _originLongitude3),
//         "origin3",
//         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
//         InfoWindow(title: 'Start point 3'));
//     _addMarker(
//         LatLng(_destLatitude3, _destLongitude3),
//         "destination3",
//         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
//         InfoWindow(title: 'End point 3'));
//     _getPolyline3();

//     // _checkMyLocation(){
//     //   if (currentLocation.latitude == _destLatitude && currentLocation.longitude == _destLongitude) {
//     //     locationsAreCool = true;
//     //   }
//     //   print(locationsAreCool);
//     // }
//     // _checkMyLocation();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//       child: Scaffold(
//         appBar: AppBar(
//           automaticallyImplyLeading: false,
//           backgroundColor: Colors.orange[900],
//           title: Container(
//               child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: <Widget>[
//               SizedBox(
//                 width: 30,
//                 height: 30,
//                 child: IconButton(
//                   icon: Image.asset(
//                     'assets/arrow2.png',
//                     width: 11,
//                     height: 11,
//                   ),
//                   onPressed: () {
//                     Navigator.pushNamed(context, '6_main_layout');
//                   },
//                 ),
//               ),
//               Image.asset('assets/freegen-logo-white.png',
//                   width: 39, height: 34, color: Colors.white),
//               Text(
//                 'FreeGen Reality',
//                 style: TextStyle(
//                     fontSize: 28,
//                     color: Colors.white,
//                     fontFamily: 'ArialRoundedMTBold'),
//               ),
//               SizedBox(
//                 width: 30,
//                 height: 30,
//               )
//             ],
//           )),
//         ),
//         body:

//         // mapToggle ?

//         GoogleMap(
//           initialCameraPosition: CameraPosition(
//               target:
//                   LatLng(currentLocation.latitude, currentLocation.longitude),
//               zoom: 15),
//           myLocationEnabled: true,
//           tiltGesturesEnabled: true,
//           compassEnabled: true,
//           scrollGesturesEnabled: true,
//           zoomGesturesEnabled: true,
//           onMapCreated: _onMapCreated,
//           // markers: _markers,
//           markers: Set<Marker>.of(markers.values),
//           polylines: Set<Polyline>.of(polylines.values),
//         )

//       //   : Center(
//       //   child: Text('Loading... Please wait...',
//       //   style: TextStyle(fontSize: 20.0),
//       //   ),
//       // )

//         ,
// bottomNavigationBar: Container(
//   alignment: Alignment.topCenter,
//   height: 83,
//   decoration: BoxDecoration(color: Colors.white),
//   child: Container(
//     margin: const EdgeInsets.only(top: 10.0),
//     child: Row(
//       mainAxisAlignment: MainAxisAlignment.spaceAround,
//       children: <Widget>[
//         SizedBox(
//           width: 35,
//           height: 35,
//           child: IconButton(
//             icon: Image.asset(
//               'assets/bottom_pic_1.png',
//               width: 16,
//               height: 16,
//             ),
//             onPressed: () {
//               Navigator.pushNamed(context, '/6_main_layout');
//             },
//           ),
//         ),
//         SizedBox(
//           width: 35,
//           height: 35,
//           child: IconButton(
//             icon: Image.asset(
//               'assets/bottom_pic_2.png',
//               width: 16,
//               height: 16,
//             ),
//             onPressed: () {
//               Navigator.pushNamed(context, '/7_search');
//             },
//           ),
//         ),
//         GestureDetector(
//           child: Image.asset(
//             'assets/bottom_pic_3.png',
//             height: 42.0,
//             width: 63.0,
//           ),
//           onTap: () {
//             Navigator.of(context).pushNamed('/6_main_layout_camera');
//           },
//         ),
//         SizedBox(
//           width: 35,
//           height: 35,
//           child: IconButton(
//             icon: Image.asset(
//               'assets/bottom_pic_4.png',
//               width: 16,
//               height: 16,
//             ),
//             onPressed: () {
//               Navigator.pushNamed(
//                   context, '../screens/home_screen.dart');
//             },
//           ),
//         ),
//         SizedBox(
//           width: 35,
//           height: 35,
//           child: IconButton(
//             icon: Image.asset(
//               'assets/bottom_pic_5.png',
//               width: 16,
//               height: 16,
//             ),
//             onPressed: () {
//               Navigator.pushNamed(context, '/9_profile');
//             },
//           ),
//         ),
//       ],
//     ),
//   ),
// ),
//     ),
//   );
// }

//   void _onMapCreated(GoogleMapController controller) async {
//     mapController = controller;

//     // setState(() { // Custom Marker
//     //   _markers.add(Marker(markerId: MarkerId("0"),
//     //   position: LatLng(37.426159, -122.083936),
//     //   infoWindow: InfoWindow(title: 'Gift', snippet: 'my best gift')
//     //   ));
//     // });
//   }

//   _addMarker(LatLng position, String id, BitmapDescriptor descriptor,
//       InfoWindow infowindow) {
//     MarkerId markerId = MarkerId(id);
//     Marker marker = Marker(
//         markerId: markerId,
//         icon: descriptor,
//         position: position,
//         infoWindow: infowindow);
//     markers[markerId] = marker;
//   }

//   // first polyline
//   _addPolyLine() {
//     PolylineId id = PolylineId("poly");
//     Polyline polyline = Polyline(
//         polylineId: id,
//         color: Colors.green,
//         points: polylineCoordinates,
//         width: 4);
//     polylines[id] = polyline;
//     setState(() {});
//   }

//   _getPolyline() async {
//     PolylineResult result1 = await polylinePoints.getRouteBetweenCoordinates(
//       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//       PointLatLng(_originLatitude, _originLongitude), /// currentLocation.latitude, currentLocation.longitude
//       PointLatLng(_destLatitude, _destLongitude),
//       travelMode: TravelMode.walking,
//       // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]
//     );
//     if (result1.points.isNotEmpty) {
//       result1.points.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });
//     }
//     _addPolyLine();
//   }

// // second polyline
//   _addPolyLine2() {
//     PolylineId id = PolylineId("poly2");
//     Polyline polyline2 = Polyline(
//         polylineId: id,
//         color: Colors.red,
//         points: polylineCoordinates2,
//         width: 4);
//     polylines[id] = polyline2;
//     setState(() {});
//   }

//   _getPolyline2() async {
//     PolylineResult result2 = await polylinePoints2.getRouteBetweenCoordinates(
//       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//       PointLatLng(_originLatitude2, _originLongitude2),
//       PointLatLng(_destLatitude2, _destLongitude2),
//       travelMode: TravelMode.walking,
//     );
//     if (result2.points.isNotEmpty) {
//       result2.points.forEach((PointLatLng point2) {
//         polylineCoordinates2.add(LatLng(point2.latitude, point2.longitude));
//       });
//     }
//     _addPolyLine2();
//   }

// // third polyline
//   _addPolyLine3() {
//     PolylineId id = PolylineId("poly3");
//     Polyline polyline3 = Polyline(
//         polylineId: id,
//         color: Colors.blue,
//         points: polylineCoordinates3,
//         width: 4);
//     polylines[id] = polyline3;
//     setState(() {});
//   }

//   _getPolyline3() async {
//     PolylineResult result3 = await polylinePoints3.getRouteBetweenCoordinates(
//       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//       PointLatLng(_originLatitude3, _originLongitude3),
//       PointLatLng(_destLatitude3, _destLongitude3),
//       travelMode: TravelMode.walking,
//     );
//     if (result3.points.isNotEmpty) {
//       result3.points.forEach((PointLatLng point3) {
//         polylineCoordinates3.add(LatLng(point3.latitude, point3.longitude));
//       });
//     }
//     _addPolyLine3();
//   }
// }

// -----------------------------Polylines+Location-------------------------------------------------------------

// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';
// import 'package:location/location.dart';

// class MyAppRoutes extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: MapScreen(),
//     );
//   }
// }

// class MapScreen extends StatefulWidget {
//   @override
//   _MapScreenState createState() => _MapScreenState();
// }

// class _MapScreenState extends State<MapScreen> {
//   GoogleMapController mapController;
//   Location _location = Location();

//   LatLng _initialPosition = LatLng(50.450712, 30.510529);

//   double _originLatitude = 50.450712, _originLongitude = 30.510529; // kiev //
//   double _destLatitude = 50.448938, _destLongitude = 30.514434;

//   double _originLatitude2 = 50.448857, _originLongitude2 = 30.514577;
//   double _destLatitude2 = 50.446069, _destLongitude2 = 30.513515;

//   double _originLatitude3 = 50.446021, _originLongitude3 = 30.513841;
//   double _destLatitude3 = 50.445422, _destLongitude3 = 30.517384;

//   // double _originLatitude = 37.423351, _originLongitude = -122.078315; // google //
//   // double _destLatitude = 37.423697, _destLongitude = -122.090152;

//   // double _originLatitude2 = 37.421532, _originLongitude2 = -122.088667;
//   // double _destLatitude2 = 37.420519, _destLongitude2 = -122.078464;

//   // double _originLatitude3 = 37.414503, _originLongitude3 = -122.092519;
//   // double _destLatitude3 = 37.414852, _destLongitude3 = -122.084268;
//   // Set<Marker> _markers = HashSet<Marker>(); // Custom marker

//   Map<MarkerId, Marker> markers = {};
//   Map<PolylineId, Polyline> polylines = {};
//   List<LatLng> polylineCoordinates = [];
//   List<LatLng> polylineCoordinates2 = [];
//   List<LatLng> polylineCoordinates3 = [];
//   PolylinePoints polylinePoints = PolylinePoints();
//   PolylinePoints polylinePoints2 = PolylinePoints();
//   PolylinePoints polylinePoints3 = PolylinePoints();
//   String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

//   void _onMapCreated(GoogleMapController _controller) async {
//     mapController = _controller;
//     _location.onLocationChanged.listen((l) {
//       _controller.animateCamera(
//         CameraUpdate.newCameraPosition(
//           CameraPosition(
//             target: LatLng(l.latitude, l.longitude),
//             zoom: 17,
//           ),
//         ),
//       );

//   // _addPolyLine() {
//   //   PolylineId id = PolylineId("poly");
//   //   Polyline polyline = Polyline(
//   //       polylineId: id,
//   //       color: Colors.green,
//   //       points: polylineCoordinates,
//   //       width: 4);
//   //   polylines[id] = polyline;
//   //   setState(() {});
//   // }

// //   _getPolyline() async {
// //     PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
// //       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
// //       PointLatLng(l.latitude, l.longitude),
// //       PointLatLng(_destLatitude, _destLongitude),
// //       travelMode: TravelMode.walking,
// //     );
// //     if (result.points.isNotEmpty) {
// //       result.points.forEach((PointLatLng point) {
// //         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
// //       });
// //     }
// //     _addPolyLine();
// //   }

// //     _getPolyline();
//     });
//   }

//   @override
//   void initState() {
//     super.initState();

//     /// first route
//     _addMarker(
//         LatLng(_originLatitude, _originLongitude),
//         "origin",
//         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
//         InfoWindow(title: 'Start point'));
//     _addMarker(
//       LatLng(_destLatitude, _destLongitude),
//       "destination",
//       BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
//       InfoWindow(title: 'End point'),
//     );
//     _getPolyline();

//     /// second route
//     _addMarker(
//         LatLng(_originLatitude2, _originLongitude2),
//         "origin2",
//         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
//         InfoWindow(title: 'Start point 2'));
//     _addMarker(
//         LatLng(_destLatitude2, _destLongitude2),
//         "destination2",
//         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
//         InfoWindow(title: 'End point 2'));
//     _getPolyline2();

//     /// third route
//     _addMarker(
//         LatLng(_originLatitude3, _originLongitude3),
//         "origin3",
//         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
//         InfoWindow(title: 'Start point 3'));
//     _addMarker(
//         LatLng(_destLatitude3, _destLongitude3),
//         "destination3",
//         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
//         InfoWindow(title: 'End point 3'));
//     _getPolyline3();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//       child: Scaffold(
//           body: GoogleMap(
//         initialCameraPosition:
//             CameraPosition(target: _initialPosition, zoom: 15),
//         mapType: MapType.normal,
//         myLocationEnabled: true,
//         tiltGesturesEnabled: true,
//         compassEnabled: true,
//         scrollGesturesEnabled: true,
//         zoomGesturesEnabled: true,
//         onMapCreated: _onMapCreated,
//         markers: Set<Marker>.of(markers.values),
//         polylines: Set<Polyline>.of(polylines.values),
//       )),
//     );
//   }

//   _addMarker(LatLng position, String id, BitmapDescriptor descriptor,
//       InfoWindow infowindow) {
//     MarkerId markerId = MarkerId(id);
//     Marker marker = Marker(
//         markerId: markerId,
//         icon: descriptor,
//         position: position,
//         infoWindow: infowindow);
//     markers[markerId] = marker;
//   }

//   // first polyline
//   _addPolyLine() {
//     PolylineId id = PolylineId("poly");
//     Polyline polyline = Polyline(
//         polylineId: id,
//         color: Colors.green,
//         points: polylineCoordinates,
//         width: 4);
//     polylines[id] = polyline;
//     setState(() {});
//   }

//   _getPolyline() async {
//     PolylineResult result1 = await polylinePoints.getRouteBetweenCoordinates(
//       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//       PointLatLng(_originLatitude, _originLongitude),

//       /// currentLocation.latitude, currentLocation.longitude
//       PointLatLng(_destLatitude, _destLongitude),
//       travelMode: TravelMode.walking,
//       // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]
//     );
//     if (result1.points.isNotEmpty) {
//       result1.points.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });
//     }
//     _addPolyLine();
//   }

// // second polyline
//   _addPolyLine2() {
//     PolylineId id = PolylineId("poly2");
//     Polyline polyline2 = Polyline(
//         polylineId: id,
//         color: Colors.red,
//         points: polylineCoordinates2,
//         width: 4);
//     polylines[id] = polyline2;
//     setState(() {});
//   }

//   _getPolyline2() async {
//     PolylineResult result2 = await polylinePoints2.getRouteBetweenCoordinates(
//       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//       PointLatLng(_originLatitude2, _originLongitude2),
//       PointLatLng(_destLatitude2, _destLongitude2),
//       travelMode: TravelMode.walking,
//     );
//     if (result2.points.isNotEmpty) {
//       result2.points.forEach((PointLatLng point2) {
//         polylineCoordinates2.add(LatLng(point2.latitude, point2.longitude));
//       });
//     }
//     _addPolyLine2();
//   }

// // third polyline
//   _addPolyLine3() {
//     PolylineId id = PolylineId("poly3");
//     Polyline polyline3 = Polyline(
//         polylineId: id,
//         color: Colors.blue,
//         points: polylineCoordinates3,
//         width: 4);
//     polylines[id] = polyline3;
//     setState(() {});
//   }

//   _getPolyline3() async {
//     PolylineResult result3 = await polylinePoints3.getRouteBetweenCoordinates(
//       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//       PointLatLng(_originLatitude3, _originLongitude3),
//       PointLatLng(_destLatitude3, _destLongitude3),
//       travelMode: TravelMode.walking,
//     );
//     if (result3.points.isNotEmpty) {
//       result3.points.forEach((PointLatLng point3) {
//         polylineCoordinates3.add(LatLng(point3.latitude, point3.longitude));
//       });
//     }
//     _addPolyLine3();
//   }
// }

// -----------------------------------------Geolocator+Polylines-24.07.20----------------------------------------------

import 'dart:async';

import 'package:flutter/material.dart';
// import 'package:flutterfreegenapp/9_profile_user_rewards.dart';
import 'package:flutterfreegenapp/geolocator_service.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';

class MyAppRoutes extends StatelessWidget {
  final geoService = GeolocatorService();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.orange[900],
        title: Container(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              width: 30,
              height: 30,
              child: IconButton(
                icon: Image.asset(
                  'assets/arrow2.png',
                  width: 11,
                  height: 11,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/6_main_layout');
                },
              ),
            ),
            Image.asset('assets/freegen-logo-white.png',
                width: 39, height: 34, color: Colors.white),
            Text(
              'FreeGen Reality',
              style: TextStyle(
                  fontSize: 28,
                  color: Colors.white,
                  fontFamily: 'ArialRoundedMTBold'),
            ),
            SizedBox(
              width: 30,
              height: 30,
            )
          ],
        )),
      ),
      body: Stack(children: <Widget>[
        FutureProvider(
          create: (context) => geoService.getInitialLocation(),
          child: MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Flutter Demo',
            theme: ThemeData(
              primarySwatch: Colors.blue,
            ),
            home: Consumer<Position>(
              builder: (context, position, widget) {
                return (position != null)
                    ? GMap(position)
                    : Center(child: CircularProgressIndicator());
              },
            ),
          ),
        ),
        Positioned.fill(
          bottom: 10,
          child: Align(
            alignment: Alignment.bottomCenter,
            child: SizedBox(
              height: 30,
              width: 150,
              child: RaisedButton(
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                  color: Colors.orange[900],
                  child: Text(
                    'Оновити маршрут',
                    style: TextStyle(
                        fontSize: 13,
                        color: Colors.white,
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w900),
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/google_maps_routes');
                  }),
            ),
          ),
        ),
      ]),
      bottomNavigationBar: Container(
        alignment: Alignment.topCenter,
        height: 83,
        decoration: BoxDecoration(color: Colors.white),
        child: Container(
          margin: const EdgeInsets.only(top: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_1.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/6_main_layout');
                  },
                ),
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_2.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/7_search');
                  },
                ),
              ),
              GestureDetector(
                child: Image.asset(
                  'assets/bottom_pic_3.png',
                  height: 42.0,
                  width: 63.0,
                ),
                onTap: () {
                  Navigator.of(context).pushNamed('/6_main_layout_camera');
                },
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_4.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '../screens/home_screen.dart');
                  },
                ),
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_5.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/9_profile');
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class GMap extends StatefulWidget {
  final Position initialPosition;

  GMap(this.initialPosition);

  @override
  State<StatefulWidget> createState() => _MapState();
}

class _MapState extends State<GMap> {
  final GeolocatorService geoService = GeolocatorService();
  Completer<GoogleMapController> _controller = Completer();

  //double _originLatitude = 50.450712, _originLongitude = 30.510529; // kiev //
  double _destLatitude = 50.448938, _destLongitude = 30.514434;

  double _originLatitude2 = 50.448857, _originLongitude2 = 30.514577;
  double _destLatitude2 = 50.446069, _destLongitude2 = 30.513515;

  double _originLatitude3 = 50.446021, _originLongitude3 = 30.513841;
  double _destLatitude3 = 50.445422, _destLongitude3 = 30.517384;

  Map<MarkerId, Marker> markers = {};
  Map<PolylineId, Polyline> polylines = {};
  List<LatLng> polylineCoordinates = [];
  List<LatLng> polylineCoordinates2 = [];
  List<LatLng> polylineCoordinates3 = [];
  PolylinePoints polylinePoints = PolylinePoints();
  PolylinePoints polylinePoints2 = PolylinePoints();
  PolylinePoints polylinePoints3 = PolylinePoints();
  String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

  @override
  void initState() {
    geoService.getCurrentLocation().listen((position) {    
      centerScreen(position);
    });
    super.initState();

    /// first route
    _addMarker(
        LatLng(widget.initialPosition.latitude, widget.initialPosition.longitude), // widget.initialPosition.latitude, widget.initialPosition.longitude _originLatitude, _originLongitude
        "origin",
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
        InfoWindow(title: 'Start point'));
    _addMarker(
      LatLng(_destLatitude, _destLongitude),
      "destination",
      BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
      InfoWindow(title: 'End point'),
    );
    _getPolyline();

    /// second route
    _addMarker(
        LatLng(_originLatitude2, _originLongitude2),
        "origin2",
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
        InfoWindow(title: 'Start point 2'));
    _addMarker(
        LatLng(_destLatitude2, _destLongitude2),
        "destination2",
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
        InfoWindow(title: 'End point 2'));
    _getPolyline2();

    /// third route
    _addMarker(
        LatLng(_originLatitude3, _originLongitude3),
        "origin3",
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
        InfoWindow(title: 'Start point 3'));
    _addMarker(
        LatLng(_destLatitude3, _destLongitude3),
        "destination3",
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
        InfoWindow(title: 'End point 3'));
    _getPolyline3();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
          GoogleMap(
        initialCameraPosition: CameraPosition(
            target: LatLng(widget.initialPosition.latitude,
                widget.initialPosition.longitude),
            zoom: 17.0),
        mapType: MapType.normal,
        tiltGesturesEnabled: true,
        compassEnabled: true,
        scrollGesturesEnabled: true,
        zoomGesturesEnabled: true,
        myLocationEnabled: true,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
        markers: Set<Marker>.of(markers.values),
        polylines: Set<Polyline>.of(polylines.values),
      ),
    );
  }

  _addMarker(LatLng position, String id, BitmapDescriptor descriptor,
      InfoWindow infowindow) {
    MarkerId markerId = MarkerId(id);
    Marker marker = Marker(
        markerId: markerId,
        icon: descriptor,
        position: position,
        infoWindow: infowindow);
    markers[markerId] = marker;
  }

  // first polyline
  _addPolyLine() {
    PolylineId id = PolylineId("poly");
    Polyline polyline = Polyline(
        polylineId: id,
        color: Colors.green,
        points: polylineCoordinates,
        width: 4);
    polylines[id] = polyline;
    setState(() {});
  }

  _getPolyline() async {
    PolylineResult result1 = await polylinePoints.getRouteBetweenCoordinates(
      "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
      PointLatLng(
          widget.initialPosition.latitude, widget.initialPosition.longitude),

      /// currentLocation.latitude, currentLocation.longitude
      PointLatLng(_destLatitude, _destLongitude),
      travelMode: TravelMode.walking,
      // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]
    );
    if (result1.points.isNotEmpty) {
      result1.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }
    _addPolyLine();
  }

// second polyline
  _addPolyLine2() {
    PolylineId id = PolylineId("poly2");
    Polyline polyline2 = Polyline(
        polylineId: id,
        color: Colors.red,
        points: polylineCoordinates2,
        width: 4);
    polylines[id] = polyline2;
    setState(() {});
  }

  _getPolyline2() async {
    PolylineResult result2 = await polylinePoints2.getRouteBetweenCoordinates(
      "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
      PointLatLng(_originLatitude2, _originLongitude2),
      PointLatLng(_destLatitude2, _destLongitude2),
      travelMode: TravelMode.walking,
    );
    if (result2.points.isNotEmpty) {
      result2.points.forEach((PointLatLng point2) {
        polylineCoordinates2.add(LatLng(point2.latitude, point2.longitude));
      });
    }
    _addPolyLine2();
  }

// third polyline
  _addPolyLine3() {
    PolylineId id = PolylineId("poly3");
    Polyline polyline3 = Polyline(
        polylineId: id,
        color: Colors.blue,
        points: polylineCoordinates3,
        width: 4);
    polylines[id] = polyline3;
    setState(() {});
  }

  _getPolyline3() async {
    PolylineResult result3 = await polylinePoints3.getRouteBetweenCoordinates(
      "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
      PointLatLng(_originLatitude3, _originLongitude3),
      PointLatLng(_destLatitude3, _destLongitude3),
      travelMode: TravelMode.walking,
    );
    if (result3.points.isNotEmpty) {
      result3.points.forEach((PointLatLng point3) {
        polylineCoordinates3.add(LatLng(point3.latitude, point3.longitude));
      });
    }
    _addPolyLine3();
  }
  
  Future<void> centerScreen(Position position) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(position.latitude, position.longitude), zoom: 17.0)));

    // void addReward(){
    //   if(position.latitude == _destLatitude && position.longitude == _destLongitude) {
    //     userRewards.add(reward4);
    //   }   
    // }
    // addReward();
  }
}
