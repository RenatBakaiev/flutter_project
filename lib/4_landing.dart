import 'package:flutter/material.dart';
import 'package:flutterfreegenapp/user.dart';
import 'package:provider/provider.dart';
import '5_login.dart';
import '6_main_layout.dart';


class LandingPage extends StatelessWidget {
  const LandingPage ({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final User user = Provider.of<User>(context);
    final bool isLoggedIn = user != null;
    
    return isLoggedIn ? MyListPage() : MyLoginPage();
  }
}