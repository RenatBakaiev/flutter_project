import './9_profile_model.dart';

final User user = User(
  id: '1000001495415',
  name: 'Влад Тіунов',
  imageUrl: 'assets/profile_foto.jpg',
  status: 'Explorer',
  tus: '24',
  foints: '9',
  skill: '807',
  tiki: '11',
  karma: '79',
  hip: '371',
  qr: 'assets/profile_code.jpg',
  qrCode: 'assets/profile_qr.png',
);

final User user2 = User(
  id: '1000001495411',
  name: 'Ренат Бакаєв',
  imageUrl: 'assets/profile_foto2.jpg',
  status: 'Explorer',
  tus: '23',
  foints: '5',
  skill: '954',
  tiki: '12',
  karma: '32',
  hip: '432',
  qr: 'assets/profile_code.jpg',
  qrCode: 'assets/profile_qr.png',
);

final Reward reward1 = Reward(
  id: 1,
  name: 'reward 1',
  imageUrl: 'assets/profile_item_1.png',
  description: 'закінчив 5 власних\nініціатив - 4/5',
);

final Reward reward2 = Reward(
  id: 2,
  name: 'reward 2',
  imageUrl: 'assets/profile_item_2.png',
  description: 'активний учасник\nсоц. акцій - 3/5',
);

final Reward reward3 = Reward(
  id: 3,
  name: 'reward 3',
  imageUrl: 'assets/profile_item_3.png',
  description: 'захисник\nприроди - 2/5',
);

final Reward reward4 = Reward(
  id: 4,
  name: 'reward 4',
  imageUrl: 'assets/profile_item_4.png',
  description: 'подолав 20 км.\nна вейку - 2/5',
);

final Reward reward5 = Reward(
  id: 5,
  name: 'reward 5',
  imageUrl: 'assets/profile_item_5.png',
  description: 'воїн соціальних\nмереж 3/5',
);

final Reward reward6 = Reward(
  id: 6,
  name: 'reward 6',
  imageUrl: 'assets/profile_item_6.png',
  description: 'лідер політичного\nруху 3/5',
);

final Reward reward7 = Reward(
  id: 7,
  name: 'reward 7',
  imageUrl: 'assets/profile_item_7.png',
  description: 'відвідав\n30 івентів - 1/5',
);

final Reward reward8 = Reward(
  id: 8,
  name: 'reward 8',
  imageUrl: 'assets/profile_item_8.png',
  description: 'адепт руху\nморжування - 3/5',
);

List<Reward> allRewards = [
  reward1,
  reward2,
  reward3,
  reward4,
  reward5,
  reward6,
  reward7,
  reward8,
];

List<Reward> userRewards = [
  reward1,
  reward2,
  reward3,
  reward4,
  reward5,
  reward6,
  reward7,
  reward8,
  reward1,
  reward2,
  reward3,
];


