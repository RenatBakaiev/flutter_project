import 'package:flutter/material.dart';
import './6_quest.model.dart';

final Quest quest1 = Quest(
  id: 1,
  name: 'Quest 1: Camera',
  imageUrl: 'assets/game2.png',
  nameBackColor: Colors.yellow,
  imageUrlBackColor: Colors.yellow[200],
  quest: '/6_main_layout_camera',
);
final Quest quest2 = Quest(
  id: 2,
  name: 'Quest 2: GM',
  imageUrl: 'assets/game2.png',
  nameBackColor: Colors.red,
  imageUrlBackColor: Colors.red[200],
  quest: '/location',
);
final Quest quest3 = Quest(
  id: 3,
  name: 'Quest 3: Routes',
  imageUrl: 'assets/game2.png',
  nameBackColor: Colors.blue,
  imageUrlBackColor: Colors.blue[200],
  quest: '/google_maps_routes',
);
final Quest quest4 = Quest(
  id: 4,
  name: 'Quest 4: AR',
  imageUrl: 'assets/game2.png',
  nameBackColor: Colors.green,
  imageUrlBackColor: Colors.green[200],
  quest: '/8_ar',
);
final Quest quest5 = Quest(
  id: 5,
  name: 'Quest 5',
  imageUrl: 'assets/game2.png',
  nameBackColor: Colors.grey,
  imageUrlBackColor: Colors.grey[200],
  quest: '/',
);
final Quest quest6 = Quest(
  id: 6,
  name: 'Quest 6',
  imageUrl: 'assets/game2.png',
  nameBackColor: Colors.pink,
  imageUrlBackColor: Colors.pink[200],
  quest: '/',
);
final Quest quest7 = Quest(
  id: 7,
  name: 'Quest 7',
  imageUrl: 'assets/game2.png',
  nameBackColor: Colors.purple,
  imageUrlBackColor: Colors.purple[200],
  quest: '/',
);
final Quest quest8 = Quest(
  id: 8,
  name: 'Quest 8',
  imageUrl: 'assets/game2.png',
  nameBackColor: Colors.teal,
  imageUrlBackColor: Colors.teal[200],
  quest: '/',
);
final Quest quest9 = Quest(
  id: 9,
  name: 'Quest 9',
  imageUrl: 'assets/game2.png',
  nameBackColor: Colors.brown,
  imageUrlBackColor: Colors.brown[200],
  quest: '/',
);
final Quest quest10 = Quest(
  id: 10,
  name: 'Quest 10',
  imageUrl: 'assets/game2.png',
  nameBackColor: Colors.lime,
  imageUrlBackColor: Colors.lime[200],
  quest: '/',
);

List<Quest> allQuests = [
  quest1,
  quest2,
  quest3,
  quest4,
  quest5,
  quest6,
  quest7,
  quest8,
  quest9,
  quest10,
];
List<Quest> nearQuests = [
  quest7,
  quest8,
  quest3,
  quest2,
];
List<Quest> popularQuests = [
  quest1,
  quest2,
  quest3,
  quest4,
  quest9,
  quest10,
];

List<Quest> questsForDisplay = allQuests;
