import 'package:flutter/material.dart';
import 'package:flutterfreegenapp/services/auth.dart';
import '6_quests.dart';
// import 'package:http/http.dart';
// import 'dart:convert';

class MyApp6 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange[900],
      body: MyListPage(),
    );
  }
}

class MyListPage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyListPage> {
  final CategoriesScroller categoriesScroller = CategoriesScroller();

  // void getData() async {

  //   Response response = await get('https://jsonplaceholder.typicode.com/todos/1');
  //   Map data = jsonDecode(response.body);
  //   print(data);
  //   print(data['title']);
  // }

  // @override
  // void initState(){
  //   super.initState();
  //   getData();
  // }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          automaticallyImplyLeading: false,
          backgroundColor: Colors.orange[900],
          title: Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                width: 30,
                height: 30,
                child: IconButton(
                  icon: Image.asset(
                    'assets/arrow2.png',
                    width: 11,
                    height: 11,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/5_login');
                  },
                ),
              ),
              // GestureDetector(
              //   child: SizedBox(
              //     width: 11,
              //     height: 11,
              //                     child: Image.asset(
              //       'assets/arrow2.png',
              //       width: 11,
              //       height: 11,
              //     ),
              //   ),
              //   onTap: () {
              //     Navigator.pushNamed(context, '/5_login');
              //   },
              // ),
              Image.asset('assets/freegen-logo-white.png',
                  width: 39, height: 34, color: Colors.white),
              Text(
                'FreeGen Reality',
                style: TextStyle(
                    fontSize: 28,
                    color: Colors.white,
                    fontFamily: 'ArialRoundedMTBold'),
              ),
              GestureDetector(
                  onTap: () {
                    AuthService().logOut();
                  },
                  child:
                      Icon(Icons.supervised_user_circle, color: Colors.white)),
            ],
          )),
        ),
        body: Container(
          padding: EdgeInsets.only(left: 10, right: 10),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.orange[900], Colors.white])),
          height: size.height,
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 7, bottom: 7),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Популярні',
                      style: TextStyle(
                          fontSize: 24,
                          color: Colors.white,
                          fontFamily: 'Comfortaa',
                          fontWeight: FontWeight.w700),
                    )
                  ],
                ),
              ),
              Container(height: 125, child: categoriesScroller),
              Container(
                margin: EdgeInsets.only(top: 7, bottom: 7),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Поруч',
                      style: TextStyle(
                          fontSize: 24,
                          color: Colors.white,
                          fontFamily: 'Comfortaa',
                          fontWeight: FontWeight.w700),
                    )
                  ],
                ),
              ),
              Expanded(
                child: ListView.builder(
                  physics: BouncingScrollPhysics(),
                  itemCount: nearQuests.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, nearQuests[index].quest);
                      },
                      child: Container(
                          child: Column(
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(top: 10.0),
                            alignment: Alignment.center,
                            height: 137,
                            width: double.infinity,
                            child: Image.asset(nearQuests[index].imageUrl,
                                width: 108, height: 94),
                            decoration: BoxDecoration(
                              color: nearQuests[index]
                                  .imageUrlBackColor, //color: Colors.pink[600],
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10.0),
                                  topLeft: Radius.circular(10.0)),
                            ),
                          ),
                          Container(
                            alignment: Alignment.center,
                            height: 38,
                            width: double.infinity,
                            child: Text(
                              nearQuests[index].name,
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                  fontFamily: 'Comfortaa',
                                  fontWeight: FontWeight.w800),
                            ),
                            decoration: BoxDecoration(
                              color: nearQuests[index]
                                  .nameBackColor, //color: Colors.cyan[300],
                              borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(10.0),
                                  bottomLeft: Radius.circular(10.0)),
                            ),
                          )
                        ],
                      )),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: Container(
          alignment: Alignment.topCenter,
          height: 83,
          decoration: BoxDecoration(color: Colors.white),
          child: Container(
            margin: const EdgeInsets.only(top: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                SizedBox(
                  width: 35,
                  height: 35,
                  child: IconButton(
                    icon: Image.asset(
                      'assets/bottom_pic_1.png',
                      width: 16,
                      height: 16,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/6_main_layout');
                    },
                  ),
                ),
                SizedBox(
                  width: 35,
                  height: 35,
                  child: IconButton(
                    icon: Image.asset(
                      'assets/bottom_pic_2.png',
                      width: 16,
                      height: 16,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/7_search');
                    },
                  ),
                ),
                GestureDetector(
                  child: Image.asset(
                    'assets/bottom_pic_3.png',
                    height: 42.0,
                    width: 63.0,
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed('/6_main_layout_camera');
                  },
                ),
                SizedBox(
                  width: 35,
                  height: 35,
                  child: IconButton(
                    icon: Image.asset(
                      'assets/bottom_pic_4.png',
                      width: 16,
                      height: 16,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(
                          context, '../screens/home_screen.dart');
                    },
                  ),
                ),
                SizedBox(
                  width: 35,
                  height: 35,
                  child: IconButton(
                    icon: Image.asset(
                      'assets/bottom_pic_5.png',
                      width: 16,
                      height: 16,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/9_profile');
                    },
                  ),
                ),
                
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class CategoriesScroller extends StatelessWidget {
  const CategoriesScroller();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                physics: BouncingScrollPhysics(),
                itemCount: popularQuests.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, popularQuests[index].quest);
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 10),
                      child: Column(
                        children: <Widget>[
                          Container(
                            alignment: Alignment.center,
                            height: 87,
                            width: 125,
                            child: Image.asset(popularQuests[index].imageUrl,
                                width: 39, height: 34),
                            decoration: BoxDecoration(
                              color: popularQuests[index]
                                  .imageUrlBackColor, //Colors.red[600]
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10.0),
                                  topLeft: Radius.circular(10.0)),
                            ),
                          ),
                          Container(
                            alignment: Alignment.center,
                            height: 38,
                            width: 125,
                            child: Text(
                              popularQuests[index].name,
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                  fontFamily: 'Comfortaa',
                                  fontWeight: FontWeight.w800),
                            ),
                            decoration: BoxDecoration(
                              color: popularQuests[index]
                                  .nameBackColor, //Colors.cyan[300]
                              borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(10.0),
                                  bottomLeft: Radius.circular(10.0)),
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                }),
          ),
        ),
      ],
    );
  }
}

                // begin bottom 

                // GestureDetector(
                //   child: Image.asset(
                //     'assets/bottom_pic_1.png',
                //     height: 15.0,
                //     width: 14.0,
                //   ),
                //   onTap: () {
                //     Navigator.pushNamed(context, '/6_main_layout');
                //   },
                // ),

                // GestureDetector(
                //   child: Image.asset(
                //     'assets/bottom_pic_2.png',
                //     height: 16.0,
                //     width: 16.0,
                //   ),
                //   onTap: () {
                //     Navigator.pushNamed(context, '/7_search');
                //   },
                // ),

                // GestureDetector(
                //   child: Image.asset(
                //     'assets/bottom_pic_3.png',
                //     height: 42.0,
                //     width: 63.0,
                //   ),
                //   onTap: () {
                //     Navigator.of(context).pushNamed('/6_main_layout_camera');
                //   },
                // ),

                // GestureDetector(
                //   child: Image.asset(
                //     'assets/bottom_pic_4.png',
                //     height: 16.0,
                //     width: 16.0,
                //   ),
                //   onTap: () {
                //     Navigator.pushNamed(context, '../screens/home_screen.dart');
                //   },
                // ),

                // GestureDetector(
                //   child: Image.asset(
                //     'assets/bottom_pic_5.png',
                //     height: 15.0,
                //     width: 11.0,
                //   ),
                //   onTap: () {
                //     Navigator.pushNamed(context, '/9_profile');
                //   },
                // ),

