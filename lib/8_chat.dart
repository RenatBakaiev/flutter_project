import 'package:flutter/material.dart';

class MyApp100 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var _blankFocusNode = new FocusNode();

    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.orange[900],
        title: Container(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            GestureDetector(
              child: Image.asset('assets/arrow2.png'),
              onTap: () {
                Navigator.pushNamed(context, '/6_main_layout');
              },
            ),
            Image.asset('assets/freegen-logo-white.png', color: Colors.white),
            Text(
              'FreeGen Reality',
              style: TextStyle(
                  fontSize: 28,
                  color: Colors.white,
                  fontFamily: 'ArialRoundedMTBold'),
            ),
            SizedBox(
              width: 11,
            )
          ],
        )),
      ),
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(_blankFocusNode);
        },
        child: Container(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 2.0, color: Colors.grey),
                    ),
                  ),
                  height: 130,
                  width: double.infinity,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          child: Container(
                              margin: const EdgeInsets.only(top: 3.0),
                              width: 90,
                              height: 90,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: Image.asset('assets/chat_foto.jpg')
                                          .image,
                                      fit: BoxFit.cover),
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color: Colors.white, width: 8)))),
                      Text(
                        'Renat Bakaiev',
                        style: TextStyle(
                            fontSize: 13,
                            color: Colors.white,
                            fontFamily: 'Comfortaa',
                            fontWeight: FontWeight.w700),
                      ),
                      Container(
                        margin: const EdgeInsets.only(bottom: 3.0),
                        child: Text(
                          'Explorer',
                          style: TextStyle(
                              fontSize: 10,
                              color: Colors.white,
                              fontFamily: 'Comfortaa',
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(),
                Container(
                    margin: const EdgeInsets.only(bottom: 5.0),
                    height: 44,
                    color: Colors.white,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        GestureDetector(
                          child: Image.asset('assets/chat_camera.png',
                              height: 32, width: 24),
                          onTap: () {
                            Navigator.of(context)
                                .pushNamed('/6_main_layout_camera');
                          },
                        ),
                        GestureDetector(
                          child: Image.asset('assets/chat_icon.png',
                              height: 34, width: 26),
                          onTap: () {},
                        ),
                      ],
                    ))
              ]),
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.orange[900], Colors.white])),
        ),
      ),
      bottomNavigationBar: Container(
        alignment: Alignment.topCenter,
        height: 83,
        decoration: BoxDecoration(color: Colors.white),
        child: Container(
          margin: const EdgeInsets.only(top: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              GestureDetector(
                child: Image.asset(
                  'assets/bottom_pic_1.png',
                  height: 15.0,
                  width: 14.0,
                ),
                onTap: () {
                  Navigator.pushNamed(context, '/6_main_layout');
                },
              ),
              GestureDetector(
                child: Image.asset(
                  'assets/bottom_pic_2.png',
                  height: 16.0,
                  width: 16.0,
                ),
                onTap: () {
                  Navigator.pushNamed(context, '/7_search');
                },
              ),
              GestureDetector(
                child: Image.asset(
                  'assets/bottom_pic_3.png',
                  height: 42.0,
                  width: 63.0,
                ),
                onTap: () {
                  Navigator.pushNamed(context, '/6_main_layout_camera');
                },
              ),
              GestureDetector(
                child: Image.asset(
                  'assets/bottom_pic_4.png',
                  height: 16.0,
                  width: 16.0,
                ),
                onTap: () {
                  Navigator.pushNamed(context, '/8_chat');
                },
              ),
              GestureDetector(
                child: Image.asset(
                  'assets/bottom_pic_5.png',
                  height: 15.0,
                  width: 11.0,
                ),
                onTap: () {
                  Navigator.pushNamed(context, '/9_profile');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
