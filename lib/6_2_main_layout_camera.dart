import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'dart:async';
import 'dart:io';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';

class MyApp62 extends StatefulWidget {
  final CameraDescription camera;
  const MyApp62({
    Key key,
    @required this.camera,
  }) : super(key: key);
  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<MyApp62> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;
  @override
  void initState() {
    super.initState();
    _controller = CameraController(
      widget.camera,
      ResolutionPreset.medium,
    );
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.orange[900],
        title: Container(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              width: 30,
              height: 30,
              child: IconButton(
                icon: Image.asset(
                  'assets/arrow2.png',
                  width: 11,
                  height: 11,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/6_main_layout');
                },
              ),
            ),
            // GestureDetector(
            //   child: Image.asset('assets/arrow2.png', width: 11, height: 11),
            //   onTap: () {
            //     Navigator.pushNamed(context, '/6_main_layout');
            //   },
            // ),
            Image.asset('assets/freegen-logo-white.png',
                width: 39, height: 34, color: Colors.white),
            Text(
              'FreeGen Reality',
              style: TextStyle(
                  fontSize: 28,
                  color: Colors.white,
                  fontFamily: 'ArialRoundedMTBold'),
            ),
            SizedBox(
              width: 30,
              height: 30,
              child: IconButton(
                icon: Image.asset(
                  'assets/3dots.png',
                  width: 2.6,
                  height: 17,
                ),
                onPressed: () {},
              ),
            ),
            // GestureDetector(
            //   child: Image.asset('assets/3dots.png', width: 2.6, height: 17),
            //   onTap: () {},
            // ),
          ],
        )),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: FutureBuilder<void>(
          future: _initializeControllerFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return Stack(
                children: <Widget>[
                  CameraPreview(_controller),
                  new Positioned.fill(
                      child: Opacity(
                    opacity: 0.3,
                    child: Container(
                      alignment: Alignment.center,
                      child: Image.asset(
                        'assets/login_start.png',
                        width: 150,
                        height: 150,
                      ),
                    ),
                  ))
                ],
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
      bottomNavigationBar: Container(
        alignment: Alignment.topCenter,
        height: 83,
        decoration: BoxDecoration(color: Colors.white),
        child: Container(
          margin: const EdgeInsets.only(top: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_1.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/6_main_layout');
                  },
                ),
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_2.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/7_search');
                  },
                ),
              ),
              GestureDetector(
                child: Image.asset(
                  'assets/bottom_pic_3.png',
                  height: 42.0,
                  width: 63.0,
                ),
                onTap: () async {
                  try {
                    await _initializeControllerFuture;
                    final path = join(
                      (await getTemporaryDirectory()).path,
                      '${DateTime.now()}.png',
                    );
                    await _controller.takePicture(path);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            DisplayPictureScreen(imagePath: path),
                      ),
                    );
                  } catch (e) {
                    print(e);
                  }
                },
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_4.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '../screens/home_screen.dart');
                  },
                ),
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_5.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/9_profile');
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;

  const DisplayPictureScreen({Key key, this.imagePath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.orange[900],
        title: Container(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              width: 30,
              height: 30,
              child: IconButton(
                icon: Image.asset(
                  'assets/arrow2.png',
                  width: 11,
                  height: 11,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/6_main_layout_camera');
                },
              ),
            ),
            // GestureDetector(
            //   child: Image.asset('assets/arrow2.png', width: 11, height: 11,),
            //   onTap: () {
            //     Navigator.pushNamed(context, '/6_main_layout_camera');
            //   },
            // ),
            Image.asset('assets/freegen-logo-white.png',
                width: 39, height: 34, color: Colors.white),
            Text(
              'FreeGen Reality',
              style: TextStyle(
                  fontSize: 28,
                  color: Colors.white,
                  fontFamily: 'ArialRoundedMTBold'),
            ),
            SizedBox(
              width: 30,
              height: 30,
              child: IconButton(
                icon: Image.asset(
                  'assets/3dots.png',
                  width: 2.6,
                  height: 17,
                ),
                onPressed: () {},
              ),
            ),
            // GestureDetector(
            //   child: Image.asset('assets/3dots.png', width: 2.6, height: 17,),
            //   onTap: () {},
            // ),
          ],
        )),
      ),
      body: Container(
          child: Image.file(
        File(imagePath),
        fit: BoxFit.contain,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
      )),
    );
  }
}
