import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
import 'package:flutter/material.dart';
import 'package:vector_math/vector_math_64.dart' as vector;

class MyApp7 extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyApp7> {
  ArCoreController arCoreController;

  void _onArCoreViewCreated(ArCoreController _arcoreController) {
    arCoreController = _arcoreController;

    _addSphere(arCoreController);
    _addCylindre(arCoreController);
    _addCube(arCoreController);
  }

  // void _addSphere(ArCoreController controller) {
  //   final material = ArCoreMaterial(
  //       color: Color.fromARGB(120, 66, 134, 244), texture: "earth.jpg");
  //   final sphere = ArCoreSphere(
  //     materials: [material],
  //     radius: 0.1,
  //   );
  //   final node = ArCoreNode(
  //     shape: sphere,
  //     position: vector.Vector3(0, 0, -1.5),
  //   );
  //   controller.addArCoreNode(node);
  // }

  void _addSphere(ArCoreController controller) {
    final material = ArCoreMaterial(color: Colors.orange[900]);
    final sphere = ArCoreSphere(
      materials: [material],
      radius: 0.2,
    );
    final node = ArCoreNode(
      shape: sphere,
      position: vector.Vector3(0, 0, -3.0),
    );
    controller.addArCoreNode(node);
  }

  void _addCylindre(ArCoreController controller) {
    final material = ArCoreMaterial(
      color: Colors.red,
      reflectance: 1.0,
    );
    final cylindre = ArCoreCylinder(
      materials: [material],
      radius: 0.5,
      height: 0.3,
    );
    final node = ArCoreNode(
      shape: cylindre,
      position: vector.Vector3(0.0, -0.5, -2.0),
    );
    controller.addArCoreNode(node);
  }

  void _addCube(ArCoreController controller) {
    final material = ArCoreMaterial(
      color: Colors.greenAccent,
      metallic: 1.0,
    );
    final cube = ArCoreCube(
      materials: [material],
      size: vector.Vector3(0.5, 0.5, 0.5),
    );
    final node = ArCoreNode(
      shape: cube,
      position: vector.Vector3(-0.5, 0.5, -3.5),
    );
    controller.addArCoreNode(node);
  }

  @override
  void dispose() {
    arCoreController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.orange[900],
          title: Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                width: 30,
                height: 30,
                child: IconButton(
                  icon: Image.asset(
                    'assets/arrow2.png',
                    width: 11,
                    height: 11,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/6_main_layout');
                  },
                ),
              ),

              // GestureDetector(
              //   child: SizedBox(
              //     width: 11,
              //     height: 11,
              //     child: Image.asset('assets/arrow2.png', width: 11, height: 11,)),
              //   onTap: () {
              //     Navigator.pushNamed(context, '/6_main_layout');
              //   },
              // ),
              Image.asset('assets/freegen-logo-white.png',
                  width: 39, height: 34, color: Colors.white),
              Text(
                'FreeGen Reality',
                style: TextStyle(
                    fontSize: 28,
                    color: Colors.white,
                    fontFamily: 'ArialRoundedMTBold'),
              ),
              SizedBox(
                width: 30,
                height: 30,
              )
            ],
          )),
        ),
        body:
            // Opacity(
            //   opacity: 0.5,
            //   child:
            Container(
          // alignment: Alignment.center,
          // decoration: BoxDecoration(
          //     image: DecorationImage(
          //   image: AssetImage('assets/login_start.png'),
          // )),
          child: ArCoreView(
            onArCoreViewCreated: _onArCoreViewCreated,
            enableTapRecognizer: true,
          ),
        ),
        // ),
        bottomNavigationBar: Container(
          alignment: Alignment.topCenter,
          height: 83,
          decoration: BoxDecoration(color: Colors.white),
          child: Container(
            margin: const EdgeInsets.only(top: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                SizedBox(
                  width: 35,
                  height: 35,
                  child: IconButton(
                    icon: Image.asset(
                      'assets/bottom_pic_1.png',
                      width: 16,
                      height: 16,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/6_main_layout');
                    },
                  ),
                ),
                SizedBox(
                  width: 35,
                  height: 35,
                  child: IconButton(
                    icon: Image.asset(
                      'assets/bottom_pic_2.png',
                      width: 16,
                      height: 16,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/7_search');
                    },
                  ),
                ),
                GestureDetector(
                  child: Image.asset(
                    'assets/bottom_pic_3.png',
                    height: 42.0,
                    width: 63.0,
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed('/6_main_layout_camera');
                  },
                ),
                SizedBox(
                  width: 35,
                  height: 35,
                  child: IconButton(
                    icon: Image.asset(
                      'assets/bottom_pic_4.png',
                      width: 16,
                      height: 16,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(
                          context, '../screens/home_screen.dart');
                    },
                  ),
                ),
                SizedBox(
                  width: 35,
                  height: 35,
                  child: IconButton(
                    icon: Image.asset(
                      'assets/bottom_pic_5.png',
                      width: 16,
                      height: 16,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/9_profile');
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
