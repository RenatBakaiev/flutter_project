import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.orange[900],
          title: Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                width: 30,
                height: 30,
                child: IconButton(
                  icon: Image.asset(
                    'assets/arrow.png',
                    width: 11,
                    height: 11,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/2_start');
                  },
                ),
              ),

              // GestureDetector(
              //   child: SizedBox(
              //       width: 11,
              //       height: 11,
              //       child: Image.asset(
              //         'assets/arrow.png',
              //         width: 11,
              //         height: 11,
              //       )),
              //   onTap: () {
              //     Navigator.pushNamed(context, '/2_start');
              //   },
              // ),

              Image.asset('assets/freegen-logo-white.png',
                  width: 39, height: 34, color: Colors.white),
              Text(
                'FreeGen Reality',
                style: TextStyle(
                    fontSize: 28,
                    color: Colors.white,
                    fontFamily: 'ArialRoundedMTBold'),
              ),

              SizedBox(
                width: 30,
                height: 30,
                child: IconButton(
                  icon: Image.asset(
                    'assets/close.png',
                    width: 11,
                    height: 11,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/google_maps_routes');
                  },
                ),
              ),

              // GestureDetector(
              //   child: SizedBox(
              //       width: 11,
              //       height: 11,
              //       child: Image.asset(
              //         'assets/close.png',
              //         width: 11,
              //         height: 11,
              //       )),
              //   onTap: () {
              //     Navigator.pushNamed(context, '/');
              //   },
              // ),
            ],
          )),
        ),
        body: Container(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/1.start_pic.jpg', width: 420),
                Container(
                  margin: const EdgeInsets.only(bottom: 5.0, top: 10),
                  child: Text(
                    'Самостiйнi та груповi\nквести або екскурсii\nпростонеба!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 22,
                        color: Colors.black,
                        fontFamily: 'Comfortaa',
                        fontWeight: FontWeight.w900),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 10.0, top: 5),
                  child: Text(
                    'Перетвори свiй телефон в\nперсонального гiда, який проведе\nекскурсiю, або квест-гру по цiкавим\nмiсцям. Просто обери, або вподобай\nквест чи прогулянку та почни в\nбудь-який час.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        fontFamily: 'Comfortaa',
                        fontWeight: FontWeight.w500),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 35.0),
                  child: SizedBox(
                    width: 237,
                    height: 50,
                    child: RaisedButton(
                      color: Colors.orange[900],
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(25.0)),
                      onPressed: () {
                        Navigator.pushNamed(context, '/2_start');
                      },
                      child: Text(
                        'ДАЛI',
                        style: TextStyle(
                            fontSize: 13,
                            color: Colors.white,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w900),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.orange[900], Colors.white])),
        ),
      ),
    );
  }
}
