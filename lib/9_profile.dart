// import 'package:flutter/material.dart';
// import '9_profile_user_rewards.dart';
// // import 'google_maps_routes.dart';

// class MyApp10 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: MyHomePage(),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   MyHomePage({Key key, this.title}) : super(key: key);
//   final String title;
  
//   @override
//   _MyHomePageState createState() => _MyHomePageState();
  
// }

// class _MyHomePageState extends State<MyHomePage> {
//   final id = int;
//   // final bool marker = true;

//   @override
//   void initState() {    
//     super.initState();  
//   }

//   @override
//   Widget build(BuildContext context) {

//     // addReward(){
//     //   if(this.marker == true) {
//     //     userRewards.add(reward4);
//     //     setState(() {
          
//     //     });
//     //   }
//     // }
//     // addReward();

//     return Scaffold(
//       appBar: AppBar(
//         elevation: 0.0,
//         automaticallyImplyLeading: false,
//         backgroundColor: Colors.orange[900],
//         title: Container(
//             child: Row(
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: <Widget>[
//             SizedBox(
//               width: 30,
//               height: 30,
//               child: IconButton(
//                 icon: Image.asset(
//                   'assets/arrow2.png',
//                   width: 11,
//                   height: 11,
//                 ),
//                 onPressed: () {
//                   Navigator.pushNamed(context, '/6_main_layout');
//                 },
//               ),
//             ),
//             // GestureDetector(
//             //   child: SizedBox(
//             //     width: 11,
//             //     height: 11,
//             //     child: Image.asset(
//             //       'assets/arrow2.png',
//             //       width: 11,
//             //       height: 11,
//             //     ),
//             //   ),
//             //   onTap: () {
//             //     Navigator.pushNamed(context, '/6_main_layout');
//             //   },
//             // ),
//             Image.asset('assets/freegen-logo-white.png',
//                 width: 39, height: 34, color: Colors.white),
//             Text(
//               'FreeGen Reality',
//               style: TextStyle(
//                   fontSize: 28,
//                   color: Colors.white,
//                   fontFamily: 'ArialRoundedMTBold'),
//             ),
//             SizedBox(
//               width: 30,
//               height: 30,
//               child: IconButton(
//                 icon: Image.asset(
//                   'assets/3dots.png',
//                   width: 2.6,
//                   height: 17,
//                 ),
//                 onPressed: () {},
//               ),
//             ),
//             // GestureDetector(
//             //   child: SizedBox(
//             //       width: 11,
//             //       height: 17,
//             //       child: Image.asset(
//             //         'assets/3dots.png',
//             //         width: 2.6,
//             //         height: 17,
//             //       )),
//             //   onTap: () {},
//             // ),
//           ],
//         )),
//       ),
//       body: Container(
//           height: double.infinity,
//           width: double.infinity,
//           decoration: BoxDecoration(
//               gradient: LinearGradient(
//                   begin: Alignment.topCenter,
//                   end: Alignment.bottomCenter,
//                   colors: [Colors.orange[900], Colors.white])),
//           child: SingleChildScrollView(
//             physics: BouncingScrollPhysics(),
//             child: Column(
//                 mainAxisAlignment: MainAxisAlignment.start,
//                 children: <Widget>[
//                   Container(
//                       margin: EdgeInsets.only(top: 80.0, left: 22, right: 22),
//                       width: double.infinity,
//                       height: 250,
//                       decoration: BoxDecoration(
//                           color: Colors.white,
//                           borderRadius: BorderRadius.only(
//                             topLeft: Radius.circular(10.0),
//                             topRight: Radius.circular(10.0),
//                             bottomLeft: Radius.circular(10.0),
//                             bottomRight: Radius.circular(10.0),
//                           )),
//                       child: Stack(
//                         overflow: Overflow.visible,
//                         alignment: Alignment.topCenter,
//                         children: <Widget>[
//                           Positioned(
//                             top: -75,
//                             child: Container(
//                               height: 210,
//                               child: Column(
//                                 children: <Widget>[
//                                   Column(
//                                     children: <Widget>[
//                                       GestureDetector(
//                                         onTap: () {
//                                           Navigator.pushNamed(
//                                               context, '/10_profile_qr');
//                                         },
//                                         child: CircleAvatar(
//                                           radius: 75,
//                                           backgroundColor: Colors.white,
//                                           child: CircleAvatar(
//                                             radius: 70,
//                                             backgroundImage:
//                                                 AssetImage(user.imageUrl),
//                                           ),
//                                         ),
//                                       ),
//                                       SizedBox(height: 5.0),
//                                       Text(
//                                         user.name,
//                                         style: TextStyle(
//                                             fontSize: 24,
//                                             color: Colors.black,
//                                             fontFamily: 'Comfortaa',
//                                             fontWeight: FontWeight.bold),
//                                       ),
//                                       SizedBox(height: 5.0),
//                                       Text(
//                                         user.status,
//                                         style: TextStyle(
//                                             fontSize: 14,
//                                             color: Colors.grey[400],
//                                             fontFamily: 'Comfortaa',
//                                             fontWeight: FontWeight.w400),
//                                       ),
//                                       SizedBox(height: 5.0),
//                                     ],
//                                   ),
//                                 ],
//                               ),
//                             ),
//                           ),
//                           Positioned(
//                             bottom: 75,
//                             child: Container(
//                               // color: Colors.red,
//                               width: 360,
//                               child: Row(
//                                 mainAxisAlignment:
//                                     MainAxisAlignment.spaceAround,
//                                 children: <Widget>[
//                                   Container(
//                                     width: 120,
//                                     child: Column(children: <Widget>[
//                                       Text(user.tus,
//                                           style: TextStyle(
//                                               color: Colors.black,
//                                               fontSize: 24.0,
//                                               fontFamily: 'Comfortaa',
//                                               fontWeight: FontWeight.normal)),
//                                       SizedBox(
//                                         height: 5,
//                                       ),
//                                       Text('TUS',
//                                           style: TextStyle(
//                                               color: Colors.grey[400],
//                                               fontSize: 10.0,
//                                               fontFamily: 'Comfortaa',
//                                               fontWeight: FontWeight.normal)),
//                                     ]),
//                                   ),
//                                   Container(
//                                     width: 120,
//                                     decoration: BoxDecoration(
//                                       border: Border(
//                                         left: BorderSide(
//                                             width: 1.0,
//                                             color: Colors.grey[400]),
//                                         right: BorderSide(
//                                             width: 1.0,
//                                             color: Colors.grey[400]),
//                                       ),
//                                     ),
//                                     child: Column(children: <Widget>[
//                                       Text(user.foints,
//                                           style: TextStyle(
//                                               color: Colors.orange[900],
//                                               fontSize: 24.0,
//                                               fontFamily: 'Comfortaa',
//                                               fontWeight: FontWeight.bold)),
//                                       SizedBox(
//                                         height: 5,
//                                       ),
//                                       Text('FOINTS',
//                                           style: TextStyle(
//                                               color: Colors.orange[900],
//                                               fontSize: 10.0,
//                                               fontFamily: 'Comfortaa',
//                                               fontWeight: FontWeight.normal)),
//                                     ]),
//                                   ),
//                                   Container(
//                                     width: 120,
//                                     child: Column(children: <Widget>[
//                                       Text(user.skill,
//                                           style: TextStyle(
//                                               color: Colors.black,
//                                               fontSize: 24.0,
//                                               fontFamily: 'Comfortaa',
//                                               fontWeight: FontWeight.normal)),
//                                       SizedBox(
//                                         height: 5,
//                                       ),
//                                       Text('SKILL',
//                                           style: TextStyle(
//                                               color: Colors.grey[400],
//                                               fontSize: 10.0,
//                                               fontFamily: 'Comfortaa',
//                                               fontWeight: FontWeight.normal)),
//                                     ]),
//                                   )
//                                 ],
//                               ),
//                             ),
//                           ),
//                           Positioned(
//                             bottom: 20,
//                             child: Container(
//                               // color: Colors.red,
//                               width: 360,
//                               child: Row(
//                                 mainAxisAlignment:
//                                     MainAxisAlignment.spaceAround,
//                                 children: <Widget>[
//                                   Container(
//                                     width: 120,
//                                     child: Column(children: <Widget>[
//                                       Text(user.tiki,
//                                           style: TextStyle(
//                                               color: Colors.black,
//                                               fontSize: 24.0,
//                                               fontFamily: 'Comfortaa',
//                                               fontWeight: FontWeight.normal)),
//                                       SizedBox(
//                                         height: 5,
//                                       ),
//                                       Text('TIKI',
//                                           style: TextStyle(
//                                               color: Colors.grey[400],
//                                               fontSize: 10.0,
//                                               fontFamily: 'Comfortaa',
//                                               fontWeight: FontWeight.normal)),
//                                     ]),
//                                   ),
//                                   Container(
//                                     width: 120,
//                                     decoration: BoxDecoration(
//                                       border: Border(
//                                         left: BorderSide(
//                                             width: 1.0,
//                                             color: Colors.grey[400]),
//                                         right: BorderSide(
//                                             width: 1.0,
//                                             color: Colors.grey[400]),
//                                       ),
//                                     ),
//                                     child: Column(children: <Widget>[
//                                       Text(user.karma,
//                                           style: TextStyle(
//                                               color: Colors.black,
//                                               fontSize: 24.0,
//                                               fontFamily: 'Comfortaa',
//                                               fontWeight: FontWeight.normal)),
//                                       SizedBox(
//                                         height: 5,
//                                       ),
//                                       Text('KARMA',
//                                           style: TextStyle(
//                                               color: Colors.grey[400],
//                                               fontSize: 10.0,
//                                               fontFamily: 'Comfortaa',
//                                               fontWeight: FontWeight.normal)),
//                                     ]),
//                                   ),
//                                   Container(
//                                     width: 120,
//                                     child: Column(children: <Widget>[
//                                       Text(user.hip,
//                                           style: TextStyle(
//                                               color: Colors.black,
//                                               fontSize: 24.0,
//                                               fontFamily: 'Comfortaa',
//                                               fontWeight: FontWeight.normal)),
//                                       SizedBox(
//                                         height: 5,
//                                       ),
//                                       Text('HIP',
//                                           style: TextStyle(
//                                               color: Colors.grey[400],
//                                               fontSize: 10.0,
//                                               fontFamily: 'Comfortaa',
//                                               fontWeight: FontWeight.normal)),
//                                     ]),
//                                   )
//                                 ],
//                               ),
//                             ),
//                           ),
//                         ],
//                       )),
//                   Container(
//                       margin: EdgeInsets.only(top: 7, bottom: 4),
//                       child: Text('Твоя власна картка FreeGen',
//                           style: TextStyle(
//                               color: Colors.white,
//                               fontSize: 14.0,
//                               fontFamily: 'Comfortaa',
//                               fontWeight: FontWeight.bold))),
//                   Container(
//                     alignment: Alignment.center,
//                     margin: EdgeInsets.only(left: 22, right: 22),
//                     width: double.infinity,
//                     height: 60,
//                     decoration: BoxDecoration(
//                         color: Colors.white,
//                         borderRadius: BorderRadius.only(
//                           topLeft: Radius.circular(10.0),
//                           topRight: Radius.circular(10.0),
//                           bottomLeft: Radius.circular(10.0),
//                           bottomRight: Radius.circular(10.0),
//                         )),
//                     child: Column(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: <Widget>[
//                         Image.asset(user.qr, width: 270, height: 30),
//                         SizedBox(
//                           height: 5,
//                         ),
//                         Text('ID:' + user.id,
//                             style: TextStyle(
//                                 color: Colors.black,
//                                 fontSize: 10.0,
//                                 fontFamily: 'Comfortaa',
//                                 fontWeight: FontWeight.normal,
//                                 letterSpacing: 1.5))
//                       ],
//                     ),
//                   ),
//                   Container(
//                       margin: EdgeInsets.only(top: 7, bottom: 4),
//                       child: Text('Твої винагороди',
//                           style: TextStyle(
//                               color: Colors.white,
//                               fontSize: 14.0,
//                               fontFamily: 'Comfortaa',
//                               fontWeight: FontWeight.bold))),
//                   Container(
//                     padding: EdgeInsets.all(6.0),
//                     child: GridView.builder(
//                         scrollDirection: Axis.vertical,
//                         gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                             crossAxisCount: 4),
//                         physics: BouncingScrollPhysics(),
//                         itemCount: userRewards.length,
//                         itemBuilder: (BuildContext context, int index) {
//                           return Container(
//                             width: 75,
//                             child: Column(
//                               mainAxisAlignment: MainAxisAlignment.center,
//                               crossAxisAlignment: CrossAxisAlignment.center,
//                               children: <Widget>[
//                                 Image.asset(
//                                   userRewards[index].imageUrl,
//                                   height: 60,
//                                   width: 60,
//                                 ),
//                                 Text(userRewards[index].description,
//                                     textAlign: TextAlign.center,
//                                     style: TextStyle(
//                                         color: Colors.grey[400],
//                                         fontSize: 7.0,
//                                         fontFamily: 'Comfortaa',
//                                         fontWeight: FontWeight.normal))
//                               ],
//                             ),
//                           );
//                         }),
//                     margin: EdgeInsets.only(left: 22, right: 22),
//                     width: double.infinity,
//                     height: 180,
//                     decoration: BoxDecoration(
//                         color: Colors.white,
//                         borderRadius: BorderRadius.only(
//                           topLeft: Radius.circular(10.0),
//                           topRight: Radius.circular(10.0),
//                           bottomLeft: Radius.circular(10.0),
//                           bottomRight: Radius.circular(10.0),
//                         )),
//                   ),
//                 ]),
//           )),
//       bottomNavigationBar: Container(
//         alignment: Alignment.topCenter,
//         height: 83,
//         decoration: BoxDecoration(color: Colors.white),
//         child: Container(
//           margin: const EdgeInsets.only(top: 10.0),
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceAround,
//             children: <Widget>[
//               SizedBox(
//                 width: 35,
//                 height: 35,
//                 child: IconButton(
//                   icon: Image.asset(
//                     'assets/bottom_pic_1.png',
//                     width: 16,
//                     height: 16,
//                   ),
//                   onPressed: () {
//                     Navigator.pushNamed(context, '/6_main_layout');
//                   },
//                 ),
//               ),
//               SizedBox(
//                 width: 35,
//                 height: 35,
//                 child: IconButton(
//                   icon: Image.asset(
//                     'assets/bottom_pic_2.png',
//                     width: 16,
//                     height: 16,
//                   ),
//                   onPressed: () {
//                     Navigator.pushNamed(context, '/7_search');
//                   },
//                 ),
//               ),
//               GestureDetector(
//                 child: Image.asset(
//                   'assets/bottom_pic_3.png',
//                   height: 42.0,
//                   width: 63.0,
//                 ),
//                 onTap: () {
//                   Navigator.of(context).pushNamed('/6_main_layout_camera');
//                 },
//               ),
//               SizedBox(
//                 width: 35,
//                 height: 35,
//                 child: IconButton(
//                   icon: Image.asset(
//                     'assets/bottom_pic_4.png',
//                     width: 16,
//                     height: 16,
//                   ),
//                   onPressed: () {
//                     Navigator.pushNamed(context, '../screens/home_screen.dart');
//                   },
//                 ),
//               ),
//               SizedBox(
//                 width: 35,
//                 height: 35,
//                 child: IconButton(
//                   icon: Image.asset(
//                     'assets/bottom_pic_5.png',
//                     width: 16,
//                     height: 16,
//                   ),
//                   onPressed: () {
//                     Navigator.pushNamed(context, '/9_profile');
//                   },
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   } 
// }

// --------------------------------------Loading data from firebase----------------------------------------

import 'package:flutter/material.dart';
import '9_profile_user_rewards.dart';
// import 'google_maps_routes.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MyApp10 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  
  @override
  _MyHomePageState createState() => _MyHomePageState();
  
}

class _MyHomePageState extends State<MyHomePage> {
  final id = int;
  // final bool marker = true;

  @override
  void initState() {    
    super.initState();  
  }

  @override
  Widget build(BuildContext context) {

    // addReward(){
    //   if(this.marker == true) {
    //     userRewards.add(reward4);
    //     setState(() {
          
    //     });
    //   }
    // }
    // addReward();

    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.orange[900],
        title: Container(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              width: 30,
              height: 30,
              child: IconButton(
                icon: Image.asset(
                  'assets/arrow2.png',
                  width: 11,
                  height: 11,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/6_main_layout');
                },
              ),
            ),
            // GestureDetector(
            //   child: SizedBox(
            //     width: 11,
            //     height: 11,
            //     child: Image.asset(
            //       'assets/arrow2.png',
            //       width: 11,
            //       height: 11,
            //     ),
            //   ),
            //   onTap: () {
            //     Navigator.pushNamed(context, '/6_main_layout');
            //   },
            // ),
            Image.asset('assets/freegen-logo-white.png',
                width: 39, height: 34, color: Colors.white),
            Text(
              'FreeGen Reality',
              style: TextStyle(
                  fontSize: 28,
                  color: Colors.white,
                  fontFamily: 'ArialRoundedMTBold'),
            ),
            SizedBox(
              width: 30,
              height: 30,
              child: IconButton(
                icon: Image.asset(
                  'assets/3dots.png',
                  width: 2.6,
                  height: 17,
                ),
                onPressed: () {},
              ),
            ),
            // GestureDetector(
            //   child: SizedBox(
            //       width: 11,
            //       height: 17,
            //       child: Image.asset(
            //         'assets/3dots.png',
            //         width: 2.6,
            //         height: 17,
            //       )),
            //   onTap: () {},
            // ),
          ],
        )),
      ),
      body: 
      StreamBuilder(
        stream: Firestore.instance.collection('users').snapshots(),
        builder: (context, snapshot) {
          if(!snapshot.hasData)
            return Text('Loading data... Please wait...');
            return Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.orange[900], Colors.white])),
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(top: 80.0, left: 22, right: 22),
                      width: double.infinity,
                      height: 250,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10.0),
                            topRight: Radius.circular(10.0),
                            bottomLeft: Radius.circular(10.0),
                            bottomRight: Radius.circular(10.0),
                          )),
                      child: Stack(
                        overflow: Overflow.visible,
                        alignment: Alignment.topCenter,
                        children: <Widget>[
                          Positioned(
                            top: -75,
                            child: Container(
                              height: 210,
                              child: Column(
                                children: <Widget>[
                                  Column(
                                    children: <Widget>[
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.pushNamed(
                                              context, '/10_profile_qr');
                                        },
                                        child: CircleAvatar(
                                          radius: 75,
                                          backgroundColor: Colors.white,
                                          child: CircleAvatar(
                                            radius: 70,
                                            backgroundImage:
                                                NetworkImage(snapshot.data.documents[0]['image']),
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 5.0),
                                      Text(
                                        snapshot.data.documents[0]['name'],
                                        style: TextStyle(
                                            fontSize: 24,
                                            color: Colors.black,
                                            fontFamily: 'Comfortaa',
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(height: 5.0),
                                      Text(
                                        snapshot.data.documents[0]['status'],
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.grey[400],
                                            fontFamily: 'Comfortaa',
                                            fontWeight: FontWeight.w400),
                                      ),
                                      SizedBox(height: 5.0),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Positioned(
                            bottom: 75,
                            child: Container(
                              // color: Colors.red,
                              width: 360,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Container(
                                    width: 120,
                                    child: Column(children: <Widget>[
                                      Text(snapshot.data.documents[0]['tus'].toString(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 24.0,
                                              fontFamily: 'Comfortaa',
                                              fontWeight: FontWeight.normal)),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text('TUS',
                                          style: TextStyle(
                                              color: Colors.grey[400],
                                              fontSize: 10.0,
                                              fontFamily: 'Comfortaa',
                                              fontWeight: FontWeight.normal)),
                                    ]),
                                  ),
                                  Container(
                                    width: 120,
                                    decoration: BoxDecoration(
                                      border: Border(
                                        left: BorderSide(
                                            width: 1.0,
                                            color: Colors.grey[400]),
                                        right: BorderSide(
                                            width: 1.0,
                                            color: Colors.grey[400]),
                                      ),
                                    ),
                                    child: Column(children: <Widget>[
                                      Text(snapshot.data.documents[0]['foints'].toString(),
                                          style: TextStyle(
                                              color: Colors.orange[900],
                                              fontSize: 24.0,
                                              fontFamily: 'Comfortaa',
                                              fontWeight: FontWeight.bold)),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text('FOINTS',
                                          style: TextStyle(
                                              color: Colors.orange[900],
                                              fontSize: 10.0,
                                              fontFamily: 'Comfortaa',
                                              fontWeight: FontWeight.normal)),
                                    ]),
                                  ),
                                  Container(
                                    width: 120,
                                    child: Column(children: <Widget>[
                                      Text(snapshot.data.documents[0]['skill'].toString(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 24.0,
                                              fontFamily: 'Comfortaa',
                                              fontWeight: FontWeight.normal)),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text('SKILL',
                                          style: TextStyle(
                                              color: Colors.grey[400],
                                              fontSize: 10.0,
                                              fontFamily: 'Comfortaa',
                                              fontWeight: FontWeight.normal)),
                                    ]),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Positioned(
                            bottom: 20,
                            child: Container(
                              // color: Colors.red,
                              width: 360,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Container(
                                    width: 120,
                                    child: Column(children: <Widget>[
                                      Text(snapshot.data.documents[0]['tiki'].toString(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 24.0,
                                              fontFamily: 'Comfortaa',
                                              fontWeight: FontWeight.normal)),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text('TIKI',
                                          style: TextStyle(
                                              color: Colors.grey[400],
                                              fontSize: 10.0,
                                              fontFamily: 'Comfortaa',
                                              fontWeight: FontWeight.normal)),
                                    ]),
                                  ),
                                  Container(
                                    width: 120,
                                    decoration: BoxDecoration(
                                      border: Border(
                                        left: BorderSide(
                                            width: 1.0,
                                            color: Colors.grey[400]),
                                        right: BorderSide(
                                            width: 1.0,
                                            color: Colors.grey[400]),
                                      ),
                                    ),
                                    child: Column(children: <Widget>[
                                      Text(snapshot.data.documents[0]['karma'].toString(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 24.0,
                                              fontFamily: 'Comfortaa',
                                              fontWeight: FontWeight.normal)),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text('KARMA',
                                          style: TextStyle(
                                              color: Colors.grey[400],
                                              fontSize: 10.0,
                                              fontFamily: 'Comfortaa',
                                              fontWeight: FontWeight.normal)),
                                    ]),
                                  ),
                                  Container(
                                    width: 120,
                                    child: Column(children: <Widget>[
                                      Text(snapshot.data.documents[0]['hip'].toString(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 24.0,
                                              fontFamily: 'Comfortaa',
                                              fontWeight: FontWeight.normal)),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text('HIP',
                                          style: TextStyle(
                                              color: Colors.grey[400],
                                              fontSize: 10.0,
                                              fontFamily: 'Comfortaa',
                                              fontWeight: FontWeight.normal)),
                                    ]),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      )),
                  Container(
                      margin: EdgeInsets.only(top: 7, bottom: 4),
                      child: Text('Твоя власна картка FreeGen',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14.0,
                              fontFamily: 'Comfortaa',
                              fontWeight: FontWeight.bold))),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(left: 22, right: 22),
                    width: double.infinity,
                    height: 60,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10.0),
                          topRight: Radius.circular(10.0),
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0),
                        )),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Image.network(snapshot.data.documents[0]['scancode'], width: 270, height: 30),
                        SizedBox(
                          height: 5,
                        ),
                        Text('ID:' + snapshot.data.documents[0]['id'].toString(),
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 10.0,
                                fontFamily: 'Comfortaa',
                                fontWeight: FontWeight.normal,
                                letterSpacing: 1.5))
                      ],
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 7, bottom: 4),
                      child: Text('Твої винагороди',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14.0,
                              fontFamily: 'Comfortaa',
                              fontWeight: FontWeight.bold))),
                  Container(
                    padding: EdgeInsets.all(6.0),
                    child: GridView.builder(
                        scrollDirection: Axis.vertical,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 4),
                        physics: BouncingScrollPhysics(),
                        itemCount: userRewards.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            width: 75,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  userRewards[index].imageUrl,
                                  height: 60,
                                  width: 60,
                                ),
                                Text(userRewards[index].description,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.grey[400],
                                        fontSize: 7.0,
                                        fontFamily: 'Comfortaa',
                                        fontWeight: FontWeight.normal))
                              ],
                            ),
                          );
                        }),
                    margin: EdgeInsets.only(left: 22, right: 22),
                    width: double.infinity,
                    height: 180,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10.0),
                          topRight: Radius.circular(10.0),
                          bottomLeft: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0),
                        )),
                  ),
                ]),
          )
          );
        }),
      
      
      bottomNavigationBar: Container(
        alignment: Alignment.topCenter,
        height: 83,
        decoration: BoxDecoration(color: Colors.white),
        child: Container(
          margin: const EdgeInsets.only(top: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_1.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/6_main_layout');
                  },
                ),
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_2.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/7_search');
                  },
                ),
              ),
              GestureDetector(
                child: Image.asset(
                  'assets/bottom_pic_3.png',
                  height: 42.0,
                  width: 63.0,
                ),
                onTap: () {
                  Navigator.of(context).pushNamed('/6_main_layout_camera');
                },
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_4.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '../screens/home_screen.dart');
                  },
                ),
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_5.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/9_profile');
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  } 
}
