class User {
  final String id;
  final String name;
  final String imageUrl;
  final String status;
  final String tus;
  final String foints;
  final String skill;
  final String tiki;
  final String karma;
  final String hip;
  final String qr;
  final String qrCode;


  User ({
    this.id,
    this.name,
    this.imageUrl, 
    this.status, 
    this.tus, 
    this.foints, 
    this.skill, 
    this.tiki, 
    this.karma, 
    this.hip,
    this.qr, 
    this.qrCode, 
  });
}

class Reward {
  final int id;
  final String name;
  final String imageUrl;
  final String description;

  Reward ({
    this.id,
    this.name,
    this.imageUrl,
    this.description,
  });
}