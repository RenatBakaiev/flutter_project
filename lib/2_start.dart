import 'package:flutter/material.dart';

class MyApp2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.orange[900],
          title: Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                width: 30,
                height: 30,
                child: IconButton(
                  icon: Image.asset(
                    'assets/arrow2.png',
                    width: 11,
                    height: 11,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/1_start');
                  },
                ),
              ),
              // GestureDetector(
              //   child: SizedBox(
              //       width: 11,
              //       height: 11,
              //       child: Image.asset(
              //         'assets/arrow2.png',
              //         width: 11,
              //         height: 11,
              //       )),
              //   onTap: () {
              //     Navigator.pushNamed(context, '/1_start');
              //   },
              // ),
              Image.asset('assets/freegen-logo-white.png',
                  width: 39, height: 34, color: Colors.white),
              Text(
                'FreeGen Reality',
                style: TextStyle(
                    fontSize: 28,
                    color: Colors.white,
                    fontFamily: 'ArialRoundedMTBold'),
              ),
              SizedBox(
                width: 30,
                height: 30,
                child: IconButton(
                  icon: Image.asset(
                    'assets/close.png',
                    width: 11,
                    height: 11,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/');
                  },
                ),
              ),
              // GestureDetector(
              //   child: SizedBox(
              //       width: 11,
              //       height: 11,
              //       child: Image.asset(
              //         'assets/close.png',
              //         width: 11,
              //         height: 11,
              //       )),
              //   onTap: () {
              //     Navigator.pushNamed(context, '/');
              //   },
              // ),
            ],
          )),
        ),
        body: Container(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/2.start_pic.jpg', width: 420),
                Container(
                  margin: const EdgeInsets.only(bottom: 10.0, top: 20),
                  child: Text(
                    'Усі пригоди у твоїй власній\nу кишені!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 22,
                        color: Colors.black,
                        fontFamily: 'Comfortaa',
                        fontWeight: FontWeight.w900),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 20.0, top: 10),
                  child: Text(
                    'Квести наповнені історіями і цікавими\nфактами від місцевих, аудіогідами,\nвікторинами та загадками з\nдоповненою реальністю.\nБуде яскраво!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        fontFamily: 'Comfortaa',
                        fontWeight: FontWeight.w500),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 35.0),
                  child: SizedBox(
                    width: 237,
                    height: 50,
                    child: RaisedButton(
                      color: Colors.orange[900],
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(25.0)),
                      onPressed: () {
                        Navigator.pushNamed(context, '/3_start');
                      },
                      child: Text(
                        'ДАЛI',
                        style: TextStyle(
                            fontSize: 13,
                            color: Colors.white,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w900),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.orange[900], Colors.white])),
        ),
      ),
    );
  }
}
