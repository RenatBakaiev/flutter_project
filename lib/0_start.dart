import 'package:flutter/material.dart';

class MyApp0 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange[900],
      body: MyStartPage(),
    );
  }
}

class MyStartPage extends StatefulWidget {
  @override
  _MyStartPage createState() => _MyStartPage();
}

class _MyStartPage extends State<MyStartPage> {
  @override
  void initState() {
    Future.delayed(const Duration(seconds: 5), () {
      Navigator.pushNamed(context, '/1_start');
      print('go to second page');
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        resizeToAvoidBottomPadding: false,
        body: GestureDetector(
          onTap: () {
            Navigator.pushNamed(context, '/1_start');
          },
          child: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Image.asset(
                      'assets/login_start.png',
                      height: 235,
                      width: 205,
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 65.0),
                    child: Text(
                      'FreeGen \n Reality',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 48,
                          color: Colors.white,
                          fontFamily: 'ArialRoundedMTBold'),
                    ),
                  )
                ],
              ),
            ),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colors.orange[900], Colors.white])),
          ),
        ),
      ),
    );
  }
}
