import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class MyAppLocation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  StreamSubscription _locationSubscription;
  Location _locationTracker = Location();
  Marker marker;
  Circle circle;
  Map<MarkerId, Marker> markers = {};
  Map<PolylineId, Polyline> polylines = {};
  GoogleMapController mapController;
  // double _originLatitude = 50.450148,
  //       _originLongitude = 30.524024;

  double _originLatitude = 50.450148, _originLongitude = 30.524024;

  double _destLatitude = 50.445309, _destLongitude = 30.517827;
  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();
  String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

  @override
  void initState() {
    super.initState();

    _addMarker(
        LatLng(_originLatitude, _originLongitude),
        "origin",
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
        InfoWindow(title: 'Start'));
    _addMarker(
        LatLng(_destLatitude, _destLongitude),
        "destination",
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
        InfoWindow(title: 'Point2'));

    //_getPolyline();
  }

  static final CameraPosition initialLocation = CameraPosition(
    target: LatLng(50.450154, 30.523185),
    // target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  Future<Uint8List> getMarker() async {
    ByteData byteData =
        await DefaultAssetBundle.of(context).load("assets/map_icon.png");
    return byteData.buffer.asUint8List();
  }

  void updateMarkerAndCircle(LocationData newLocalData, Uint8List imageData) {
    LatLng latlng = LatLng(newLocalData.latitude, newLocalData.longitude);
    this.setState(() {
      marker = Marker(
          markerId: MarkerId("home"),
          position: latlng,
          rotation: newLocalData.heading,
          draggable: false,
          zIndex: 2,
          flat: true,
          anchor: Offset(0.5, 0.5),
          icon: BitmapDescriptor.fromBytes(imageData));
      circle = Circle(
          circleId: CircleId("car"),
          radius: newLocalData.accuracy,
          zIndex: 1,
          strokeColor: Colors.orange[900],
          center: latlng,
          fillColor: Colors.orange[900].withAlpha(70));
    });
  }

  void getCurrentLocation() async {
    try {
      Uint8List imageData = await getMarker();
      var location = await _locationTracker.getLocation();

      updateMarkerAndCircle(location, imageData);

      if (_locationSubscription != null) {
        _locationSubscription.cancel();
      }

      _locationSubscription =
          _locationTracker.onLocationChanged.listen((newLocalData) {
        if (mapController != null) {
          mapController.animateCamera(CameraUpdate.newCameraPosition(
              new CameraPosition(
                  bearing: 0,
                  target: LatLng(newLocalData.latitude, newLocalData.longitude),
                  tilt: 0,
                  zoom: 18.00)));
          updateMarkerAndCircle(newLocalData, imageData);
          _getPolyline(newLocalData.latitude, newLocalData.longitude);
        }

// _addPolyLine() {
//     PolylineId id = PolylineId("poly");
//     Polyline polyline = Polyline(
//       polylineId: id,
//       visible: true,
//       color: Colors.green,
//       points: polylineCoordinates,
//       width: 4,
//       startCap: Cap.roundCap,
//       endCap: Cap.buttCap,
//     );
//     polylines[id] = polyline;
//     setState(() {});
//   }

//   _getPolyline() async {
//     PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
//         "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//         PointLatLng(newLocalData.latitude, newLocalData.longitude),
//         PointLatLng(_destLatitude, _destLongitude),
//         travelMode: TravelMode.walking,
//         wayPoints: [PolylineWayPoint(location: "Kiev")]);
//     if (result.points.isNotEmpty) {
//       result.points.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });
//     }

//     _addPolyLine();
//   }

        //_getPolyline();
      });
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        debugPrint("Permission Denied");
      }
    }
  }

  @override
  void dispose() {
    if (_locationSubscription != null) {
      _locationSubscription.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.orange[900],
        title: Container(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              width: 30,
              height: 30,
              child: IconButton(
                icon: Image.asset(
                  'assets/arrow2.png',
                  width: 11,
                  height: 11,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/6_main_layout');
                },
              ),
            ),
            // GestureDetector(
            //   child: SizedBox(
            //        height: 11,
            //       width: 11,
            //     child: Image.asset('assets/arrow2.png', width: 11, height: 11)),
            //   onTap: () {
            //     Navigator.pushNamed(context, '/1_start');
            //   },
            // ),
            Image.asset('assets/freegen-logo-white.png',
                width: 39, height: 34, color: Colors.white),
            Text(
              'FreeGen Reality',
              style: TextStyle(
                  fontSize: 28,
                  color: Colors.white,
                  fontFamily: 'ArialRoundedMTBold'),
            ),
            SizedBox(
              width: 30,
              height: 30,
            )
          ],
        )),
      ),
      body: Stack(
        children: <Widget>[
          GoogleMap(
            mapType: MapType.normal,
            polylines: Set<Polyline>.of(polylines.values),
            initialCameraPosition: initialLocation,
            myLocationEnabled: true,

            // onTap: _handleTap, // TAP TO ADD MARKER
            // markers: Set.from(myMarker), // TAP TO ADD MARKER
            markers: Set.of((marker != null) ? [marker] : []),
            circles: Set.of((circle != null) ? [circle] : []),
            onMapCreated: (GoogleMapController controller) {
              mapController = controller;
            },
          ),
          Positioned(
            bottom: 120,
            right: 12,
            child: Container(
              height: 40.0,
              width: 40.0,
              child: FloatingActionButton(
                  backgroundColor: Colors.orange[900],
                  child: Icon(Icons.location_searching),
                  onPressed: () {
                    getCurrentLocation();
                  }),
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        alignment: Alignment.topCenter,
        height: 83,
        decoration: BoxDecoration(color: Colors.white),
        child: Container(
          margin: const EdgeInsets.only(top: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_1.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/6_main_layout');
                  },
                ),
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_2.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/7_search');
                  },
                ),
              ),
              GestureDetector(
                child: Image.asset(
                  'assets/bottom_pic_3.png',
                  height: 42.0,
                  width: 63.0,
                ),
                onTap: () {
                  Navigator.of(context).pushNamed('/6_main_layout_camera');
                },
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_4.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '../screens/home_screen.dart');
                  },
                ),
              ),
              SizedBox(
                width: 35,
                height: 35,
                child: IconButton(
                  icon: Image.asset(
                    'assets/bottom_pic_5.png',
                    width: 16,
                    height: 16,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/9_profile');
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  // _handleTap(LatLng tappedPoint) { // TAP TO ADD MARKER
  //   print(tappedPoint);
  //   setState(() {
  //     myMarker = [];
  //     myMarker.add(Marker(
  //       markerId: MarkerId(tappedPoint.toString()),
  //       position: tappedPoint,
  //       draggable: true,
  //       icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
  //       onDragEnd: (dragEndPosition) {
  //         print(dragEndPosition);
  //       }
  //     ));
  //   });
  // }

// void _onMapCreated(GoogleMapController controller) async {
//     mapController = controller;
//   }

  _addMarker(LatLng position, String id, BitmapDescriptor descriptor,
      InfoWindow infowindow) {
    MarkerId markerId = MarkerId(id);
    Marker marker = Marker(
        markerId: markerId,
        icon: descriptor,
        position: position,
        infoWindow: infowindow);
    markers[markerId] = marker;
  }

  _addPolyLine() {
    PolylineId id = PolylineId("poly");
    Polyline polyline = Polyline(
      polylineId: id,
      visible: true,
      color: Colors.green,
      points: polylineCoordinates,
      width: 4,
      startCap: Cap.roundCap,
      endCap: Cap.buttCap,
    );
    polylines[id] = polyline;
    setState(() {});
  }

  _getPolyline(a, b) async {
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
        "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
        PointLatLng(a, b),
        PointLatLng(_destLatitude, _destLongitude),
        travelMode: TravelMode.walking,
        wayPoints: [PolylineWayPoint(location: "Kiev")]);
    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }
    _addPolyLine();
  }
}
