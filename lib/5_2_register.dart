import 'package:flutter/material.dart';
import 'package:flutterfreegenapp/services/auth.dart';
import 'package:flutterfreegenapp/user.dart';
import 'package:fluttertoast/fluttertoast.dart';

class MyForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyFormState();
}

class MyFormState extends State {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  String _email;
  String _password;
  bool showLogin = true;

  FlutterToast flutterToast;

  @override
  void initState() {
    super.initState();
    flutterToast = FlutterToast(context);
  }

  _showToast() {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Colors.greenAccent,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.check),
          SizedBox(
            width: 12.0,
          ),
          Text("Error!"),
        ],
      ),
    );

    flutterToast.showToast(
      child: toast,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 2),
    );
  }

  void _registerButtonAction() async {
    _email = _emailController.text;
    _password = _passwordController.text;

    // if (_email.isEmpty || _password.isEmpty) return;

    User user = await _authService.registerWithEmailAndPassword(
        _email.trim(), _password.trim());
    if (user == null) {
      _showToast();
    } else {
      _emailController.clear();
      _passwordController.clear();
    }
  }

  AuthService _authService = AuthService();

  @override
  Widget build(BuildContext context) {
    return Container(
        child: new Form(
      key: _formKey,
      // child: SingleChildScrollView(
      child: new Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 18.0, bottom: 18.0),
            alignment: Alignment.center,
            child: TextFormField(
              style: TextStyle(
                  fontSize: 15, color: Colors.black, fontFamily: 'Roboto'),
              textAlign: TextAlign.left,
              textAlignVertical: TextAlignVertical.center,
              decoration: new InputDecoration(
                hintText: 'Введіть E-mail',
                contentPadding:
                    new EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                fillColor: Colors.white,
                filled: true,
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(25.0),
                ),
              ),
              validator: (String value) {
                if (value.isEmpty) return 'Будь-ласка введіть E-mail!';
                String p =
                    "[a-zA-Z0-9+.\_\%-+]{1,256}@[a-zA-Z0-9][a-zA-Z0-9-]{0,64}(.[a-zA-Z0-9][a-zA-Z0-9-]{0,25})+";
                RegExp regExp = new RegExp(p);
                if (regExp.hasMatch(value)) return null;
                return 'Будь-ласка введіть коректний E-mail!';
              },
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 18.0),
            alignment: Alignment.center,
            child: TextFormField(
              style: TextStyle(
                  fontSize: 15, color: Colors.black, fontFamily: 'Roboto'),
              obscureText: true,
              textAlign: TextAlign.left,
              textAlignVertical: TextAlignVertical.center,
              decoration: new InputDecoration(
                hintText: 'Введіть пароль',
                contentPadding:
                    new EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                fillColor: Colors.white,
                filled: true,
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(25.0),
                ),
              ),
              // ignore: missing_return
              validator: (String value) {
                if (value.isEmpty) return 'Будь-ласка введіть пароль!';
              },
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 18.0),
            alignment: Alignment.center,
            child: TextFormField(
              style: TextStyle(
                  fontSize: 15, color: Colors.black, fontFamily: 'Roboto'),
              obscureText: true,
              textAlign: TextAlign.left,
              textAlignVertical: TextAlignVertical.center,
              decoration: new InputDecoration(
                hintText: 'Повторіть пароль',
                contentPadding:
                    new EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                fillColor: Colors.white,
                filled: true,
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(25.0),
                ),
              ),
              // ignore: missing_return
              validator: (String value) {
                if (value.isEmpty) return 'Будь-ласка введіть пароль!';
              },
            ),
          ),
          SizedBox(
            width: 343,
            height: 50,
            child: RaisedButton(
              color: Colors.orange[900],
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(25.0)),
              onPressed: () {
                if (_formKey.currentState.validate())
                  // Scaffold.of(context).showSnackBar(new SnackBar(
                  //   content: Text('Форма успішно заповнена'),
                  //   backgroundColor: Colors.green,

                  // ));
                  // Navigator.pushNamed(context, '/6_main_layout');
                  _registerButtonAction();
              },
              child: Text(
                'ДАЛІ',
                style: TextStyle(
                    fontSize: 13,
                    color: Colors.white,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w900),
              ),
            ),
          ),
        ],
      ),
      // ),
    ));
  }
}

class MyApp52 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var _blankFocusNode = new FocusNode();

    return MaterialApp(
      home: Scaffold(
        // resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.orange[900],
          title: Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                width: 30,
                height: 30,
                child: IconButton(
                  icon: Image.asset(
                    'assets/arrow2.png',
                    width: 11,
                    height: 11,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/5_login');
                  },
                ),
              ),
              // GestureDetector(
              //   child: Image.asset(
              //     'assets/arrow2.png',
              //     width: 11,
              //     height: 11,
              //   ),
              //   onTap: () {
              //     Navigator.pushNamed(context, '/5_login');
              //   },
              // ),
              Image.asset('assets/freegen-logo-white.png',
                  width: 39, height: 34, color: Colors.white),
              Text(
                'FreeGen Reality',
                style: TextStyle(
                    fontSize: 28,
                    color: Colors.white,
                    fontFamily: 'ArialRoundedMTBold'),
              ),
              SizedBox(
                width: 30,
              )
            ],
          )),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).requestFocus(_blankFocusNode);
          },
          child: Container(
            // child: Center(
            child: SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.only(top: 50.0),
                width: 343,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Зареєструватись',
                              style: TextStyle(
                                  fontSize: 24,
                                  color: Colors.white,
                                  fontFamily: 'Comfortaa',
                                  fontWeight: FontWeight.w700),
                            )
                          ],
                        ),
                        MyForm(),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            // ),
            // alignment: Alignment.center,
            width: double.infinity,
            height: double.infinity,
            padding: EdgeInsets.only(left: 10, right: 10),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colors.orange[900], Colors.white])),
          ),
        ),
      ),
    );
  }
}
