import 'package:flutter/material.dart';

class MyApp4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange[900],
      body: MyChosePage(),
    );
  }
}

class MyChosePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyChosePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.orange[900],
          title: Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                width: 30,
                height: 30,
                child: IconButton(
                  icon: Image.asset(
                    'assets/arrow2.png',
                    width: 11,
                    height: 11,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/3_start');
                  },
                ),
              ),
              // GestureDetector(
              //   child: SizedBox(
              //     width: 11,
              //     height: 11,
              //     child: Image.asset('assets/arrow2.png', width: 11, height: 11)),
              //   onTap: () {
              //     Navigator.pushNamed(context, '/3_start');
              //   },
              // ),
            ],
          )),
        ),
        body: Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.only(top: 10),
                      child: Image.asset(
                        'assets/login_start.png',
                        width: 235,
                        height: 205,
                      ),
                    ),
                    Container(
                      // margin: const EdgeInsets.only(top: 50.0),
                      child: Text(
                        'FreeGen \n Reality',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 48,
                            color: Colors.white,
                            fontFamily: 'ArialRoundedMTBold'),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.only(bottom: 21.0),
                        child: SizedBox(
                          // width: 343,
                          height: 50,
                          child: RaisedButton(
                              color: Colors.orange[900],
                              shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(25.0)),
                              onPressed: () {},
                              child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    Image.asset(
                                      'assets/login_button_apple.png',
                                      width: 22,
                                      height: 19,
                                    ),
                                    Text(
                                      'ВХІД ЧЕРЕЗ APPLE',
                                      style: TextStyle(
                                          fontSize: 13,
                                          color: Colors.white,
                                          fontFamily: 'Roboto',
                                          fontWeight: FontWeight.w900),
                                    ),
                                    SizedBox(
                                      width: 22,
                                    )
                                  ])),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(bottom: 21.0),
                        child: SizedBox(
                          // width: 343,
                          height: 50,
                          child: RaisedButton(
                              color: Colors.orange[900],
                              shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(25.0)),
                              onPressed: () {},
                              child: Container(
                                margin: const EdgeInsets.only(
                                    left: 8.0, right: 8.0),
                                child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: <Widget>[
                                      Image.asset(
                                        'assets/login_button_facebook.png',
                                        width: 15,
                                        height: 22,
                                      ),
                                      Text(
                                        'ВХІД ЧЕРЕЗ FACEBOOK',
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: Colors.white,
                                            fontFamily: 'Roboto',
                                            fontWeight: FontWeight.w900),
                                      ),
                                      SizedBox(
                                        width: 15,
                                      )
                                    ]),
                              )),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(bottom: 35.0),
                        child: SizedBox(
                          // width: 343,
                          height: 50,
                          child: RaisedButton(
                              color: Colors.orange[900],
                              shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(25.0)),
                              onPressed: () {
                                Navigator.pushNamed(context, '/4_landing');
                              },
                              child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    Image.asset(
                                      'assets/login_button_mail.png',
                                      width: 22,
                                      height: 15,
                                    ),
                                    Text(
                                      'ВХІД ЧЕРЕЗ EMAIL',
                                      style: TextStyle(
                                          fontSize: 13,
                                          color: Colors.white,
                                          fontFamily: 'Roboto',
                                          fontWeight: FontWeight.w900),
                                    ),
                                    SizedBox(
                                      width: 22,
                                    )
                                  ])),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.orange[900], Colors.white])),
        ),
      ),
    );
  }
}
