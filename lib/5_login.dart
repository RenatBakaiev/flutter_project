// import 'package:flutter/material.dart';
// import 'package:flutterfreegenapp/services/auth.dart';
// import 'package:flutterfreegenapp/user.dart';
// import 'package:fluttertoast/fluttertoast.dart';

// class MyForm extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() => MyFormState();
// }

// class MyFormState extends State {
//   final _formKey = GlobalKey<FormState>();

//   TextEditingController _emailController = TextEditingController();
//   TextEditingController _passwordController = TextEditingController();

//   String _email;
//   String _password;
//   bool showLogin = true;

//         FlutterToast flutterToast;

// @override
// void initState() {
//     super.initState();
//     flutterToast = FlutterToast(context);
// }

// _showToast() {
//     Widget toast = Container(
//         padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
//         decoration: BoxDecoration(
//         borderRadius: BorderRadius.circular(25.0),
//         color: Colors.greenAccent,
//         ),
//         child: Row(
//         mainAxisSize: MainAxisSize.min,
//         children: [
//             Icon(Icons.check),
//             SizedBox(
//             width: 12.0,
//             ),
//             Text("Error!"),
//         ],
//         ),
//     );

//     flutterToast.showToast(
//         child: toast,
//         gravity: ToastGravity.BOTTOM,
//         toastDuration: Duration(seconds: 2),
//     );
// }

//   void _loginButtonAction() async {
//     _email = _emailController.text;
//     _password = _passwordController.text;

//     // if(_email.isEmpty || _password.isEmpty) return;

//     User user = await _authService.signInWithEmailAndPassword(_email.trim(), _password.trim());
//     if(user == null)
//     {

// _showToast();

//     }else{
//       _emailController.clear();
//       _passwordController.clear();
//     }
//   }

//   AuthService _authService = AuthService();

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//         child: new Form(
//       key: _formKey,
//       child: new Column(
//         children: <Widget>[
// Container(
//   margin: const EdgeInsets.only(top: 18.0, bottom: 18.0),
//   alignment: Alignment.center,
//   child: TextFormField(
//     style: TextStyle(
//         fontSize: 15, color: Colors.black, fontFamily: 'Roboto'),
//     textAlign: TextAlign.left,
//     textAlignVertical: TextAlignVertical.center,
//     decoration: new InputDecoration(
//       hintText: 'Введіть E-mail',
//       contentPadding:
//           new EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
//       fillColor: Colors.white,
//       filled: true,
//       border: new OutlineInputBorder(
//         borderRadius: new BorderRadius.circular(25.0),
//       ),
//     ),
//     validator: (String value) {
//       if (value.isEmpty) return 'Будь-ласка введіть E-mail!';
//       String p =
//           "[a-zA-Z0-9+.\_\%-+]{1,256}@[a-zA-Z0-9][a-zA-Z0-9-]{0,64}(.[a-zA-Z0-9][a-zA-Z0-9-]{0,25})+";
//       RegExp regExp = new RegExp(p);
//       if (regExp.hasMatch(value)) return null;
//       return 'Будь-ласка введіть коректний E-mail!';
//     },
//   ),
// ),
//           Container(
//             margin: const EdgeInsets.only(bottom: 18.0),
//             alignment: Alignment.center,
//             child: TextFormField(
//               style: TextStyle(
//                   fontSize: 15, color: Colors.black, fontFamily: 'Roboto'),
//               obscureText: true,
//               textAlign: TextAlign.left,
//               textAlignVertical: TextAlignVertical.center,
//               decoration: new InputDecoration(
//                 hintText: 'Введіть пароль',
//                 contentPadding:
//                     new EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
//                 fillColor: Colors.white,
//                 filled: true,
//                 border: new OutlineInputBorder(
//                   borderRadius: new BorderRadius.circular(25.0),
//                 ),
//               ),
//               // ignore: missing_return
//               validator: (String value) {
//                 if (value.isEmpty) return 'Будь-ласка введіть пароль!';
//               },
//             ),
//           ),
//           SizedBox(
//             width: 343,
//             height: 50,
//             child: RaisedButton(
//               color: Colors.orange[900],
//               shape: new RoundedRectangleBorder(
//                   borderRadius: new BorderRadius.circular(25.0)),
//               onPressed: () {
//                 // _loginButtonAction();
//                 if (_formKey.currentState.validate())
//                                 _loginButtonAction();

//                 // Navigator.pushNamed(context, '/6_main_layout');
//               },
//               child: Text(
//                 'ДАЛІ',
//                 style: TextStyle(
//                     fontSize: 13,
//                     color: Colors.white,
//                     fontFamily: 'Roboto',
//                     fontWeight: FontWeight.w900),
//               ),
//             ),
//           ),
//         ],
//       ),
//     ));
//   }
// }

// class MyLoginPage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     var _blankFocusNode = new FocusNode();
//       return Scaffold(
//         resizeToAvoidBottomPadding: false,
//         appBar: AppBar(
//           elevation: 0.0,
//           automaticallyImplyLeading: false,
//           backgroundColor: Colors.orange[900],
//           title: Container(
//               child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: <Widget>[
//               GestureDetector(
//                 child: Image.asset('assets/arrow2.png',width: 11, height: 11,),
//                 onTap: () {
//                   Navigator.pushNamed(context, '/4_login');
//                 },
//               ),
//               Image.asset('assets/freegen-logo-white.png',width: 39, height: 34, color: Colors.white),
//               Text(
//                 'FreeGen Reality',
//                 style: TextStyle(
//                     fontSize: 28,
//                     color: Colors.white,
//                     fontFamily: 'ArialRoundedMTBold'),
//               ),
//               SizedBox(
//                 width: 1,
//               )
//             ],
//           )),
//         ),
//         body: GestureDetector(
//           behavior: HitTestBehavior.opaque,
//           onTap: () {
//             FocusScope.of(context).requestFocus(_blankFocusNode);
//           },
//                   child: Container(
//               child: SingleChildScrollView(
//                             child: Container(
//                   margin: const EdgeInsets.only(top: 52.0),
//                   width: 343,
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.start,
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     children: <Widget>[
//                       Column(
//                         children: <Widget>[
//                           Row(
//                             mainAxisAlignment: MainAxisAlignment.start,
//                             children: <Widget>[
//                               Text(
//                                 'Увійти',
//                                 style: TextStyle(
//                                     fontSize: 24,
//                                     color: Colors.white,
//                                     fontFamily: 'Comfortaa',
//                                     fontWeight: FontWeight.w700),
//                               )
//                             ],
//                           ),
//                           MyForm(),
//                           Container(
//                             margin: const EdgeInsets.only(top: 10.0),
//                             child: GestureDetector(
//                                 child: Text(
//                                   'Не зареєстровані? Зареєструватись!',
//                                   style: TextStyle(
//                                       fontSize: 13,
//                                       color: Colors.black,
//                                       fontFamily: 'Roboto',
//                                       fontWeight: FontWeight.w500),
//                                 ),
//                                 onTap: () {
//                                   Navigator.pushNamed(context, '/5_2_register');
//                                 }),
//                           )
//                         ],
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             width: double.infinity,
//             height: double.infinity,
//             padding: EdgeInsets.only(left: 10, right: 10),
//             decoration: BoxDecoration(
//                 gradient: LinearGradient(
//                     begin: Alignment.topCenter,
//                     end: Alignment.bottomCenter,
//                     colors: [Colors.orange[900], Colors.white])),
//           ),
//         ),
//       );
//     // );
//   }
// }

// -------------------------------------------------------------LOGIN-VERSION-2-----------------------------------------------------------------

import 'package:flutter/material.dart';
import 'package:flutterfreegenapp/services/auth.dart';
import 'package:flutterfreegenapp/user.dart';
import 'package:fluttertoast/fluttertoast.dart';

class MyLoginPage extends StatefulWidget {
  @override
  _MyLoginPageState createState() => _MyLoginPageState();
}

class _MyLoginPageState extends State<MyLoginPage> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  String _email;
  String _password;
  bool showLogin = true;

  AuthService _authService = AuthService();

  FlutterToast flutterToast;

  @override
  void initState() {
    super.initState();
    flutterToast = FlutterToast(context);
  }

  _showToast() {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        color: Colors.red,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            Icons.check,
            color: Colors.white,
          ),
          SizedBox(
            width: 5.0,
          ),
          (showLogin
              ? Text(
                  "Користувач не знайдений!\nЗареєструйтесь!",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 15,
                      color: Colors.white,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w500),
                )
              : Text(
                  "Невірний формат email!",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 15,
                      color: Colors.white,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w500),
                ))
        ],
      ),
    );

    flutterToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  @override
  Widget build(BuildContext context) {
    var _blankFocusNode = new FocusNode();

    Widget _input(String hint, TextEditingController controller, bool obscure) {
      return Container(
        height: 50,
        padding: EdgeInsets.only(left: 16, right: 16),
        child: TextField(
          controller: controller,
          obscureText: obscure,
          textAlign: TextAlign.left,
          style: TextStyle(
              fontWeight: FontWeight.normal,
              fontSize: 16,
              color: Colors.black,
              fontFamily: 'Roboto'),
          decoration: InputDecoration(
            contentPadding:
                new EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
            fillColor: Colors.white,
            filled: true,
            hintStyle: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 15,
                color: Colors.grey,
                fontFamily: 'Roboto'),
            hintText: hint,
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(color: Colors.orange[900], width: 3)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(color: Colors.white54, width: 1)),
            // prefixIcon: Padding(
            //   padding: EdgeInsets.only(left: 10, right: 10),
            //   child: IconTheme(
            //     data: IconThemeData(color: Colors.white),
            //     child: icon,
            //   ),
            // )
          ),
        ),
      );
    }

    Widget _button(String text, void func()) {
      return RaisedButton(
        // splashColor: Theme.of(context).primaryColor,
        // highlightColor: Theme.of(context).primaryColor,
        color: Colors.orange[900],
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(25.0)),
        child: Text(text,
            style: TextStyle(
                fontWeight: FontWeight.w900,
                fontFamily: 'Roboto',
                color: Colors.white,
                letterSpacing: 1.05,
                fontSize: 13)),
        onPressed: () {
          func();
        },
      );
    }

    Widget _form(String label, void func()) {
      return Container(
        child: Column(children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: 18, top: 18),
            child: _input("Введіть E-mail", _emailController, false),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 18),
            child: _input("Введіть пароль (не менше 6 символів)",
                _passwordController, true),
          ),
          Padding(
            padding: EdgeInsets.only(
              left: 16,
              right: 16,
            ),
            child: Container(
              height: 50,
              width: MediaQuery.of(context).size.width,
              child: _button(label, func),
            ),
          ),
        ]),
      );
    }

    void _loginButtonAction() async {
      _email = _emailController.text;
      _password = _passwordController.text;

      if (_email.isEmpty || _password.isEmpty) return;

      User user = await _authService.signInWithEmailAndPassword(
          _email.trim(), _password.trim());
      if (user == null) {
        _showToast();
      } else {
        _emailController.clear();
        _passwordController.clear();
      }
    }

    void _registerButtonAction() async {
      _email = _emailController.text;
      _password = _passwordController.text;

      if (_email.isEmpty || _password.isEmpty) return;

      User user = await _authService.registerWithEmailAndPassword(
          _email.trim(), _password.trim());
      if (user == null) {
        _showToast();
      } else {
        _emailController.clear();
        _passwordController.clear();
      }
    }

    return Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          automaticallyImplyLeading: false,
          backgroundColor: Colors.orange[900],
          title: Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                width: 30,
                height: 30,
                child: IconButton(
                  icon: Image.asset(
                    'assets/arrow2.png',
                    width: 11,
                    height: 11,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/4_login');
                  },
                ),
              ),
              // GestureDetector(
              //   child: SizedBox(
              //     width: 11,
              //     height: 11,
              //                     child: Image.asset(
              //       'assets/arrow2.png',
              //       width: 11,
              //       height: 11,
              //     ),
              //   ),
              //   onTap: () {
              //     Navigator.pushNamed(context, '/4_login');
              //   },
              // ),
              Image.asset('assets/freegen-logo-white.png',
                  width: 39, height: 34, color: Colors.white),
              Text(
                'FreeGen Reality',
                style: TextStyle(
                    fontSize: 28,
                    color: Colors.white,
                    fontFamily: 'ArialRoundedMTBold'),
              ),
              SizedBox(
                width: 30,
                height: 30,
              )
            ],
          )),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            FocusScope.of(context).requestFocus(_blankFocusNode);
          },
          child: Container(
            height: double.infinity,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colors.orange[900], Colors.white])),
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.only(top: 50, left: 16.0),
                        child: (showLogin
                            ? Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Увійти',
                                    style: TextStyle(
                                        fontSize: 24,
                                        color: Colors.white,
                                        fontFamily: 'Comfortaa',
                                        fontWeight: FontWeight.w700),
                                  )
                                ],
                              )
                            : Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Зареєструватись',
                                    style: TextStyle(
                                        fontSize: 24,
                                        color: Colors.white,
                                        fontFamily: 'Comfortaa',
                                        fontWeight: FontWeight.w700),
                                  )
                                ],
                              ))),
                    (showLogin
                        ? Column(
                            children: <Widget>[
                              _form('ДАЛІ', _loginButtonAction),
                              Padding(
                                padding: EdgeInsets.all(10),
                                child: GestureDetector(
                                  child: Text(
                                    'Не зареєстровані? Зареєструватись!',
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.black,
                                        fontFamily: 'Roboto',
                                        fontWeight: FontWeight.w500),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      showLogin = false;
                                    });
                                  },
                                ),
                              )
                            ],
                          )
                        : Column(
                            children: <Widget>[
                              _form('ЗАРЕЄСТРУВАТИСЬ', _registerButtonAction),
                              Padding(
                                padding: EdgeInsets.all(10),
                                child: GestureDetector(
                                  child: Text(
                                    'Вже зареєстровані? Увійти!',
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.black,
                                        fontFamily: 'Roboto',
                                        fontWeight: FontWeight.w500),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      showLogin = true;
                                    });
                                  },
                                ),
                              )
                            ],
                          ))
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
