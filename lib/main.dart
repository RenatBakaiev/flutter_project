import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:flutterfreegenapp/services/auth.dart';
import 'package:flutterfreegenapp/user.dart';
import 'package:provider/provider.dart';
import 'dart:async';

import '0_start.dart';
import '1_start.dart';
import '2_start.dart';
import '3_start.dart';
import '4_login.dart';
import '4_landing.dart';
import '5_login.dart';
// import '5_2_register.dart';
import '6_main_layout.dart';
import '6_2_main_layout_camera.dart';
import '8_ar.dart';
import '7_search.dart';
import '9_profile.dart';
import '10_profile_qr.dart';
import 'location.dart';
import 'google_maps_routes.dart';
import './chat/screens/home_screen.dart';

List<CameraDescription> cameras;

Future<Null> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final cameras = await availableCameras();
  final firstCamera = cameras.first;

  runApp(StreamProvider<User>.value(
    value: AuthService().currentUser,
    child: MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/', routes: {
      '/': (BuildContext context) => MyApp0(),
      '/1_start': (BuildContext context) => MyApp(),
      '/2_start': (BuildContext context) => MyApp2(),
      '/3_start': (BuildContext context) => MyApp3(),
      '/4_login': (BuildContext context) => MyApp4(),
      '/4_landing': (BuildContext context) => LandingPage(),
      '/5_login': (BuildContext context) => MyLoginPage(),
      // '/5_2_register': (BuildContext context) => MyApp52(),
      '/6_main_layout': (BuildContext context) => MyApp6(),
      '/6_main_layout_camera': (BuildContext context) => MyApp62(
            camera: firstCamera,
          ),
      '/8_ar': (BuildContext context) => MyApp7(),
      '/7_search': (BuildContext context) => MyApp8(),
      '/location': (BuildContext context) => MyAppLocation(),
      '/google_maps_routes': (BuildContext context) => MyAppRoutes(),
      '../screens/home_screen.dart': (BuildContext context) => MyApp9(),
      '/9_profile': (BuildContext context) => MyApp10(),
      '/10_profile_qr': (BuildContext context) => MyApp11(),
    }),
  ));
}

//-------------------------------------ANDROID------------------------------------------

// import 'package:flutter/material.dart';
// import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
// import 'package:vector_math/vector_math_64.dart' as vector;

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter AR',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: MyHomePage(title: 'Flutter AR'),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   MyHomePage({Key key, this.title}) : super(key: key);
//   final String title;

//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   ArCoreController arCoreController;

//   _onArCoreViewCreated(ArCoreController _arcoreController) {
//     arCoreController = _arcoreController;
//     _addSphere(arCoreController);
//     _addCube(arCoreController);
//     _addCyclinder(arCoreController);
//   }

//   _addSphere(ArCoreController _arcoreController) {
//     final material = ArCoreMaterial(color: Colors.orange);
//     final sphere = ArCoreSphere(materials: [material], radius: 0.2);
//     final node = ArCoreNode(
//       shape: sphere,
//       position: vector.Vector3(
//         0,
//         0,
//         -1,
//       ),
//     );

//     _arcoreController.addArCoreNode(node);
//   }

//   _addCyclinder(ArCoreController _arcoreController) {
//     final material = ArCoreMaterial(color: Colors.green, reflectance: 1);
//     final cylinder =
//         ArCoreCylinder(materials: [material], radius: 0.3, height: 0.3);
//     final node = ArCoreNode(
//       shape: cylinder,
//       position: vector.Vector3(
//         0,
//         -2.5,
//         -3.0,
//       ),
//     );

//     _arcoreController.addArCoreNode(node);
//   }

//   _addCube(ArCoreController _arcoreController) {
//     final material = ArCoreMaterial(color: Colors.pink, metallic: 1);
//     final cube =
//         ArCoreCube(materials: [material], size: vector.Vector3(1, 1, 1));
//     final node = ArCoreNode(
//       shape: cube,
//       position: vector.Vector3(
//         -0.5,
//         -0.5,
//         -4,
//       ),
//     );

//     _arcoreController.addArCoreNode(node);
//   }

//   @override
//   void dispose() {
//     arCoreController.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(widget.title),
//       ),
//       body: ArCoreView(
//         onArCoreViewCreated: _onArCoreViewCreated,
//       ),
//     );
//   }
// }

//-------------------------------------IOS------------------------------------------

// import 'package:flutter/material.dart';

// import 'package:arkit_plugin/arkit_plugin.dart';
// import 'package:arkit_plugin/geometries/arkit_sphere.dart';
// import 'package:arkit_plugin/widget/arkit_scene_view.dart';
// import 'package:vector_math/vector_math_64.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'VR',
//       home: Sensorama(),
//     );
//   }
// }

// class Sensorama extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: ARKitSceneView(
//             detectionImagesGroupName: 'AR Resourses',
//             onARKitViewCreated: (c) {
//               c.onAddNodeForAnchor = (anchor) {
//                 if (anchor is ARKitImageAnchor) {
//                   final earthPosition = Vector3(
//                     anchor.transform.getColumn(3).x,
//                     anchor.transform.getColumn(3).y,
//                     anchor.transform.getColumn(3).z,);
//                   final node = ARKitNode(
//                     geometry: ARKitSphere(materials: [
//                       ARKitMaterial(
//                         diffuse:
//                             ARKitMaterialProperty(image: 'images/earth.jpg'),
//                         doubleSided: true,
//                       )
//                     ], radius: 0.06),
//                     position: earthPosition,
//                   );
//                   c.add(node);
//                 }
//               };
//             }
//           )
//         );
//   }
// }
// --------------------------------------------CAMERA----------------------------------------------------------

// import 'dart:async';
// import 'dart:io';

// import 'package:camera/camera.dart';
// import 'package:flutter/material.dart';
// import 'package:path/path.dart' show join;
// import 'package:path_provider/path_provider.dart';

// Future<void> main() async {
//   WidgetsFlutterBinding.ensureInitialized();
//   final cameras = await availableCameras();
//   final firstCamera = cameras.first;

//   runApp(
//     MaterialApp(
//       theme: ThemeData.dark(),
//       home: TakePictureScreen(
//         camera: firstCamera,
//       ),
//     ),
//   );
// }

// class TakePictureScreen extends StatefulWidget {
//   final CameraDescription camera;
//   const TakePictureScreen({
//     Key key,
//     @required this.camera,
//   }) : super(key: key);
//   @override
//   TakePictureScreenState createState() => TakePictureScreenState();
// }

// class TakePictureScreenState extends State<TakePictureScreen> {
//   CameraController _controller;
//   Future<void> _initializeControllerFuture;
//   @override
//   void initState() {
//     super.initState();
//     _controller = CameraController(
//       widget.camera,
//       ResolutionPreset.medium,
//     );
//     _initializeControllerFuture = _controller.initialize();
//   }

//   @override
//   void dispose() {
//     _controller.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: Text('Take a picture')),
//       body: FutureBuilder<void>(
//         future: _initializeControllerFuture,
//         builder: (context, snapshot) {
//           if (snapshot.connectionState == ConnectionState.done) {
//             return CameraPreview(_controller);
//           } else {
//             return Center(child: CircularProgressIndicator());
//           }
//         },
//       ),
//       floatingActionButton: Opacity(
//         opacity: 0.5,
//               child: Container(

//           child: FloatingActionButton(
//           child: Icon(Icons.camera_alt),
//           onPressed: () async {
//             try {
//               await _initializeControllerFuture;
//               final path = join(
//                 (await getTemporaryDirectory()).path,
//                 '${DateTime.now()}.png',
//               );
//               await _controller.takePicture(path);
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(
//                   builder: (context) => DisplayPictureScreen(imagePath: path),
//                 ),
//               );
//             } catch (e) {
//               print(e);
//             }
//           },
//         ),
//               alignment: Alignment.center,
//               decoration: BoxDecoration(
//                   image: DecorationImage(
//                 image: AssetImage('assets/login_start.png'),
//               )),
//               // child: ArCoreView(
//               //   onArCoreViewCreated: _onArCoreViewCreated,
//               //   enableTapRecognizer: true,
//               // ),
//             ),
//       ),
//     );
//   }
// }

// class DisplayPictureScreen extends StatelessWidget {
//   final String imagePath;

//   const DisplayPictureScreen({Key key, this.imagePath}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: Text('Display the Picture')),
//       body: Image.file(File(imagePath)),
//     );
//   }
// }

//  ___________________________________TWO LIST VIEWS__________________________________

// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'constants.dart';

// void main() {
//   runApp(MyApp());
// }

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//         visualDensity: VisualDensity.adaptivePlatformDensity,
//       ),
//       home: MyHomePage(),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   final CategoriesScroller categoriesScroller = CategoriesScroller();
//   ScrollController controller = ScrollController();
//   bool closeTopContainer = false;
//   double topContainer = 0;

//   List<Widget> itemsData = [];

//   void getPostsData() {
//     List<dynamic> responseList = FOOD_DATA;
//     List<Widget> listItems = [];
//     responseList.forEach((post) {
//       listItems.add(Container(
//           height: 150,
//           margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
//           decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20.0)), color: Colors.white, boxShadow: [
//             BoxShadow(color: Colors.black.withAlpha(100), blurRadius: 10.0),
//           ]),
//           child: Padding(
//             padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: <Widget>[
//                 Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: <Widget>[
//                     Text(
//                       post["name"],
//                       style: const TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
//                     ),
//                     Text(
//                       post["brand"],
//                       style: const TextStyle(fontSize: 17, color: Colors.grey),
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),
//                     Text(
//                       "\$ ${post["price"]}",
//                       style: const TextStyle(fontSize: 25, color: Colors.black, fontWeight: FontWeight.bold),
//                     )
//                   ],
//                 ),
//                 // Image.asset(
//                 //   "assets/images/${post["image"]}",
//                 //   height: double.infinity,
//                 // )
//               ],
//             ),
//           )));
//     });
//     setState(() {
//       itemsData = listItems;
//     });
//   }

//   @override
//   void initState() {
//     super.initState();
//     getPostsData();
//     controller.addListener(() {

//       double value = controller.offset/119;

//       setState(() {
//         topContainer = value;
//         closeTopContainer = controller.offset > 50;
//       });
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     final Size size = MediaQuery.of(context).size;
//     final double categoryHeight = size.height*0.30;
//     return SafeArea(
//       child: Scaffold(
//         backgroundColor: Colors.white,
//         appBar: AppBar(
//           elevation: 0,
//           backgroundColor: Colors.white,
//           leading: Icon(
//             Icons.menu,
//             color: Colors.black,
//           ),
//           actions: <Widget>[
//             IconButton(
//               icon: Icon(Icons.search, color: Colors.black),
//               onPressed: () {},
//             ),
//             IconButton(
//               icon: Icon(Icons.person, color: Colors.black),
//               onPressed: () {},
//             )
//           ],
//         ),
//         body: Container(
//           height: size.height,
//           child: Column(
//             children: <Widget>[
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceAround,
//                 children: <Widget>[
//                   Text(
//                     "Loyality Cards",
//                     style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 20),
//                   ),
//                   Text(
//                     "Menu",
//                     style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
//                   ),
//                 ],
//               ),
//               const SizedBox(
//                 height: 10,
//               ),
//               AnimatedOpacity(
//                 duration: const Duration(milliseconds: 200),
//                 opacity: closeTopContainer?0:1,
//                 child: AnimatedContainer(
//                     duration: const Duration(milliseconds: 200),
//                     width: size.width,
//                     alignment: Alignment.topCenter,
//                     height: closeTopContainer?0:categoryHeight,
//                     child: categoriesScroller),
//               ),
//               Expanded(
//                   child: ListView.builder(
//                     controller: controller,
//                       itemCount: itemsData.length,
//                       physics: BouncingScrollPhysics(),
//                       itemBuilder: (context, index) {
//                         double scale = 1.0;
//                         if (topContainer > 0.5) {
//                           scale = index + 0.5 - topContainer;
//                           if (scale < 0) {
//                             scale = 0;
//                           } else if (scale > 1) {
//                             scale = 1;
//                           }
//                         }
//                         return Opacity(
//                           opacity: scale,
//                           child: Transform(
//                             transform:  Matrix4.identity()..scale(scale,scale),
//                             alignment: Alignment.bottomCenter,
//                             child: Align(
//                                 heightFactor: 0.7,
//                                 alignment: Alignment.topCenter,
//                                 child: itemsData[index]),
//                           ),
//                         );
//                       })),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// class CategoriesScroller extends StatelessWidget {
//   const CategoriesScroller();

//   @override
//   Widget build(BuildContext context) {
//     final double categoryHeight = MediaQuery.of(context).size.height * 0.30 - 50;
//     return SingleChildScrollView(
//       physics: BouncingScrollPhysics(),
//       scrollDirection: Axis.horizontal,
//       child: Container(
//         margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
//         child: FittedBox(
//           fit: BoxFit.fill,
//           alignment: Alignment.topCenter,
//           child: Row(
//             children: <Widget>[
//               Container(
//                 width: 150,
//                 margin: EdgeInsets.only(right: 20),
//                 height: categoryHeight,
//                 decoration: BoxDecoration(color: Colors.orange.shade400, borderRadius: BorderRadius.all(Radius.circular(20.0))),
//                 child: Padding(
//                   padding: const EdgeInsets.all(12.0),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: <Widget>[
//                       Text(
//                         "Most\nFavorites",
//                         style: TextStyle(fontSize: 25, color: Colors.white, fontWeight: FontWeight.bold),
//                       ),
//                       SizedBox(
//                         height: 10,
//                       ),
//                       Text(
//                         "20 Items",
//                         style: TextStyle(fontSize: 16, color: Colors.white),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//               Container(
//                 width: 150,
//                 margin: EdgeInsets.only(right: 20),
//                 height: categoryHeight,
//                 decoration: BoxDecoration(color: Colors.blue.shade400, borderRadius: BorderRadius.all(Radius.circular(20.0))),
//                 child: Container(
//                   child: Padding(
//                     padding: const EdgeInsets.all(12.0),
//                     child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: <Widget>[
//                         Text(
//                           "Newest",
//                           style: TextStyle(fontSize: 25, color: Colors.white, fontWeight: FontWeight.bold),
//                         ),
//                         SizedBox(
//                           height: 10,
//                         ),
//                         Text(
//                           "20 Items",
//                           style: TextStyle(fontSize: 16, color: Colors.white),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ),
//               Container(
//                 width: 150,
//                 margin: EdgeInsets.only(right: 20),
//                 height: categoryHeight,
//                 decoration: BoxDecoration(color: Colors.lightBlueAccent.shade400, borderRadius: BorderRadius.all(Radius.circular(20.0))),
//                 child: Padding(
//                   padding: const EdgeInsets.all(12.0),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: <Widget>[
//                       Text(
//                         "Super\nSaving",
//                         style: TextStyle(fontSize: 25, color: Colors.white, fontWeight: FontWeight.bold),
//                       ),
//                       SizedBox(
//                         height: 10,
//                       ),
//                       Text(
//                         "20 Items",
//                         style: TextStyle(fontSize: 16, color: Colors.white),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// _________________________________________________MAPS_________________________________________________________________

// import 'dart:async';
// import 'dart:typed_data';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:location/location.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       title: 'Flutter Maps',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: MyHomePage(title: 'Flutter Map Home Page'),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   MyHomePage({Key key, this.title}) : super(key: key);
//   final String title;

//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   StreamSubscription _locationSubscription;
//   Location _locationTracker = Location();
//   Marker marker;
//   Circle circle;
//   GoogleMapController _controller;

//   static final CameraPosition initialLocation = CameraPosition(
//     target: LatLng(37.42796133580664, -122.085749655962),
//     zoom: 14.4746,
//   );

//   Future<Uint8List> getMarker() async {
//     ByteData byteData = await DefaultAssetBundle.of(context).load("assets/car_icon.png");
//     return byteData.buffer.asUint8List();
//   }

//   void updateMarkerAndCircle(LocationData newLocalData, Uint8List imageData) {
//     LatLng latlng = LatLng(newLocalData.latitude, newLocalData.longitude);
//     this.setState(() {
//       marker = Marker(
//           markerId: MarkerId("home"),
//           position: latlng,
//           rotation: newLocalData.heading,
//           draggable: false,
//           zIndex: 2,
//           flat: true,
//           anchor: Offset(0.5, 0.5),
//           icon: BitmapDescriptor.fromBytes(imageData));
//       circle = Circle(
//           circleId: CircleId("car"),
//           radius: newLocalData.accuracy,
//           zIndex: 1,
//           strokeColor: Colors.blue,
//           center: latlng,
//           fillColor: Colors.blue.withAlpha(70));
//     });
//   }

//   void getCurrentLocation() async {
//     try {

//       Uint8List imageData = await getMarker();
//       var location = await _locationTracker.getLocation();

//       updateMarkerAndCircle(location, imageData);

//       if (_locationSubscription != null) {
//         _locationSubscription.cancel();
//       }

//       _locationSubscription = _locationTracker.onLocationChanged.listen((newLocalData) {
//         if (_controller != null) {
//           _controller.animateCamera(CameraUpdate.newCameraPosition(new CameraPosition(
//               bearing: 192.8334901395799,
//               target: LatLng(newLocalData.latitude, newLocalData.longitude),
//               tilt: 0,
//               zoom: 18.00)));
//           updateMarkerAndCircle(newLocalData, imageData);
//         }
//       });

//     } on PlatformException catch (e) {
//       if (e.code == 'PERMISSION_DENIED') {
//         debugPrint("Permission Denied");
//       }
//     }
//   }

//   @override
//   void dispose() {
//     if (_locationSubscription != null) {
//       _locationSubscription.cancel();
//     }
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(widget.title),
//       ),
//       body: GoogleMap(
//         mapType: MapType.hybrid,
//         initialCameraPosition: initialLocation,
//         markers: Set.of((marker != null) ? [marker] : []),
//         circles: Set.of((circle != null) ? [circle] : []),
//         onMapCreated: (GoogleMapController controller) {
//           _controller = controller;
//         },

//       ),
//       floatingActionButton: FloatingActionButton(
//           child: Icon(Icons.location_searching),
//           onPressed: () {
//             getCurrentLocation();
//           }),
//     );
//   }
// }

// -----------------------------------ARCoreAndroid-------------------------------------

// import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
// import 'package:flutter/material.dart';
// import 'package:vector_math/vector_math_64.dart' as vector;

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter AR',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: DemoPage(),
//     );
//   }
// }

// class DemoPage extends StatefulWidget {
//   @override
//   _DemoPageState createState() => _DemoPageState();
// }

// class _DemoPageState extends State<DemoPage> {
//   ArCoreController controller;
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: Text('Demo')),
//       body: ArCoreView(
//         // configuration: 'AR Resourses',
//         onArCoreViewCreated: (c) => onArCoreViewCreated(c)),
//     );
//   }

//   void onArCoreViewCreated(ArCoreController c){
//     this.controller = c;
//     // this.controller.addArCoreNodeWithAnchor = addArCoreNodeWithAnchor;
//   }

//   void addArCoreNodeWithAnchor(ArCoreCylinder anchor)

//   }

// --------------------------------------------------CHAT------------------------------------------------------------

// import 'package:flutter/material.dart';
// import './screens/home_screen.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Chat',
//       debugShowCheckedModeBanner: false,
//       theme: ThemeData(
//         // primarySwatch: Colors.orange[900],
//         accentColor: Colors.white,
//         ),
//       home: MyApp9(),
//     );
//   }
// }

// ______________________________GOOGLE MAPS ROUTES____________________________________________

// import 'package:flutter/material.dart';
// import 'package:google_map_polyline/google_map_polyline.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:permission/permission.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: MyHomePage(),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   final Set<Polyline> polyline = {};

//   GoogleMapController _controller;
//   List<LatLng> routeCoords;
//   GoogleMapPolyline googleMapPolyline =
//      // new GoogleMapPolyline(apiKey: "AIzaSyAHDpDI5Tx7qYzeAP9HtgWV71cKmqM-AoE");
//       new GoogleMapPolyline(apiKey: "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM");

// getsomePoints() async {
//   var permissions =
//       await Permission.getPermissionsStatus([PermissionName.Location]);
//   if (permissions[0].permissionStatus == PermissionStatus.notAgain) {
//     var askpermissions =
//         await Permission.requestPermissions([PermissionName.Location]);
//   } else {
//     routeCoords = await googleMapPolyline.getCoordinatesWithLocation(
//         origin: LatLng(40.6782, -73.9442),
//         destination: LatLng(40.6944, -73.9212),
//         mode: RouteMode.driving);
//   }
// }

// getaddressPoints() async {
//   routeCoords = await googleMapPolyline.getPolylineCoordinatesWithAddress(
//           origin: '55 Kingston Ave, Brooklyn, NY 11213, USA',
//           destination: '178 Broadway, Brooklyn, NY 11211, USA',
//           mode: RouteMode.driving);
// }

//   @override
//   void initState() {
//     super.initState();
//     getsomePoints();
//     // getaddressPoints();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: GoogleMap(
//       onMapCreated: onMapCreated,
//       polylines: polyline,
//       initialCameraPosition:
//           CameraPosition(target: LatLng(40.6782, -73.9442), zoom: 14.0),
//       mapType: MapType.normal,
//     ));
//   }

//   void onMapCreated(GoogleMapController controller) {
//     setState(() {
//       _controller = controller;

//       polyline.add(Polyline(
//           polylineId: PolylineId('route1'),
//           visible: true,
//           points: routeCoords,
//           width: 4,
//           color: Colors.blue,
//           startCap: Cap.roundCap,
//           endCap: Cap.buttCap));
//     });
//   }
// }

// ---------------------------------------POLYLINES-----------------------------------------------------

// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Polyline example',
//       theme: ThemeData(
//         // This is the theme of your application.
//         //
//         // Try running your application with "flutter run". You'll see the
//         // application has a blue toolbar. Then, without quitting the app, try
//         // changing the primarySwatch below to Colors.green and then invoke
//         // "hot reload" (press "r" in the console where you ran "flutter run",
//         // or simply save your changes to "hot reload" in a Flutter IDE).
//         // Notice that the counter didn't reset back to zero; the application
//         // is not restarted.
//         primarySwatch: Colors.orange,
//       ),
//       home: MapScreen(),
//     );
//   }
// }

// class MapScreen extends StatefulWidget {
//   @override
//   _MapScreenState createState() => _MapScreenState();
// }

// class _MapScreenState extends State<MapScreen> {
//   GoogleMapController mapController;
//   double _originLatitude = 50.450373, _originLongitude = 30.511032;
//   double _destLatitude = 50.447497, _destLongitude = 30.522705;
//   Map<MarkerId, Marker> markers = {};
//   Map<PolylineId, Polyline> polylines = {};
//   List<LatLng> polylineCoordinates = [];
//   PolylinePoints polylinePoints = PolylinePoints();
//   String googleAPiKey = "AIzaSyAHDpDI5Tx7qYzeAP9HtgWV71cKmqM-AoE";

//   @override
//   void initState() {
//     super.initState();

//     /// origin marker
//     _addMarker(LatLng(_originLatitude, _originLongitude), "origin",
//         BitmapDescriptor.defaultMarker);

//     /// destination marker
//     _addMarker(LatLng(_destLatitude, _destLongitude), "destination",
//         BitmapDescriptor.defaultMarkerWithHue(90));
//     _getPolyline();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//       child: Scaffold(
//           body: GoogleMap(
//             initialCameraPosition: CameraPosition(
//                 target: LatLng(_originLatitude, _originLongitude), zoom: 15),
//             myLocationEnabled: true,
//             tiltGesturesEnabled: true,
//             compassEnabled: true,
//             scrollGesturesEnabled: true,
//             zoomGesturesEnabled: true,
//             onMapCreated: _onMapCreated,
//             markers: Set<Marker>.of(markers.values),
//             polylines: Set<Polyline>.of(polylines.values),
//           )),
//     );
//   }

//   void _onMapCreated(GoogleMapController controller) async {
//     mapController = controller;
//   }

//   _addMarker(LatLng position, String id, BitmapDescriptor descriptor) {
//     MarkerId markerId = MarkerId(id);
//     Marker marker =
//     Marker(markerId: markerId, icon: descriptor, position: position);
//     markers[markerId] = marker;
//   }

//   _addPolyLine() {
//     PolylineId id = PolylineId("poly");
//     Polyline polyline = Polyline(
//         polylineId: id,
//         visible: true,
//         color: Colors.red,
//         points: polylineCoordinates,
//         width: 4,
//         startCap: Cap.roundCap,
//         endCap: Cap.buttCap,);
//     polylines[id] = polyline;
//     setState(() {});
//   }

//   _getPolyline() async {
//     PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
//         "AIzaSyAHDpDI5Tx7qYzeAP9HtgWV71cKmqM-AoE",
//         PointLatLng(_originLatitude, _originLongitude),
//         PointLatLng(_destLatitude, _destLongitude),
//         travelMode: TravelMode.walking,
//       wayPoints: [PolylineWayPoint(location: "Kiev")]
//     );
//     if (result.points.isNotEmpty) {
//       result.points.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));

//       });
//     }
//     _addPolyLine();
//   }
// }

// -----------------------------------------------Routelines------------------------------------------------------

// import 'package:flutter_polyline_points/flutter_polyline_points.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'dart:async';
// import 'package:flutter/material.dart';

// void main() => runApp(MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: MapPage(),
//     ));

// const double CAMERA_ZOOM = 13;
// const double CAMERA_TILT = 0;
// const double CAMERA_BEARING = 30;
// // const LatLng SOURCE_LOCATION = LatLng(42.7477863, -71.1699932);
// // const LatLng DEST_LOCATION = LatLng(42.6871386, -71.2143403);

//   double _originLatitude = 50.450373, _originLongitude = 30.511032;
//   double _destLatitude = 50.447497, _destLongitude = 30.522705;

// class MapPage extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() => MapPageState();
// }

// class MapPageState extends State<MapPage> {
//     Completer<GoogleMapController> _controller = Completer();
//     // this set will hold my markers
//     Set<Marker> _markers = {};
//     // this will hold the generated polylines
//     Set<Polyline> _polylines = {};
//     // this will hold each polyline coordinate as Lat and Lng pairs
//     List<LatLng> polylineCoordinates = [];
//     // this is the key object - the PolylinePoints
//     // which generates every polyline between start and finish
//     PolylinePoints polylinePoints = PolylinePoints();
//     String googleAPIKey = "AIzaSyAHDpDI5Tx7qYzeAP9HtgWV71cKmqM-AoE";
//     // for my custom icons
//     BitmapDescriptor sourceIcon;
//     BitmapDescriptor destinationIcon;

//     @override
//     void initState() {
//       super.initState();
//       setSourceAndDestinationIcons();
//     }

//     void setSourceAndDestinationIcons() async {
//       sourceIcon = await BitmapDescriptor.fromAssetImage(
//           ImageConfiguration(devicePixelRatio: 2.5), 'assets/map_icon.png');
//       destinationIcon = await BitmapDescriptor.fromAssetImage(
//           ImageConfiguration(devicePixelRatio: 2.5),
//           'assets/map_icon.png');
//     }

//     @override
//     Widget build(BuildContext context) {
//       CameraPosition initialLocation = CameraPosition(
//           zoom: CAMERA_ZOOM,
//           bearing: CAMERA_BEARING,
//           tilt: CAMERA_TILT,
//           target: LatLng(_originLatitude, _originLongitude));
//       return GoogleMap(
//           myLocationEnabled: true,
//           compassEnabled: true,
//           tiltGesturesEnabled: false,
//           markers: _markers,
//           polylines: _polylines,
//           mapType: MapType.normal,
//           initialCameraPosition: initialLocation,
//           onMapCreated: onMapCreated);
//     }

//     void onMapCreated(GoogleMapController controller) {
//       controller.setMapStyle(Utils.mapStyles);
//       _controller.complete(controller);
//       setMapPins();
//       setPolylines();
//     }

//     void setMapPins() {
//       setState(() {
//         // source pin
//         _markers.add(Marker(
//             markerId: MarkerId('sourcePin'),
//             position: LatLng(_originLatitude, _originLongitude),
//             icon: sourceIcon));
//         // destination pin
//         _markers.add(Marker(
//             markerId: MarkerId('destPin'),
//             position: LatLng(_destLatitude, _destLongitude),
//             icon: destinationIcon));
//       });
//     }

//     setPolylines() async {

//         PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
//             "AIzaSyAHDpDI5Tx7qYzeAP9HtgWV71cKmqM-AoE",
//             PointLatLng(_originLatitude, _originLongitude),
//             PointLatLng(_destLatitude, _destLongitude),
//             );
//         if (result.points.isNotEmpty) {
//           // loop through all PointLatLng points and convert them
//           // to a list of LatLng, required by the Polyline
//           result.points.forEach((PointLatLng point) {
//             polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//           });
//         }

//       setState(() {
//           // create a Polyline instance
//           // with an id, an RGB color and the list of LatLng pairs
//           Polyline polyline = Polyline(
//               polylineId: PolylineId("poly"),
//               color: Color.fromARGB(255, 40, 122, 198),
//               points: polylineCoordinates);

//           // add the constructed polyline as a set of points
//           // to the polyline set, which will eventually
//           // end up showing up on the map
//           _polylines.add(polyline);
//       });
//   }
// }
// class Utils {
//   static String mapStyles = '''[
//   {
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#f5f5f5"
//       }
//     ]
//   },
//   {
//     "elementType": "labels.icon",
//     "stylers": [
//       {
//         "visibility": "off"
//       }
//     ]
//   },
//   {
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#616161"
//       }
//     ]
//   },
//   {
//     "elementType": "labels.text.stroke",
//     "stylers": [
//       {
//         "color": "#f5f5f5"
//       }
//     ]
//   },
//   {
//     "featureType": "administrative.land_parcel",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#bdbdbd"
//       }
//     ]
//   },
//   {
//     "featureType": "poi",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#eeeeee"
//       }
//     ]
//   },
//   {
//     "featureType": "poi",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#757575"
//       }
//     ]
//   },
//   {
//     "featureType": "poi.park",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#e5e5e5"
//       }
//     ]
//   },
//   {
//     "featureType": "poi.park",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#9e9e9e"
//       }
//     ]
//   },
//   {
//     "featureType": "road",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#ffffff"
//       }
//     ]
//   },
//   {
//     "featureType": "road.arterial",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#757575"
//       }
//     ]
//   },
//   {
//     "featureType": "road.highway",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#dadada"
//       }
//     ]
//   },
//   {
//     "featureType": "road.highway",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#616161"
//       }
//     ]
//   },
//   {
//     "featureType": "road.local",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#9e9e9e"
//       }
//     ]
//   },
//   {
//     "featureType": "transit.line",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#e5e5e5"
//       }
//     ]
//   },
//   {
//     "featureType": "transit.station",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#eeeeee"
//       }
//     ]
//   },
//   {
//     "featureType": "water",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#c9c9c9"
//       }
//     ]
//   },
//   {
//     "featureType": "water",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#9e9e9e"
//       }
//     ]
//   }
// ]''';
// }

// -----------------------------------------GOOGLE-MAP-WOMAN-NEW-YORK--------------------------------------------

// import 'dart:async';
// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Polyline example',
//       theme: ThemeData(
//         primarySwatch: Colors.orange,
//       ),
//       home: MapScreen(),
//     );
//   }
// }

// class MapScreen extends StatefulWidget {
//   @override
//   MapScreenState createState() => MapScreenState();
// }

// class MapScreenState extends State<MapScreen> {
//   Completer<GoogleMapController> _controller = Completer();

//   @override
//   void initState() {
//     super.initState();
//   }

//   double zoomVal = 5.0;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         leading: IconButton(
//             icon: Icon(FontAwesomeIcons.arrowLeft),
//             onPressed: () {
//               //
//             }),
//         title: Text("New York"),
//         actions: <Widget>[
//           IconButton(
//               icon: Icon(FontAwesomeIcons.search),
//               onPressed: () {
//                 //
//               }),
//         ],
//       ),
//       body: Stack(children: <Widget>[
//         _googlemap(context),
//         _zoomminusfunction(),
//         _zoomplusfunction(),
//         _buildContainer(),
//       ]),
//     );
//   }

//   Widget _zoomminusfunction() {

//     return Container(
//       margin: EdgeInsets.only(top: 80),
//       child: Align(
//         alignment: Alignment.topLeft,
//         child: IconButton(
//               icon: Icon(FontAwesomeIcons.searchMinus,color:Color(0xff6200ee)),
//               onPressed: () {
//                 zoomVal--;
//                _minus( zoomVal);
//               }),
//       ),
//     );
//  }

//   Future<void> _minus(double zoomVal) async {
//     final GoogleMapController controller = await _controller.future;
//     controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: LatLng(40.712776, -74.005974), zoom: zoomVal)));
//   }

//   Widget _zoomplusfunction() {

//     return Container(
//       margin: EdgeInsets.only(top: 40),
//       child: Align(
//         alignment: Alignment.topLeft,
//         child: IconButton(
//               icon: Icon(FontAwesomeIcons.searchPlus,color:Color(0xff6200ee)),
//               onPressed: () {
//                 zoomVal++;
//                 _plus(zoomVal);
//               }),
//       ),
//     );
//  }

//   Future<void> _plus(double zoomVal) async {
//     final GoogleMapController controller = await _controller.future;
//     controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: LatLng(40.712776, -74.005974), zoom: zoomVal)));
//   }

//   Widget _buildContainer() {
//     return Align(
//       alignment: Alignment.bottomLeft,
//       child: Container(
//         margin: EdgeInsets.symmetric(vertical: 20.0),
//         height: 150.0,
//         child: ListView(
//           physics: BouncingScrollPhysics(),
//           scrollDirection: Axis.horizontal,
//           children: <Widget>[
//             SizedBox(width: 10.0),
//             Padding(
//               padding: const EdgeInsets.all(8.0),
//               child: _boxes(
//                   "https://lh5.googleusercontent.com/p/AF1QipO3VPL9m-b355xWeg4MXmOQTauFAEkavSluTtJU=w225-h160-k-no",
//                   40.738380,
//                   -73.988426,
//                   "Gramercy Tavern"),
//             ),
//             SizedBox(width: 10.0),
//             Padding(
//               padding: const EdgeInsets.all(8.0),
//               child: _boxes(
//                   "https://lh5.googleusercontent.com/p/AF1QipMKRN-1zTYMUVPrH-CcKzfTo6Nai7wdL7D8PMkt=w340-h160-k-no",
//                   40.761421,
//                   -73.981667,
//                   "Le Bernardin"),
//             ),
//             SizedBox(width: 10.0),
//             Padding(
//               padding: const EdgeInsets.all(8.0),
//               child: _boxes(
//                   "https://images.unsplash.com/photo-1504940892017-d23b9053d5d4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
//                   40.732128,
//                   -73.999619,
//                   "Blue Hill"),
//             ),
//           ],
//         ),
//       ),
//     );
//   }

//   Widget _boxes(String _image, double lat, double long, String restaurantName) {
//     return GestureDetector(
//       onTap: () {
//         _gotoLocation(lat, long);
//       },
//       child: Container(
//         child: new FittedBox(
//           child: Material(
//               color: Colors.white,
//               elevation: 14.0,
//               borderRadius: BorderRadius.circular(24.0),
//               shadowColor: Color(0x802196F3),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: <Widget>[
//                   Container(
//                     width: 180,
//                     height: 200,
//                     child: ClipRRect(
//                       borderRadius: new BorderRadius.circular(24.0),
//                       child: Image(
//                         fit: BoxFit.fill,
//                         image: NetworkImage(_image),
//                       ),
//                     ),
//                   ),
//                   Container(
//                     child: Padding(
//                       padding: const EdgeInsets.all(8.0),
//                       child: myDetailsContainer(restaurantName),
//                     ),
//                   ),
//                 ],
//               )),
//         ),
//       ),
//     );
//   }

//   Widget myDetailsContainer(String restaurantName) {
//     return Column(
//       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//       children: <Widget>[
//         Padding(
//           padding: const EdgeInsets.only(left: 8.0),
//           child: Container(
//               child: Text(restaurantName,
//             style: TextStyle(
//                 color: Color(0xff6200ee),
//                 fontSize: 24.0,
//                 fontWeight: FontWeight.bold),
//           )),
//         ),
//         SizedBox(height:5.0),
//         Container(
//               child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             children: <Widget>[
//               Container(
//                   child: Text(
//                 "4.1",
//                 style: TextStyle(
//                   color: Colors.black54,
//                   fontSize: 18.0,
//                 ),
//               )),
//               Container(
//                 child: Icon(
//                   FontAwesomeIcons.solidStar,
//                   color: Colors.amber,
//                   size: 15.0,
//                 ),
//               ),
//               Container(
//                 child: Icon(
//                   FontAwesomeIcons.solidStar,
//                   color: Colors.amber,
//                   size: 15.0,
//                 ),
//               ),
//               Container(
//                 child: Icon(
//                   FontAwesomeIcons.solidStar,
//                   color: Colors.amber,
//                   size: 15.0,
//                 ),
//               ),
//               Container(
//                 child: Icon(
//                   FontAwesomeIcons.solidStar,
//                   color: Colors.amber,
//                   size: 15.0,
//                 ),
//               ),
//               Container(
//                 child: Icon(
//                   FontAwesomeIcons.solidStarHalf,
//                   color: Colors.amber,
//                   size: 15.0,
//                 ),
//               ),
//                Container(
//                   child: Text(
//                 "(946)",
//                 style: TextStyle(
//                   color: Colors.black54,
//                   fontSize: 18.0,
//                 ),
//               )),
//             ],
//           )),
//           SizedBox(height:5.0),
//         Container(
//                   child: Text(
//                 "American \u00B7 \u0024\u0024 \u00B7 1.6 mi",
//                 style: TextStyle(
//                   color: Colors.black54,
//                   fontSize: 18.0,
//                 ),
//               )),
//               SizedBox(height:5.0),
//         Container(
//             child: Text(
//           "Closed \u00B7 Opens 17:00 Thu",
//           style: TextStyle(
//               color: Colors.black54,
//               fontSize: 18.0,
//               fontWeight: FontWeight.bold),
//         )),
//       ],
//     );
//   }

//   Widget _googlemap(BuildContext context) {
//     return Container(
//       height: MediaQuery.of(context).size.height,
//       width: MediaQuery.of(context).size.width,
//       child: GoogleMap(
//         myLocationEnabled: true,
//         // tiltGesturesEnabled: true,
//         compassEnabled: true,
//         // scrollGesturesEnabled: true,
//         // zoomGesturesEnabled: true,
//         mapType: MapType.normal,
//         initialCameraPosition:
//             CameraPosition(target: LatLng(40.712776, -74.005974), zoom: 12),
//         onMapCreated: (GoogleMapController controller) {
//           _controller.complete(controller);
//         },
//         markers: {
//           newyorkMarker,
//           newyork2Marker,
//           gramercyMarker,
//           bernardinMarker,
//           blueMarker
//         },
//       ),
//     );
//   }

//   Future<void> _gotoLocation(double lat, double long) async {
//     final GoogleMapController controller = await _controller.future;
//     controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
//         target: LatLng(lat, long), zoom: 15, tilt: 50.0, bearing: 45.0)));
//   }
// }

// Marker gramercyMarker = Marker(
//   markerId: MarkerId('gramercy'),
//   position: LatLng(40.738380, -73.988426),
//   infoWindow: InfoWindow(title: 'Gramercy Tavern'),
//   icon: BitmapDescriptor.defaultMarkerWithHue(
//     BitmapDescriptor.hueViolet,
//   ),
// );

// Marker bernardinMarker = Marker(
//   markerId: MarkerId('bernardin'),
//   position: LatLng(40.761421, -73.981667),
//   infoWindow: InfoWindow(title: 'Le Bernardin'),
//   icon: BitmapDescriptor.defaultMarkerWithHue(
//     BitmapDescriptor.hueViolet,
//   ),
// );
// Marker blueMarker = Marker(
//   markerId: MarkerId('bluehill'),
//   position: LatLng(40.732128, -73.999619),
//   infoWindow: InfoWindow(title: 'Blue Hill'),
//   icon: BitmapDescriptor.defaultMarkerWithHue(
//     BitmapDescriptor.hueViolet,
//   ),
// );

// Marker newyorkMarker = Marker(
//     markerId: MarkerId('newyork1'),
//     position: LatLng(40.742451, -74.005959),
//     infoWindow: InfoWindow(title: 'Los Tacos'),
//     icon: BitmapDescriptor.defaultMarkerWithHue(
//       BitmapDescriptor.hueViolet,
//     ));

// Marker newyork2Marker = Marker(
//     markerId: MarkerId('newyork2'),
//     position: LatLng(40.729640, -73.983510),
//     infoWindow: InfoWindow(title: 'Tree Bistro'),
//     icon: BitmapDescriptor.defaultMarkerWithHue(
//       BitmapDescriptor.hueViolet,
//     ));

// --------------------google_maps San Francisco: marker, poligons, polylines, circles, mapStyles---------------------

// import 'dart:collection';
// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Polyline example',
//       theme: ThemeData(
//         primarySwatch: Colors.orange,
//       ),
//       home: GMap(),
//     );
//   }
// }

// class GMap extends StatefulWidget {
//   GMap({Key key}) : super(key: key);

//   @override
//   _GMapState createState() => _GMapState();
// }

// class _GMapState extends State<GMap> {
//   Set<Marker> _markers = LinkedHashSet<Marker>();
//   Set<Polygon> _polygons = LinkedHashSet<Polygon>();
//   Set<Polyline> _polylines = LinkedHashSet<Polyline>();
//   Set<Circle> _circles = LinkedHashSet<Circle>();

//   GoogleMapController _mapController;
//   BitmapDescriptor _markerIcon;

//   @override
//   void initState() {
//     super.initState();
//     _setMarkersIcon();
//     _setPolygons();
//     _setPolylines();
//     _setCircles();
//   }

//   void _setMarkersIcon() async {
//     _markerIcon = await BitmapDescriptor.fromAssetImage(
//         ImageConfiguration(), 'assets/map_icon.png');
//   }

//   void _setMapStyle() async {
//     String style = await DefaultAssetBundle.of(context).loadString('assets/map_style.json');
//     _mapController.setMapStyle(style);
//   }

//   void _setPolygons() {
//     List<LatLng> polygonLatLongs = List<LatLng>();
//     polygonLatLongs.add(LatLng(37.78493, -122.42932));
//     polygonLatLongs.add(LatLng(37.78693, -122.41942));
//     polygonLatLongs.add(LatLng(37.78923, -122.41542));
//     polygonLatLongs.add(LatLng(37.78209, -122.42182));

//     _polygons.add(
//       Polygon(
//         polygonId: PolygonId("0"),
//         points: polygonLatLongs,
//         fillColor: Colors.white,
//         strokeWidth: 1,
//       ),
//     );

//   }

//   void _setPolylines() {
//     List<LatLng> polylineLatLongs = List<LatLng>();
//     polylineLatLongs.add(LatLng(37.74493, -122.42932));
//     polylineLatLongs.add(LatLng(37.74693, -122.41942));
//     polylineLatLongs.add(LatLng(37.74923, -122.41542));
//     polylineLatLongs.add(LatLng(37.74923, -122.42582));
//     // polylineLatLongs.add(LatLng(37.74493, -122.42923));

//             _polylines.add(
//       Polyline(
//         polylineId: PolylineId("0"),
//         points: polylineLatLongs,
//         color: Colors.purple,
//         width: 1,
//       ),
//     );
//   }

//   void _setCircles(){
//     _circles.add(Circle(circleId: CircleId("0"),
//     center: LatLng(37.76493, -122.42432),
//     radius: 1000,
//     strokeWidth: 2,
//     fillColor: Color.fromRGBO(102, 51, 153, .5)
//     ));
//   }

//   void _onMapCreated(GoogleMapController controller) {
//     _mapController = controller;

//     setState(() {
//       _markers.add(
//         Marker(
//             markerId: MarkerId('0'),
//             position: LatLng(37.77483, -122.41942),
//             infoWindow: InfoWindow(
//                 title: 'San Francisco', snippet: 'An interesting city'),
//             icon: _markerIcon),
//       );
//     });
//     _setMapStyle();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           backgroundColor: Colors.lightGreen[200],
//           title: Center(child: Text('Map')),
//         ),
//         body: Stack(
//           children: <Widget>[
//             GoogleMap(
//               onMapCreated: _onMapCreated,
//               initialCameraPosition: CameraPosition(
//                 target: LatLng(37.77483, -122.41942),
//                 zoom: 12,
//               ),
//               markers: _markers,
//               polygons: _polygons,
//               polylines: _polylines,
//               circles: _circles,
//               myLocationEnabled: true,
//             ),
//             Container(
//               alignment: Alignment.bottomCenter,
//               padding: EdgeInsets.fromLTRB(0, 0, 0, 32),
//               child: Text('Google map for App'),
//             )
//           ],
//         ));
//   }
// }

// --------------------------------------flutter_polyline_points 0.2.2----------------------------------------------------

// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';
// import 'package:geolocator/geolocator.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Polyline example',
//       theme: ThemeData(
//         // This is the theme of your application.
//         //
//         // Try running your application with "flutter run". You'll see the
//         // application has a blue toolbar. Then, without quitting the app, try
//         // changing the primarySwatch below to Colors.green and then invoke
//         // "hot reload" (press "r" in the console where you ran "flutter run",
//         // or simply save your changes to "hot reload" in a Flutter IDE).
//         // Notice that the counter didn't reset back to zero; the application
//         // is not restarted.
//         primarySwatch: Colors.orange,
//       ),
//       home: MapScreen(),
//     );
//   }
// }

// class MapScreen extends StatefulWidget {
//   @override
//   _MapScreenState createState() => _MapScreenState();
// }

// class _MapScreenState extends State<MapScreen> {
//   var currentLocation;
//   GoogleMapController mapController;
//   double _originLatitude = 37.423351, _originLongitude = -122.078315;
//   double _destLatitude = 37.423697, _destLongitude = -122.090152;
//   // double _originLatitude2 = 37.423697, _originLongitude2 = -122.090152; end of route 1 = start of route 2 !!!
//   double _originLatitude2 = 37.421532, _originLongitude2 = -122.088667;
//   double _destLatitude2 = 37.420519, _destLongitude2 = -122.078464;

//   Map<MarkerId, Marker> markers = {};
//   Map<PolylineId, Polyline> polylines = {};
//   List<LatLng> polylineCoordinates = [];
//   List<LatLng> polylineCoordinates2 = [];
//   PolylinePoints polylinePoints = PolylinePoints();
//   PolylinePoints polylinePoints2 = PolylinePoints();
//   String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

//   @override
//   void initState() {
//     super.initState();

//     Geolocator().getCurrentPosition().then((currloc) {
//       setState(() {
//         currentLocation = currloc;
//       });
//     });

//     /// first route
//     _addMarker(LatLng(_originLatitude, _originLongitude), "origin",
//         BitmapDescriptor.defaultMarker, InfoWindow(title: 'Start point'));
//     _addMarker(LatLng(_destLatitude, _destLongitude), "destination",
//         BitmapDescriptor.defaultMarkerWithHue(90), InfoWindow(title: 'End point'));
//     _getPolyline();

//     /// second route
//     _addMarker(LatLng(_originLatitude2, _originLongitude2), "origin2",
//         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange), InfoWindow(title: 'Start point 2'));
//     _addMarker(LatLng(_destLatitude2, _destLongitude2), "destination2",
//         BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueYellow), InfoWindow(title: 'End point 2'));
//     _getPolyline2();

//   }

//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//       child: Scaffold(
//           body: GoogleMap(
//         initialCameraPosition: CameraPosition(
//             target: LatLng(currentLocation.latitude, currentLocation.longitude),
//             zoom: 15),
//         myLocationEnabled: true,
//         tiltGesturesEnabled: true,
//         compassEnabled: true,
//         scrollGesturesEnabled: true,
//         zoomGesturesEnabled: true,
//         onMapCreated: _onMapCreated,
//         markers: Set<Marker>.of(markers.values),
//         polylines: Set<Polyline>.of(polylines.values),
//       )),
//     );
//   }

//   void _onMapCreated(GoogleMapController controller) async {
//     mapController = controller;
//   }

//   _addMarker(LatLng position, String id, BitmapDescriptor descriptor, InfoWindow infowindow) {
//     MarkerId markerId = MarkerId(id);
//     Marker marker = Marker(
//       markerId: markerId,
//       icon: descriptor,
//       position: position,
//       infoWindow: infowindow
//     );
//     markers[markerId] = marker;
//   }

//   // first polyline
//   _addPolyLine() {
//     PolylineId id = PolylineId("poly");
//     Polyline polyline = Polyline(
//         polylineId: id, color: Colors.red, points: polylineCoordinates, width: 3);
//     polylines[id] = polyline;
//     setState(() {});
//   }

//   _getPolyline() async {
//     PolylineResult result1 = await polylinePoints.getRouteBetweenCoordinates(
//       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//       PointLatLng(_originLatitude, _originLongitude),
//       PointLatLng(_destLatitude, _destLongitude),
//       travelMode: TravelMode.driving,
//       // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]
//     );
//     if (result1.points.isNotEmpty) {
//       result1.points.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });
//     }
//     _addPolyLine();
//   }

// // second polyline
//   _addPolyLine2() {
//     PolylineId id = PolylineId("poly2");
//     Polyline polyline2 = Polyline(
//         polylineId: id, color: Colors.orange, points: polylineCoordinates2, width: 3);
//     polylines[id] = polyline2;
//     setState(() {});
//   }

//   _getPolyline2() async {
//     PolylineResult result2 = await polylinePoints2.getRouteBetweenCoordinates(
//       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//       PointLatLng(_originLatitude2, _originLongitude2),
//       PointLatLng(_destLatitude2, _destLongitude2),
//       travelMode: TravelMode.driving,
//     );
//     if (result2.points.isNotEmpty) {
//       result2.points.forEach((PointLatLng point2) {
//         polylineCoordinates2.add(LatLng(point2.latitude, point2.longitude));
//       });
//     }
//     _addPolyLine2();
//   }

// }

// _______________________________________GEOLOCATION_MY_DEVICE_________________________________________________________

// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: MyHomePage(),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   bool mapToggle = false;

//   var currentLocation;

//   GoogleMapController mapController;

//   double _originLatitude = 37.423351, _originLongitude = -122.078315;
//   double _destLatitude = 37.423697, _destLongitude = -122.090152;
//   Map<MarkerId, Marker> markers = {};
//   Map<PolylineId, Polyline> polylines = {};
//   List<LatLng> polylineCoordinates = [];
//   PolylinePoints polylinePoints = PolylinePoints();
//   String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

// void initState() {
//   super.initState();
//   Geolocator().getCurrentPosition().then((currloc) {
//     setState(() {
//       currentLocation = currloc;
//       mapToggle = true;

//     });
//   });

//   // _addPolyLine() {
//   //   PolylineId id = PolylineId("poly");
//   //   Polyline polyline = Polyline(
//   //     polylineId: id,
//   //     visible: true,
//   //     color: Colors.green,
//   //     points: polylineCoordinates,
//   //     width: 4,
//   //     startCap: Cap.roundCap,
//   //     endCap: Cap.buttCap,
//   //   );
//   //   polylines[id] = polyline;
//   //   setState(() {});
//   // }

//   // _getPolyline() async {
//   //   PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
//   //       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//   //       PointLatLng(currentLocation.latitude, currentLocation.longitude),
//   //       PointLatLng(_destLatitude, _destLongitude),

//   //       travelMode: TravelMode.walking,
//   //       wayPoints: [PolylineWayPoint(location: "Kiev")]);
//   //   if (result.points.isNotEmpty) {
//   //     result.points.forEach((PointLatLng point) {
//   //       polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//   //     });
//   //   }
//   //   _addPolyLine();
//   // }
// _getPolyline();

//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           title: Text('Geolocation'),
//         ),
//         body: Column(
//           children: <Widget>[
//             Stack(children: <Widget>[
//               Container(
//                   height: MediaQuery.of(context).size.height - 80.0,
//                   width: double.infinity,
//                   child: mapToggle
//                       ? GoogleMap(
//                           polylines: Set<Polyline>.of(polylines.values),
//                           myLocationEnabled: true,
//                           tiltGesturesEnabled: true,
//                           compassEnabled: true,
//                           scrollGesturesEnabled: true,
//                           onMapCreated: onMapCreated,
//                           initialCameraPosition: CameraPosition(
//                               target: LatLng(currentLocation.latitude,
//                                   currentLocation.longitude),
//                               zoom: 10.0),
//                         )
//                       : Center(
//                           child: Text('Loading...Please wait',
//                               style: TextStyle(
//                                 fontSize: 20,
//                               ))))
//             ])
//           ],
//         ));
//   }

//   void onMapCreated(GoogleMapController controller) async {
//     // setState(() {
//       mapController = controller;
//     // });
//   }
//   _addPolyLine() {
//     PolylineId id = PolylineId("poly");
//     Polyline polyline = Polyline(
//       polylineId: id,
//       visible: true,
//       color: Colors.green,
//       points: polylineCoordinates,
//       width: 4,
//       startCap: Cap.roundCap,
//       endCap: Cap.buttCap,
//     );
//     polylines[id] = polyline;
//     setState(() {});
//   }

//   _getPolyline() async {
//     PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
//         "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//         PointLatLng(_originLatitude, _originLongitude),
//         PointLatLng(_destLatitude, _destLongitude),

//         travelMode: TravelMode.walking,
//         wayPoints: [PolylineWayPoint(location: "Kiev")]);
//     if (result.points.isNotEmpty) {
//       result.points.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });
//     }
//     _addPolyLine();
//   }

// }

// --------------------------Location/Polylines------------------------------------

// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';
// import 'package:location/location.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Polyline example',
//       theme: ThemeData(
//         // This is the theme of your application.
//         //
//         // Try running your application with "flutter run". You'll see the
//         // application has a blue toolbar. Then, without quitting the app, try
//         // changing the primarySwatch below to Colors.green and then invoke
//         // "hot reload" (press "r" in the console where you ran "flutter run",
//         // or simply save your changes to "hot reload" in a Flutter IDE).
//         // Notice that the counter didn't reset back to zero; the application
//         // is not restarted.
//         primarySwatch: Colors.orange,
//       ),
//       home: MapScreen(),
//     );
//   }
// }

// class MapScreen extends StatefulWidget {
//   @override
//   _MapScreenState createState() => _MapScreenState();
// }

// class _MapScreenState extends State<MapScreen> {
//   GoogleMapController mapController;
//   Location _location = Location();

//   LatLng _initialPosition = LatLng(50.450712, 30.510529);

//   double _originLatitude = 50.450712, _originLongitude = 30.510529; // kiev //
//   double _destLatitude = 50.448938, _destLongitude = 30.514434;

//   Map<MarkerId, Marker> markers = {};
//   Map<PolylineId, Polyline> polylines = {};
//   List<LatLng> polylineCoordinates = [];
//   PolylinePoints polylinePoints = PolylinePoints();
//   String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

//   void _onMapCreated(GoogleMapController _controller) async {
//     mapController = _controller;
//     _location.onLocationChanged.listen((l) {
//       _controller.animateCamera(
//         CameraUpdate.newCameraPosition(
//           CameraPosition(
//             target: LatLng(l.latitude, l.longitude),
//             zoom: 15,
//           ),
//         ),
//       );
//     });
//   }

//   @override
//   void initState() {
//     super.initState();

//     /// origin marker
//     _addMarker(LatLng(_originLatitude, _originLongitude), "origin",
//         BitmapDescriptor.defaultMarker);

//     /// destination marker
//     _addMarker(LatLng(_destLatitude, _destLongitude), "destination",
//         BitmapDescriptor.defaultMarkerWithHue(90));
//     _getPolyline();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//       child: Scaffold(
//           body: GoogleMap(
//         initialCameraPosition:
//             CameraPosition(target: _initialPosition, zoom: 15),
//         mapType: MapType.terrain,
//         myLocationEnabled: true,
//         tiltGesturesEnabled: true,
//         compassEnabled: true,
//         scrollGesturesEnabled: true,
//         zoomGesturesEnabled: true,
//         onMapCreated: _onMapCreated,
//         markers: Set<Marker>.of(markers.values),
//         polylines: Set<Polyline>.of(polylines.values),
//       )),
//     );
//   }

//   _addMarker(LatLng position, String id, BitmapDescriptor descriptor) {
//     MarkerId markerId = MarkerId(id);
//     Marker marker =
//         Marker(markerId: markerId, icon: descriptor, position: position);
//     markers[markerId] = marker;
//   }

//   _addPolyLine() {
//     PolylineId id = PolylineId("poly");
//     Polyline polyline = Polyline(
//         polylineId: id, color: Colors.red, points: polylineCoordinates);
//     polylines[id] = polyline;
//     setState(() {});
//   }

//   _getPolyline() async {
//     PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
//       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//       PointLatLng(_originLatitude, _originLongitude),
//       PointLatLng(_destLatitude, _destLongitude),
//       travelMode: TravelMode.driving,
//       // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]
//     );
//     if (result.points.isNotEmpty) {
//       result.points.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });
//     }
//     _addPolyLine();
//   }
// }

// --------------------------Location/Polylines--error------------------------------------

// import 'dart:html';

// import 'dart:async';

// import 'package:flutter/material.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';
// // import 'package:location/location.dart';

// // import 'dart:async';

// import 'package:flutter/cupertino.dart';
// // import 'package:fluttertoast/fluttertoast.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Polyline example',
//       theme: ThemeData(
//         // This is the theme of your application.
//         //
//         // Try running your application with "flutter run". You'll see the
//         // application has a blue toolbar. Then, without quitting the app, try
//         // changing the primarySwatch below to Colors.green and then invoke
//         // "hot reload" (press "r" in the console where you ran "flutter run",
//         // or simply save your changes to "hot reload" in a Flutter IDE).
//         // Notice that the counter didn't reset back to zero; the application
//         // is not restarted.
//         primarySwatch: Colors.orange,
//       ),
//       home: MapScreen(),
//     );
//   }
// }

// class MapScreen extends StatefulWidget {
//   @override
//   _MapScreenState createState() => _MapScreenState();

// }

// class _MapScreenState extends State<MapScreen> {
//   bool mapToggle = false;
//   GoogleMapController mapController;
//   Position _position;
//   StreamSubscription <Position> _positionStream;

//   double _originLatitude = 50.450712, _originLongitude = 30.510529; // kiev //
//   double _destLatitude = 50.448938, _destLongitude = 30.514434;

//   Map<MarkerId, Marker> markers = {};
//   Map<PolylineId, Polyline> polylines = {};
//   List<LatLng> polylineCoordinates = [];
//   PolylinePoints polylinePoints = PolylinePoints();
//   String googleAPiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

//   void _onMapCreated(GoogleMapController _controller) async {
//     mapController = _controller;

//   }

//   @override
//   void initState() {
//     super.initState();
//     var locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 10);
//     _positionStream = Geolocator().getPositionStream(locationOptions).listen((Position position) {
//       setState(() {
//         print(position);
//         _position = position;
//       });
//     });

//     /// origin marker
//     _addMarker(LatLng(_originLatitude, _originLongitude), "origin",
//         BitmapDescriptor.defaultMarker);

//     /// destination marker
//     _addMarker(LatLng(_destLatitude, _destLongitude), "destination",
//         BitmapDescriptor.defaultMarkerWithHue(90));
//     _getPolyline();
//   }

//   @override
//   void dispose() {
//     super.dispose();
//     _positionStream.cancel();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//       child: Scaffold(
//           body: mapToggle ? GoogleMap(
//         initialCameraPosition:
//             CameraPosition(target: LatLng(_position.latitude, _position.longitude), zoom: 15),
//         mapType: MapType.terrain,
//         myLocationEnabled: true,
//         tiltGesturesEnabled: true,
//         compassEnabled: true,
//         scrollGesturesEnabled: true,
//         zoomGesturesEnabled: true,
//         onMapCreated: _onMapCreated,
//         markers: Set<Marker>.of(markers.values),
//         polylines: Set<Polyline>.of(polylines.values),
//       )
//       : Center(
//         child: Text('Loading... Please wait...',
//         style: TextStyle(fontSize: 20.0),
//         ),
//       )

//       ),
//     );
//   }

//   _addMarker(LatLng position, String id, BitmapDescriptor descriptor) {
//     MarkerId markerId = MarkerId(id);
//     Marker marker =
//         Marker(markerId: markerId, icon: descriptor, position: position);
//     markers[markerId] = marker;
//   }

//   _addPolyLine() {
//     PolylineId id = PolylineId("poly");
//     Polyline polyline = Polyline(
//         polylineId: id, color: Colors.red, points: polylineCoordinates);
//     polylines[id] = polyline;
//     setState(() {});
//   }

//   _getPolyline() async {
//     PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
//       "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//       PointLatLng(_originLatitude, _originLongitude),
//       PointLatLng(_destLatitude, _destLongitude),
//       travelMode: TravelMode.driving,
//       // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")]
//     );
//     if (result.points.isNotEmpty) {
//       result.points.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });
//     }
//     _addPolyLine();
//   }
// }

// ----------------------------------------MyLocation+Polyline-23.07.20-------------------------------------------

// import 'package:flutter_polyline_points/flutter_polyline_points.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:location/location.dart';
// import 'package:flutter/material.dart';
// import 'dart:async';
// import 'package:flutterfreegenapp/map_pin_pill.dart';
// import 'package:flutterfreegenapp/pin_pill_info.dart';

// const double CAMERA_ZOOM = 16;
// const double CAMERA_TILT = 0;
// const double CAMERA_BEARING = 0;
// const LatLng SOURCE_LOCATION = LatLng(50.450712, 30.510529);
// const LatLng DEST_LOCATION = LatLng(50.448938, 30.514434);

// void main() =>
//     runApp(MaterialApp(debugShowCheckedModeBanner: false, home: MapPage()));

// class MapPage extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() => MapPageState();
// }

// class MapPageState extends State<MapPage> {
//   Completer<GoogleMapController> _controller = Completer();
//   Set<Marker> _markers = Set<Marker>();
// // for my drawn routes on the map
//   Set<Polyline> _polylines = Set<Polyline>();
//   List<LatLng> polylineCoordinates = [];
//   PolylinePoints polylinePoints;
//   String googleAPIKey = 'AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM';
// // for my custom marker pins
//   BitmapDescriptor sourceIcon;
//   BitmapDescriptor destinationIcon;
// // the user's initial location and current location
// // as it moves
//   LocationData currentLocation;
// // a reference to the destination location
//   LocationData destinationLocation;
// // wrapper around the location API
//   Location location;
//   double pinPillPosition = -100;
//   PinInformation currentlySelectedPin = PinInformation(
//       pinPath: 'assets/map_icon.png',
//       avatarPath: 'assets/map_icon.png',
//       location: LatLng(50.450712, 30.510529),
//       locationName: 'Kiev',
//       labelColor: Colors.grey);
//   PinInformation sourcePinInfo;
//   PinInformation destinationPinInfo;

//   @override
//   void initState() {
//     super.initState();

//     // create an instance of Location
//     location = new Location();
//     polylinePoints = PolylinePoints();

//     // subscribe to changes in the user's location
//     // by "listening" to the location's onLocationChanged event
//     location.onLocationChanged.listen((LocationData cLoc) {
//       // cLoc contains the lat and long of the
//       // current user's position in real time,
//       // so we're holding on to it
//       currentLocation = cLoc;
//       updatePinOnMap();
//     });
//     // set custom marker pins
//     setSourceAndDestinationIcons();
//     // set the initial location
//     setInitialLocation();
//   }

//   void setSourceAndDestinationIcons() async {
//     BitmapDescriptor.fromAssetImage(
//             ImageConfiguration(devicePixelRatio: 2.0), 'assets/map_icon.png')
//         .then((onValue) {
//       sourceIcon = onValue;
//     });

//     BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.0),
//             'assets/map_icon.png')
//         .then((onValue) {
//       destinationIcon = onValue;
//     });
//   }

//   void setInitialLocation() async {
//     // set the initial location by pulling the user's
//     // current location from the location's getLocation()
//     currentLocation = await location.getLocation();

//     // hard-coded destination for this example
//     destinationLocation = LocationData.fromMap({
//       "latitude": DEST_LOCATION.latitude,
//       "longitude": DEST_LOCATION.longitude
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     CameraPosition initialCameraPosition = CameraPosition(
//         zoom: CAMERA_ZOOM,
//         tilt: CAMERA_TILT,
//         bearing: CAMERA_BEARING,
//         target: SOURCE_LOCATION);
//     if (currentLocation != null) {
//       initialCameraPosition = CameraPosition(
//           target: LatLng(currentLocation.latitude, currentLocation.longitude),
//           zoom: CAMERA_ZOOM,
//           tilt: CAMERA_TILT,
//           bearing: CAMERA_BEARING);
//     }
//     return Scaffold(
//       body: Stack(
//         children: <Widget>[
//           GoogleMap(
//               myLocationEnabled: true,
//               compassEnabled: true,
//               tiltGesturesEnabled: false,
//               markers: _markers,
//               polylines: _polylines,
//               mapType: MapType.normal,
//               initialCameraPosition: initialCameraPosition,
//               onTap: (LatLng loc) {
//                 pinPillPosition = -100;
//               },
//               onMapCreated: (GoogleMapController controller) {
//                 controller.setMapStyle(Utils.mapStyles);
//                 _controller.complete(controller);
//                 // my map has completed being created;
//                 // i'm ready to show the pins on the map
//                 showPinsOnMap();
//               }),
//           MapPinPillComponent(
//               pinPillPosition: pinPillPosition,
//               currentlySelectedPin: currentlySelectedPin)
//         ],
//       ),
//     );
//   }

//   void showPinsOnMap() {
//     // get a LatLng for the source location
//     // from the LocationData currentLocation object
//     var pinPosition =
//         LatLng(currentLocation.latitude, currentLocation.longitude);
//     // get a LatLng out of the LocationData object
//     var destPosition =
//         LatLng(destinationLocation.latitude, destinationLocation.longitude);

//     sourcePinInfo = PinInformation(
//         locationName: "Start Location",
//         location: SOURCE_LOCATION,
//         pinPath: "assets/map_icon.png",
//         avatarPath: "assets/map_icon.png",
//         labelColor: Colors.blueAccent);

//     destinationPinInfo = PinInformation(
//         locationName: "End Location",
//         location: DEST_LOCATION,
//         pinPath: "assets/map_icon.png",
//         avatarPath: "assets/map_icon.png",
//         labelColor: Colors.purple);

//     // add the initial source location pin
//     _markers.add(Marker(
//         markerId: MarkerId('sourcePin'),
//         position: pinPosition,
//         onTap: () {
//           setState(() {
//             currentlySelectedPin = sourcePinInfo;
//             pinPillPosition = 0;
//           });
//         },
//         icon: sourceIcon));
//     // destination pin
//     _markers.add(Marker(
//         markerId: MarkerId('destPin'),
//         position: destPosition,
//         onTap: () {
//           setState(() {
//             currentlySelectedPin = destinationPinInfo;
//             pinPillPosition = 0;
//           });
//         },
//         icon: destinationIcon));
//     // set the route lines on the map from source to destination
//     // for more info follow this tutorial
//     setPolylines();
//   }

//   void setPolylines() async {
//     PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
//         "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM",
//         PointLatLng(currentLocation.latitude, currentLocation.longitude),
//         PointLatLng(destinationLocation.latitude,
//         destinationLocation.longitude));

//     if (result.points.isNotEmpty) {
//       result.points.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });

//       setState(() {
//         _polylines.add(Polyline(
//             width: 2, // set the width of the polylines
//             polylineId: PolylineId("poly"),
//             color: Color.fromARGB(255, 40, 122, 198),
//             points: polylineCoordinates));
//       });
//     }
//   }

//   void updatePinOnMap() async {
//     // create a new CameraPosition instance
//     // every time the location changes, so the camera
//     // follows the pin as it moves with an animation
//     CameraPosition cPosition = CameraPosition(
//       zoom: CAMERA_ZOOM,
//       tilt: CAMERA_TILT,
//       bearing: CAMERA_BEARING,
//       target: LatLng(currentLocation.latitude, currentLocation.longitude),
//     );
//     final GoogleMapController controller = await _controller.future;
//     controller.animateCamera(CameraUpdate.newCameraPosition(cPosition));
//     // do this inside the setState() so Flutter gets notified
//     // that a widget update is due
//     setState(() {
//       // updated position
//       var pinPosition =
//           LatLng(currentLocation.latitude, currentLocation.longitude);

//       sourcePinInfo.location = pinPosition;

//       // the trick is to remove the marker (by id)
//       // and add it again at the updated location
//       _markers.removeWhere((m) => m.markerId.value == 'sourcePin');
//       _markers.add(Marker(
//           markerId: MarkerId('sourcePin'),
//           onTap: () {
//             setState(() {
//               currentlySelectedPin = sourcePinInfo;
//               pinPillPosition = 0;
//             });
//           },
//           position: pinPosition, // updated position
//           icon: sourceIcon));
//     });
//   }
// }

// class Utils {
//   static String mapStyles = '''[
//   {
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#f5f5f5"
//       }
//     ]
//   },
//   {
//     "elementType": "labels.icon",
//     "stylers": [
//       {
//         "visibility": "off"
//       }
//     ]
//   },
//   {
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#616161"
//       }
//     ]
//   },
//   {
//     "elementType": "labels.text.stroke",
//     "stylers": [
//       {
//         "color": "#f5f5f5"
//       }
//     ]
//   },
//   {
//     "featureType": "administrative.land_parcel",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#bdbdbd"
//       }
//     ]
//   },
//   {
//     "featureType": "poi",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#eeeeee"
//       }
//     ]
//   },
//   {
//     "featureType": "poi",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#757575"
//       }
//     ]
//   },
//   {
//     "featureType": "poi.park",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#e5e5e5"
//       }
//     ]
//   },
//   {
//     "featureType": "poi.park",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#9e9e9e"
//       }
//     ]
//   },
//   {
//     "featureType": "road",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#ffffff"
//       }
//     ]
//   },
//   {
//     "featureType": "road.arterial",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#757575"
//       }
//     ]
//   },
//   {
//     "featureType": "road.highway",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#dadada"
//       }
//     ]
//   },
//   {
//     "featureType": "road.highway",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#616161"
//       }
//     ]
//   },
//   {
//     "featureType": "road.local",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#9e9e9e"
//       }
//     ]
//   },
//   {
//     "featureType": "transit.line",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#e5e5e5"
//       }
//     ]
//   },
//   {
//     "featureType": "transit.station",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#eeeeee"
//       }
//     ]
//   },
//   {
//     "featureType": "water",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#c9c9c9"
//       }
//     ]
//   },
//   {
//     "featureType": "water",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#9e9e9e"
//       }
//     ]
//   }
// ]''';
// }

// ------------------------Find place and show polyline-------------------------------

// import 'dart:async';
// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:google_maps_webservice/places.dart';
// import 'package:flutter_google_places/flutter_google_places.dart';
// import 'package:flutter/foundation.dart';
// import 'package:flutter/gestures.dart';

// void main() {
//   runApp(MyApp());
// }

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(

//         primarySwatch: Colors.blue,
//         visualDensity: VisualDensity.adaptivePlatformDensity,
//       ),
//       home: MyHomePage(title: 'Current Location And Map'),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   MyHomePage({Key key, this.title}) : super(key: key);
//   final String title;
//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }
// const String kGoogleApiKey = "AIzaSyC4z0D8CV6k19Cl7XVweY2-qzSCaRhVihM";

// class _MyHomePageState extends State<MyHomePage> {

// // to get places detail (lat/lng)
//   GoogleMapsPlaces places = GoogleMapsPlaces(apiKey: kGoogleApiKey);
//   List<Marker> globalMarkerList  = List<Marker>();
//   final Set<Polyline>_polyline = {};
//   MapType mapType = MapType.none;

//   @override
//   void initState() {
//     super.initState();
//     Future<Position> currentDefaultPosition = getCurrentPosition();
//     currentDefaultPosition.then((value) {
//     updateMap(value, true);
//     });
//   }

//   //for any change in marker like pick or  drop just call this method
//   void updateMap(Position value, bool isPickUp) {
//     //update marker Set
//     MarkerPickDrop mpd = getMarkerWithMarkerValueAndpickDropState(value, isPickUp);
//     updateGlobalMarkerListState(mpd);
//     //update polyline
//     Polyline polyline = getPolyLine(value);
//     updatePolylineState(polyline);
//     //update map style
//     updateMapStyle();
//   }
// /*
// * I have foundle map have some problems in rendering. I tried with hybrid,
// * satellite & terrain they all take a lot of time in updating UI. So you will
// * notice that i am rendering mapType after a deleay of 1 sec.
// * */
//   void updateMapStyle() {
//     Future.delayed(const Duration(milliseconds: 1000), ()
//     {
//       //update map style
//       setState(() {
//         mapType = MapType.normal;
//       });
//     });
//   }

//   /*
// *
// * By default when youlaunch ther will be on pick up point which is your default current location on index 0.
// * when you set drop at, it is either added or update on index 1.
// * I will use   s class model/tuple/dictiry later fix
// * */
//   void updateGlobalMarkerListState(MarkerPickDrop mpd) {

//     var l = globalMarkerList;
//     if (mpd.isPick == true) {
//       if (l.length == 0){
//         l.insert(0, mpd.marker);
//       }else {
//         l.removeAt(0);
//         l.insert(0, mpd.marker);
//       }
//     } else {
//       if (l.length == 1) {
//         l.add(mpd.marker);
//       } else {
//         l.removeLast();
//         l.add(mpd.marker);
//       }
//     }

//     setState(() {
//       this.globalMarkerList = l;
//     });
//   }

//   void updatePolylineState(Polyline polyline) {
//     Future.delayed(const Duration(milliseconds: 1000), ()
//     {
//       setState(() {
//         _polyline.clear();
//       });
//       setState(() {
//         _polyline.add(polyline);
//       });
//     });
//   }

//   Polyline getPolyLine(Position value) {

//     List<LatLng> l = List<LatLng>();

//     for (var i = 0; i < globalMarkerList.length; i++) {
//       Marker m = globalMarkerList.elementAt(i);
//       LatLng ll = LatLng(m.position.latitude, m.position.longitude);
//       l.add(ll);
//     }

//     Polyline polyline = Polyline(
//       polylineId: PolylineId(value.toString()),
//       visible: true,
//       points: l,
//       color: Colors.red,
//     );
//     return polyline;
//   }

//   MarkerPickDrop getMarkerWithMarkerValueAndpickDropState(Position value, bool isPick) {
//     MarkerPickDrop mpd = MarkerPickDrop();
//     mpd.isPick = isPick;
//     mpd.marker = Marker(position: LatLng(value.latitude, value.longitude), markerId: MarkerId(value.toString()));
//     return mpd;
//   }

//   Future<Position> getCurrentPosition() async {
//     Position p = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
//     return p;
//   }

//   @override
//   Widget build(BuildContext context) {
// //draw map only when you have position and markers set
//     if (this.globalMarkerList.length > 0) {

//       return Scaffold(

//         appBar: AppBar(
//           title: Text(widget.title),
//         ),
//         body: Center(

//           child: Column(
//             children: [
//               Expanded(
//                 child:buildGoogleMap(),
//               ),
//               FlatButton(
//                 child: Text("Pick From"),
//                 onPressed: ()  {
//                   buttonPressed(context, true);
//                 },
//               ),
//               FlatButton(
//                 child: Text("Drop At"),
//                 onPressed: ()  {
//                   buttonPressed(context, false);
//                   },
//               ),
//             ],
//           ),
//         ),
//       );
// //otherwise show loading text...
//     }else {
//       return getLoader(context);
//     }

//   }

//   void buttonPressed(BuildContext context, bool isPick) async {
//     Prediction p = await PlacesAutocomplete.show(
//         context: context, apiKey: kGoogleApiKey);
//     Future<PlacesDetailsResponse> placesDetailsResponse = getPredictedLatLng(p, isPick);
//     placesDetailsResponse.then((detail) {
//       var placeId = p.placeId;
//       double lat = detail.result.geometry.location.lat;
//       double lng = detail.result.geometry.location.lng;
//       Position position = Position(latitude: lat, longitude: lng);
//       updateMap(position, isPick);
//     }).catchError((error) {
//       //default position
//       Position position = Position(latitude: globalMarkerList.elementAt(0).position.latitude, longitude: globalMarkerList.elementAt(0).position.longitude);
//       updateMap(position, isPick);
//       print(error);
//     }).whenComplete(() {

//     });
//   }
// //return google map
//   GoogleMap buildGoogleMap() {

//     return GoogleMap(
//       gestureRecognizers: Set()
//         ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer())),
//       polylines: _polyline,
//       markers: Set.from(globalMarkerList),
//       mapType: MapType.normal,
//       initialCameraPosition: CameraPosition(
//           target: LatLng(globalMarkerList.elementAt(0).position.latitude, globalMarkerList.elementAt(0).position.longitude),
//           zoom: 15),
//     );
//   }

//   //return loader
//   Widget getLoader(BuildContext context) {
//     return Center(
//       child: CircularProgressIndicator(),
//     );
//   }

//   Future<PlacesDetailsResponse> getPredictedLatLng(Prediction p, bool isPickUp) async {
//       PlacesDetailsResponse detail =
//       await places.getDetailsByPlaceId(p.placeId);
//       return detail;
//   }
// }

// class MarkerPickDrop {
//   bool isPick;
//   Marker marker;
// }

// --------------------------------------------LYFECYCLE---------------------------------------------------------------

// import 'package:flutter/material.dart';

// void main() => runApp(new MyApp());

// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
//   AppLifecycleState _appLifecycleState;
//   @override
//   void initState() {
//     super.initState();
//     WidgetsBinding.instance.addObserver(this);
//   }

//   @override
//   void dispose() {
//     WidgetsBinding.instance.removeObserver(this);
//     super.dispose();
//   }

//   @override
//   void didChangeAppLifecycleState(AppLifecycleState state) {
//     setState(() {
//       _appLifecycleState = state;
//       print('My App State: $_appLifecycleState');
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(home:
//         Scaffold(body: OrientationBuilder(builder: (context, orientation) {
//       return Center(
//           child: Text(_appLifecycleState.toString(),
//               style: TextStyle(
//                 fontSize: 20,
//                 color: orientation == Orientation.portrait
//                     ? Colors.blue
//                     : Colors.red,
//               )));
//     })));
//   }
// }

// -------------------------------Working with backend----------------------------------------

// import 'package:flutter/material.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: MyHomePage(),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Working with backend'),
//       ),
//       body: StreamBuilder(
//         stream: Firestore.instance.collection('users').snapshots(),
//         builder: (context, snapshot) {
//           if(!snapshot.hasData)
//             return Text('Loading data... Please wait...');
//             return Column(children: <Widget>[
//               Text(snapshot.data.documents[0]['name'], style: TextStyle(fontSize: 15.0)),              
//               Text(snapshot.data.documents[0]['hip'].toString(), style: TextStyle(fontSize: 15.0)),              
//               // Text(snapshot.data.documents[1]['rewardsname'], style: TextStyle(fontSize: 15.0)),
//               // Text(snapshot.data.documents[1]['points'].toString(), style: TextStyle(fontSize: 15.0))
//             ],);
//         },
//         ),
//     );
//   }
// }